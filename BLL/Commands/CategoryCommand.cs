//------------------------------------------------------------------------------
// <auto-generated> This code was generated from a T4 template on:
//      02-Jan-18 2:35:44 PM 
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLL;
using BLL.ModelViews;
using BLL.Commands;
using BLL.Queries;
using BLL.Mappers;
using BLL.Validations;
using WICMS.Domain;

namespace BLL.Commands
{

    public class Category
    {

        public static ModelViews.Category.Extend Create(ModelViews.Category.Extend view)
        {

            var data = new WICMS.Domain.Entities.Category();

            try
            {
                using (var ctx = ISDAL.GetDataContext())
                {
                    if (view.ID == 0)
                    {

                        data.Guid = view.Guid;
                        data.Title = view.Title;
                        data.Description = view.Description;
                        data.Group = view.Group;
                        data.CreatedDate = DateTime.UtcNow;
                        data.UpdatedDate = DateTime.UtcNow;
                        data.Status = view.Status;
                        ctx.Categories.InsertOnSubmit(data);
                        ctx.SubmitChanges();

                        // Tracker.Track(TrackingState.Created, "Created Category", "Category", data.id);
                    }
                }
            }
            catch (Exception ex)
            {
                data = new WICMS.Domain.Entities.Category();
                // ExceptionLog.Create(ex, ExceptionLog.ErrorState.Significant, "CategoryController.Create");
            }

            return Mappers.Category.Basic(data);
        }

        public static ModelViews.Category.Extend Update(ModelViews.Category.Extend view)
        {

            var data = new WICMS.Domain.Entities.Category();

            try
            {
                using (var ctx = ISDAL.GetDataContext())
                {
                    data = ctx.Categories.FirstOrDefault(c => c.Guid == view.Guid);

                    if (data != null)
                    {

                        data.Title = view.Title;
                        data.Description = view.Description;
                        data.Group = view.Group;
                        data.CreatedDate = DateTime.UtcNow;
                        data.UpdatedDate = DateTime.UtcNow;
                        data.Status = view.Status;

                        ctx.SubmitChanges();

                        // Tracker.Track(TrackingState.Updated, "Updated Category", "Category", data.guid);
                    }
                }
            }
            catch (Exception ex)
            {
                data = new WICMS.Domain.Entities.Category();
                //  ExceptionLog.Create(ex, ExceptionLog.ErrorState.Significant, "CategoryController.Update");
            }

            return Mappers.Category.Basic(data);
        }

        public static bool Delete(Guid guid)
        {
            try
            {
                using (var ctx = ISDAL.GetDataContext())
                {
                    var data = ctx.Categories.FirstOrDefault(c => c.Guid == guid);
                    if (data != null)
                    {
                        ctx.Categories.DeleteOnSubmit(data);
                        ctx.SubmitChanges();
                        // Tracker.Track(TrackingState.Deleted, "Delete Categorys", "Category", data.guid);
                    }
                }
            }
            catch (Exception ex)
            {
                //ExceptionLog.Create(ex, ExceptionLog.ErrorState.Significant, "CategoryController.Delete by Guid");
                return false;
            }

            return true;
        }

    }
}
