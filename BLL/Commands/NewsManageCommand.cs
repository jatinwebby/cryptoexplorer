﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WICMS.Domain;
using WICMS.Domain.Entities;

namespace BLL.Commands
{
    public class NewsManage
    {
        public static ModelViews.NewsManage.Extend Create(ModelViews.NewsManage.Extend view)
        {

            var data = new WICMS.Domain.Entities.NewsManage();
            try
            {
                using (var ctx = ISDAL.GetDataContext())
                {
                    if (view.ID == 0)
                    {
                        data.Guid = Guid.NewGuid();
                        data.HeaderImage = view.HeaderImage;
                        data.Author = view.Author;
                        data.Title = view.Title;
                        data.ShortDescription = view.ShortDescription;
                        data.LongDescription = view.LongDescription;
                        data.StartEventDate = Convert.ToDateTime(view.StartEventDate).ToUniversalTime(); //.Date for only date from datetime
                        data.EndEventDate = Convert.ToDateTime(view.EndEventDate).ToUniversalTime();
                        data.Sort = view.Sort;           
                        data.CreatedDate = DateTime.UtcNow;
                        data.UpdatedDate = DateTime.UtcNow;
                        data.isDeleted = view.isDeleted;
                        data.Status = Tools.Constants.Status.ACTIVE;
                        ctx.NewsManages.InsertOnSubmit(data);
                        ctx.SubmitChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                data = new WICMS.Domain.Entities.NewsManage();
            }

            return Mappers.NewsManage.Basic(data);
        }

        public static ModelViews.NewsManage.Extend Update(ModelViews.NewsManage.Extend view)
        {
            var data = new WICMS.Domain.Entities.NewsManage();
            try
            {
                using (var ctx = ISDAL.GetDataContext())
                {
                    data = ctx.NewsManages.FirstOrDefault(c => c.Guid == view.Guid);
                    if (data != null)
                    {
                        // data.Guid = Guid.NewGuid();
                        data.HeaderImage = view.HeaderImage;
                        data.Author = view.Author;
                        data.Title = view.Title;
                        data.ShortDescription = view.ShortDescription;
                        data.LongDescription = view.LongDescription;
                        data.StartEventDate = Convert.ToDateTime(view.StartEventDate).ToUniversalTime();//.Date for only date from datetime
                        data.EndEventDate = Convert.ToDateTime(view.EndEventDate).ToUniversalTime();
                        data.Sort = view.Sort;  
                        // data.CreatedDate = DateTime.UtcNow;
                        data.UpdatedDate = DateTime.UtcNow;
                        //data.isDeleted = view.isDeleted;
                        //data.Status = Tools.Constants.Status.ACTIVE;
                        // ctx.NewsManages.InsertOnSubmit(data);
                        ctx.SubmitChanges();
                    }                   
                }
            }
            catch (Exception ex)
            {
                data = new WICMS.Domain.Entities.NewsManage();
            }

            return Mappers.NewsManage.Basic(data);
        }
        
        public static bool Delete(Guid guid)
        {
            try
            {
                using (var ctx = ISDAL.GetDataContext())
                {
                    var data = ctx.NewsManages.FirstOrDefault(c => c.Guid == guid);
                    if (data != null)
                    {
                        //ctx.NewsManages.DeleteOnSubmit(data);
                        data.isDeleted = true;
                        ctx.SubmitChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }
    }
}
