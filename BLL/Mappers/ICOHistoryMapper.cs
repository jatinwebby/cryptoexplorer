﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Mappers
{
    public class ICOHistory
    {
        public static ModelViews.ICOHistory.Extend Basic(WICMS.Domain.Entities.ICOHistory data)
        {
            var view = new ModelViews.ICOHistory.Extend();
            view.ID = data.ID;
            view.Guid = data.Guid;
            view.ICOID = data.ICOID;
            view.Price = data.Price;           
            view.MarketCap = data.MarketCap;
            view.CirculatingSupply = data.CirculatingSupply;
            view.MaxSupply = data.MaxSupply;            
            view.UpdatedDate = data.UpdatedDate;
            view.Status = data.Status;

            return view;
        }

    }
}
