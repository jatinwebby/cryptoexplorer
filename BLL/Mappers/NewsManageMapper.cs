﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Mappers
{
    public class NewsManage
    {
        public static ModelViews.NewsManage.Extend Basic(WICMS.Domain.Entities.NewsManage data)
        {
            var view = new ModelViews.NewsManage.Extend();

            view.ID = data.ID;
            view.Guid = data.Guid;
            view.HeaderImage = data.HeaderImage;
            view.Author = data.Author;
            view.Title = data.Title;
            view.ShortDescription = data.ShortDescription;
            view.LongDescription = data.LongDescription;
            view.StartEventDate = data.StartEventDate;
            view.EndEventDate = data.EndEventDate;
            view.Sort = data.Sort;           
            view.CreatedDate = data.CreatedDate;
            view.UpdatedDate = data.UpdatedDate;            
            view.isDeleted = data.isDeleted;
            view.Status = data.Status;

            return view;            
        } 
    }
}
