﻿using System;
using System.Net;

namespace BLL.ViewModel
{
    public class APIResponse
    {
        public APIResponse(string status, string message, object data = null)
        {
            Status = status;
            Message = message;
            Data = data;
        }

        public string Status { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
    }
}