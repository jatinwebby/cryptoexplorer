﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.ModelViews
{
    public class ICOHistory
    {
        public class Extend : DbModel
        {

        }
        public class DbModel
        {
            public long ID { get; set; }
            public Guid Guid { get; set; }
            public long ICOID { get; set; }
            public Nullable<double> Price { get; set; }          
            public Nullable<double> MarketCap { get; set; }
            public Nullable<double> CirculatingSupply { get; set; }
            public Nullable<double> MaxSupply { get; set; }
            public System.DateTime UpdatedDate { get; set; }
            public string Status { get; set; }
        }

    }
}
