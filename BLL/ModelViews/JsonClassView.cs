﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.ModelViews
{
    public class JsonClassView
    {

    }
    public class USD
    {
        public string TYPE { get; set; }
        public string MARKET { get; set; }
        public string FROMSYMBOL { get; set; }
        public string TOSYMBOL { get; set; }
        public string FLAGS { get; set; }
        public string PRICE { get; set; }
        public int LASTUPDATE { get; set; }
        public int LASTVOLUME { get; set; }
        public int LASTVOLUMETO { get; set; }
        public int LASTTRADEID { get; set; }
        public int VOLUMEDAY { get; set; }
        public int VOLUMEDAYTO { get; set; }
        public int VOLUME24HOUR { get; set; }
        public int VOLUME24HOURTO { get; set; }
        public string OPENDAY { get; set; }
        public string HIGHDAY { get; set; }
        public string LOWDAY { get; set; }
        public string OPEN24HOUR { get; set; }
        public string HIGH24HOUR { get; set; }
        public string LOW24HOUR { get; set; }
        public string LASTMARKET { get; set; }
        public double CHANGE24HOUR { get; set; }
        public double CHANGEPCT24HOUR { get; set; }
        public double CHANGEDAY { get; set; }
        public double CHANGEPCTDAY { get; set; }
        public int SUPPLY { get; set; }
        public int MKTCAP { get; set; }
        public double TOTALVOLUME24H { get; set; }
        public double TOTALVOLUME24HTO { get; set; }
    }

    public class Coin_Code
    {
        public USD USD { get; set; }
    }

    public class RAW
    {
        public dynamic Coin_Code { get; set; }
    }

    public class USD2
    {
        public string FROMSYMBOL { get; set; }
        public string TOSYMBOL { get; set; }
        public string MARKET { get; set; }
        public string PRICE { get; set; }
        public string LASTUPDATE { get; set; }
        public string LASTVOLUME { get; set; }
        public string LASTVOLUMETO { get; set; }
        public int LASTTRADEID { get; set; }
        public string VOLUMEDAY { get; set; }
        public string VOLUMEDAYTO { get; set; }
        public string VOLUME24HOUR { get; set; }
        public string VOLUME24HOURTO { get; set; }
        public string OPENDAY { get; set; }
        public string HIGHDAY { get; set; }
        public string LOWDAY { get; set; }
        public string OPEN24HOUR { get; set; }
        public string HIGH24HOUR { get; set; }
        public string LOW24HOUR { get; set; }
        public string LASTMARKET { get; set; }
        public string CHANGE24HOUR { get; set; }
        public string CHANGEPCT24HOUR { get; set; }
        public string CHANGEDAY { get; set; }
        public string CHANGEPCTDAY { get; set; }
        public string SUPPLY { get; set; }
        public string MKTCAP { get; set; }
        public string TOTALVOLUME24H { get; set; }
        public string TOTALVOLUME24HTO { get; set; }
    }

    public class Coin_Code2
    {
        public USD2 USD { get; set; }
    }

    public class DISPLAY
    {
        public Coin_Code2 Coin_Code { get; set; }
    }

    public class RootObject
    {
        public RAW RAW { get; set; }
        public DISPLAY DISPLAY { get; set; }
    }


    //public class RAW
    //{
    //    public class USD
    //    {
    //        public string PRICE { get; set; }
    //        public string SUPPLY { get; set; }
    //        public string MKTCAP { get; set; }

    //    }

    //}
}

