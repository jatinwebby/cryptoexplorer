﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.ModelViews
{
   public class LogManager
    {
        public int ID { get; set; }
        public System.Guid GUID { get; set; }
        public string UserMsg { get; set; }
        public string ex_Message { get; set; }
        public string ex_StackTrace { get; set; }
        public string ex_InnerMessage { get; set; }
        public string ex_InnerStrackTrace { get; set; }
        public string PageName { get; set; }
        public System.DateTime CreatedDateTime { get; set; }
        public System.Nullable<long> TranscationID { get; set; }
    }
}
