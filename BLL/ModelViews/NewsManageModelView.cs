﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.ModelViews
{
    public class NewsManage
    {

        public class Extend : DbModel
        {

        }
        public class DbModel
        {
            public long ID { get; set; }
            public Guid Guid { get; set; }
            //public long ICOID { get; set; }
            public string HeaderImage { get; set; }
            public string Author { get; set; }
            public string Title { get; set; }
            public string ShortDescription { get; set; }
            public string LongDescription { get; set; }
            public System.DateTime StartEventDate { get; set; }
            public System.DateTime EndEventDate { get; set; }
            public Nullable<int> Sort { get; set; }
            public System.DateTime CreatedDate { get; set; }
            public System.DateTime UpdatedDate { get; set; }
            public bool isDeleted { get; set; }
            public string Status { get; set; }

        }

    }
}
