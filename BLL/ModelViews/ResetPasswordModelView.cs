﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.ModelViews
{       //ModelView
    public class ResetPassword
    { 
         /// </summary>
        public string Password { get; set; }
        public string ConfirmNewPassword { get; set; }
        //public string ConfirmNewPassword { get; set; }
        public string ReturnToken { get; set; }
    }
}
