﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.ModelViews
{
    public class URLResponse
    {
        //public URLResponse(string title, string message, string page_link,string link_text)
        //{
        //    Title = title;
        //    Message = message;
        //    Page_Link = page_link;
        //    Link_Text = link_text;
        //}System.Uri

        public string Title { get; set; }
        public string Message { get; set; }
        public string Page_Link { get; set; } 
        public string Link_Text { get; set; }
        //public URLResponse() { Page_Link = String.Empty; }
    }
}
