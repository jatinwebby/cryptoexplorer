﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WICMS.Domain;

namespace BLL.Queries
{
    public class NewsManage
    {
        public static List<ModelViews.NewsManage.Extend> GetAll()
        {
            using (var ctx = ISDAL.GetDataContext())
            {
                var data = ctx.NewsManages.Where(h=>h.isDeleted == false && h.Status == Tools.Constants.Status.ACTIVE).OrderBy(h => h.ID).ToList();
                return data.Select(Mappers.NewsManage.Basic).ToList();
            }
        }

        public static List<ModelViews.NewsManage.Extend> GetActive()
        {
            using (var ctx = ISDAL.GetDataContext())
            {
                var data = ctx.NewsManages.Where(h => h.Status == Tools.Constants.Status.ACTIVE).OrderBy(h => h.ID).ToList();
                return data.Select(Mappers.NewsManage.Basic).ToList();
            }
        }     

        public static ModelViews.NewsManage.Extend GetById(int id)
        {
            using (var ctx = ISDAL.GetDataContext())
            {
                var data = ctx.NewsManages.FirstOrDefault(h => h.ID == id);
                return data != null ? Mappers.NewsManage.Basic(data) : null;
            }
        }

        public static ModelViews.NewsManage.Extend GetByGuid(Guid guid)
        {
            using (var ctx = ISDAL.GetDataContext())
            {
                var data = ctx.NewsManages.FirstOrDefault(h => h.Guid == guid);
                return data != null ? Mappers.NewsManage.Basic(data) : null;
            }
        }

        public static List<ModelViews.NewsManage.Extend> GetTodayRangeNews()
        {
            using (var ctx = ISDAL.GetDataContext())
            {
                DateTime today_date = DateTime.UtcNow; //|| (h.StartEventDate <= (today_date).Date && h.EndEventDate >= (today_date).Date)
                var data = ctx.NewsManages.Where(h=> (h.StartEventDate >= (today_date).Date && h.EndEventDate >= (today_date).Date) || (h.StartEventDate <= (today_date).Date && h.EndEventDate >= (today_date).Date) && h.isDeleted == false && h.Status == Tools.Constants.Status.ACTIVE).OrderByDescending(h => h.StartEventDate).ToList();
                // return data != null ? Mappers.NewsManage.Basic(data) : null;
                return data.Select(Mappers.NewsManage.Basic).ToList();
            }
        }

        public static List<ModelViews.NewsManage.Extend> GetSelDateRangeNews(string fromDate, string toDate)
        {
            using (var ctx = ISDAL.GetDataContext())
            {
                DateTime l_DateFrom = Convert.ToDateTime(fromDate).AddHours(24).AddDays(-1).ToUniversalTime();
                DateTime l_DateTo = Convert.ToDateTime(toDate).AddDays(1).ToUniversalTime();

                //var data = ctx.NewsManages.Where(h => h.StartEventDate >= Convert.ToDateTime(fromDate, System.Globalization.CultureInfo.GetCultureInfo("ur-PK").DateTimeFormat) && h.EndEventDate <= Convert.ToDateTime(toDate, System.Globalization.CultureInfo.GetCultureInfo("ur-PK").DateTimeFormat) && h.isDeleted == false && h.Status == Tools.Constants.Status.ACTIVE).OrderByDescending(h => h.StartEventDate).ToList();
                //var data1 = ctx.NewsManages.Where(h => h.StartEventDate >= Convert.ToDateTime((l_DateFrom).ToString("dd/MM/yyyy")) && h.EndEventDate <= Convert.ToDateTime((l_DateTo).ToString("dd/MM/yyyy")) && h.isDeleted == false && h.Status == Tools.Constants.Status.ACTIVE).OrderByDescending(h => h.StartEventDate).ToList();
                var data = ctx.NewsManages.Where(h => (h.StartEventDate >= l_DateFrom.Date && h.EndEventDate <= l_DateFrom.Date) || (h.StartEventDate<=l_DateTo && h.EndEventDate>=l_DateTo) && h.isDeleted == false && h.Status == Tools.Constants.Status.ACTIVE).OrderByDescending(h => h.StartEventDate).ToList();
                
                //var data1 = ctx.NewsManages.Where(h => h.StartEventDate.CompareTo(l_DateFrom.ToString("dd/MM/yyyy")) >= 0 && h.EndEventDate.CompareTo()  && h.isDeleted == false && h.Status == Tools.Constants.Status.ACTIVE).OrderByDescending(h => h.StartEventDate).ToList();
                return data.Select(Mappers.NewsManage.Basic).ToList();
            }
        }
    }
}
