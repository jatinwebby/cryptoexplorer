﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Configuration;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web.Script.Serialization;
using System.Xml;
using WI.DBLogManager;
using System.Net.Mail;
using System.Data;
using System.Collections.Generic;
using System.Web;
using BLL.Validations;
//using System.Net.Http;
using BLL.ViewModel;
using System.Net;
using System.Data.SqlClient;
using WICMS.Domain;

namespace BLL.Tools
{
    public class CommonFunction
    {
        public static string GenerateHash(string forHash)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(forHash));

            //get hash result after compute it
            byte[] result = md5.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                //change it into 2 hexadecimal digits
                //for each byte
                strBuilder.Append(result[i].ToString("x2"));
            }
            //return Json(strBuilder.ToString(),JsonRequestBehavior.AllowGet);
            return strBuilder.ToString();
        }

        public static DataTable ToDataTable(List<BLL.ModelViews.ICO.Extend> data)// T is any generic type
        {
            //PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            table.Columns.Add("ID", typeof(Int64));
            table.Columns.Add("CoinName",typeof(string));
            table.Columns.Add("Price", typeof(double));
            table.Columns.Add("MarketCap", typeof(double));
            table.Columns.Add("CirculatingSupply", typeof(double));
            table.Columns.Add("MaxSupply", typeof(double));
            table.Columns.Add("Rate", typeof(double));
            object[] values = new object[7];
            foreach (var item in data)
            {
                
                        values[0] = item.ID;
                        //values[1] = item.Guid;
                        //values[2] = item.CoinCode;
                        values[1] = item.CoinName;
                        //values[4] = item.CoinIcon;
                        
                        //values[6] = item.Hype;
                        //values[7] = item.Scam;
                        //values[8] = item.Moon;
                        //values[9] = item.Start;
                        //values[10] = item.ICOEnd;
                        //values[11] = item.Stage;
                        values[2] = item.Price.Value;
                        //values[13] = item.NextRound;
                        //values[14] = item.Website;
                        //values[15] = item.ReferrerUrl;
                        values[3] = item.MarketCap.Value;
                        values[4] = item.CirculatingSupply.Value;
                        values[5] = item.MaxSupply.Value;
                                              
                        if(item.Rate != null)
                        {
                            values[6] = item.Rate.Value;
                        }
                        
                //values[19] = item.IsRecommend;
                //values[20] = item.CrypoType;
                //values[21] = item.Description;
                //values[23] = item.CreatedDate;
                //values[24] = item.UpdatedDate;
                //values[25] = item.Status;

                table.Rows.Add(values);
            }
            return table;
        }

        public static bool sendMail(string sub,string body, string toMail = "",string fromMail= "")
        {
            try { 

                    if (toMail == "" || string.IsNullOrEmpty(toMail)){
                        toMail=BLL.Tools.Constants.SMTP_MAIL.USERNAME.ToString();
                    }
            
                    if(fromMail== "" || string.IsNullOrEmpty(fromMail)) {
                        fromMail = BLL.Tools.Constants.SMTP_MAIL.FROM_MAIL_ADDRESS;
                    }
                
                    string[] ToMuliId = toMail.Split(',');
                    foreach (string ToEMailId in ToMuliId)
                    {
                        MailMessage mail = new MailMessage();
                        mail.From = new MailAddress(fromMail, fromMail);
                        mail.Subject = sub;
                        mail.Body = body;
                        mail.IsBodyHtml = true;
                        mail.To.Add(ToEMailId);
                        SmtpClient smtp = new SmtpClient();
                        smtp.Host = BLL.Tools.Constants.SMTP_MAIL.SMTP_SERVER;
                        smtp.Port = BLL.Tools.Constants.SMTP_MAIL.SMTP_PORT;
                        smtp.UseDefaultCredentials = false;
                        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                        smtp.Credentials = new System.Net.NetworkCredential(BLL.Tools.Constants.SMTP_MAIL.USERNAME, BLL.Tools.Constants.SMTP_MAIL.PASSWORD); // Enter seders User name and password   
                        smtp.EnableSsl = BLL.Tools.Constants.SMTP_MAIL.ENABLE_SSL;
                        smtp.Send(mail);
                        smtp.Dispose();                   
                    }
            }
            catch(Exception ex)
            {
                Guid Errorguid = CommonFunction.LogManager("BLL/Tools/CommonFunction/sendMail()", ex, "BLL/Tools/CommonFunction/sendMail()");
                return false;
            }
            return true;
        }

        public static bool sendMailSubscriberToAdmin(string fromMail,string body)
        {
            try { 
            MailMessage mail = new MailMessage();
            mail.To.Add(BLL.Tools.Constants.SMTP_MAIL.USERNAME);
            mail.From = new MailAddress(fromMail);
            mail.Subject = "Contact Us";            
            mail.Body = body;
            mail.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();           
            smtp.Host = @BLL.Tools.Constants.SMTP_MAIL.SMTP_SERVER;
            smtp.Port = @BLL.Tools.Constants.SMTP_MAIL.SMTP_PORT;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new System.Net.NetworkCredential(BLL.Tools.Constants.SMTP_MAIL.USERNAME, BLL.Tools.Constants.SMTP_MAIL.PASSWORD); // Enter seders User name and password   
            smtp.EnableSsl = true;
            smtp.Send(mail);
            }
            catch(Exception ex)
            {
                return false;
            }
            return true;
        }

        public static CaptchaResponse ValidateCaptcha(string response)
        {
            var ip = System.Web.HttpContext.Current != null ? System.Web.HttpContext.Current.Request.UserHostAddress : "";
            // ----- local test ----- //
            if (ip == "::1")
                ip = "116.73.199.88";

            //secret that was generated in key value pair  
            string secret = BLL.Tools.Constants.API.RECAPTCHA_PRIVATE_KEY;

            var reply = CommonFunction.GetStringFromURL(String.Format(Constants.API.RECAPTCHA_RESPONSE, secret, response,ip));
            var captchaResponse = BLL.Tools.Converstions.JsonDeserialize<CaptchaResponse>(reply);

            return captchaResponse;

        }

        public static Guid LogManager(String FilePath, Exception ex, String UserMsg)
        { 

            string l_constring = System.Configuration.ConfigurationManager.ConnectionStrings["CryptoExplorerConnectionString"].ConnectionString;
            string l_provider = new System.Data.SqlClient.SqlConnectionStringBuilder(l_constring).ConnectionString;
            //string l_provider = new System.Data.Entity.Core.EntityClient.EntityConnectionStringBuilder(l_constring).ProviderConnectionString;
            clsDBLogger m_odblogger = new clsDBLogger(l_constring);

            return m_odblogger.addDBLog(FilePath, ex, UserMsg);

        }

        /*public static Guid LogManager(String f_Page, Exception f_ex, String f_UserMsg, string TranscationID = null)
        {
            Guid l_Guid = Guid.NewGuid();
            var data = new ModelViews.TblLogManager.Extend();
            data.UserMsg = f_UserMsg;
            data.PageName = f_Page;
            data.TranscationID = Convert.ToInt64(TranscationID);
            data.Ex_Message = f_ex.Message == null ? null : f_ex.Message;
            data.Ex_StackTrace = f_ex.StackTrace == null ? null : f_ex.StackTrace;
            data.Ex_InnerMessage = f_ex.InnerException == null ? null : f_ex.InnerException.Message;
            data.Ex_InnerStrackTrace = f_ex.InnerException == null ? null : f_ex.InnerException.StackTrace;

            data = Commands.TblLogManager.Create(data);
            return data.GUID;
        }*/

        public class Location
        {
            public string country_code;
            public string country_name;
        }
        public static string getIPGeoCountryCode(string ip)
        {
            var json = new System.Net.WebClient().DownloadString("http://freegeoip.net/json/" + ip);
            //var location = Newtonsoft.Json.JsonConvert.DeserializeObject<Location>(json);
            var location = new JavaScriptSerializer().Deserialize<Location>(json);
            return location.country_code;
        }

        #region Encrypt Decrypt
        static private byte[] _keyByte = { };
        //Default Key
        private static string _key = ConfigurationManager.AppSettings["SHA1key"];
        //Default initial vector
        static private byte[] _ivByte = { 0x01, 0x12, 0x23, 0x34, 0x45, 0x56, 0x67, 0x78 };
        public static string Encrypt(string value)
        {
            string key = _key;
            string iv = System.Text.Encoding.UTF8.GetString(_ivByte);

            string encryptValue = string.Empty;
            MemoryStream ms = null;
            CryptoStream cs = null;

            if (!string.IsNullOrEmpty(value))
            {
                try
                {
                    if (!string.IsNullOrEmpty(key))
                    {
                        _keyByte = Encoding.UTF8.GetBytes(key.Substring(0, 8));
                        if (!string.IsNullOrEmpty(iv))
                            _ivByte = Encoding.UTF8.GetBytes(iv.Substring(0, 8));
                    }
                    else
                        _keyByte = Encoding.UTF8.GetBytes(_key);

                    using (DESCryptoServiceProvider des = new DESCryptoServiceProvider())
                    {
                        byte[] inputByteArray =
                            Encoding.UTF8.GetBytes(value);
                        ms = new MemoryStream();
                        cs = new CryptoStream(ms, des.CreateEncryptor
                        (_keyByte, _ivByte), CryptoStreamMode.Write);
                        cs.Write(inputByteArray, 0, inputByteArray.Length);
                        cs.FlushFinalBlock();
                        encryptValue = Convert.ToBase64String(ms.ToArray());
                    }
                }
                catch
                {
                    //TODO: write log 
                }
                finally
                {
                    //cs.Dispose();
                    //ms.Dispose();
                }
            }
            return encryptValue;
        }

        public static string Decrypt(string value)
        {
            string key = _key;
            string iv = System.Text.Encoding.UTF8.GetString(_ivByte);

            string decrptValue = string.Empty;
            if (!string.IsNullOrEmpty(value))
            {
                MemoryStream ms = null;
                CryptoStream cs = null;
                value = value.Replace(" ", "+");
                byte[] inputByteArray = new byte[value.Length];
                try
                {
                    if (!string.IsNullOrEmpty(key))
                    {
                        _keyByte = Encoding.UTF8.GetBytes(key.Substring(0, 8));
                        if (!string.IsNullOrEmpty(iv))
                            _ivByte = Encoding.UTF8.GetBytes(iv.Substring(0, 8));
                    }
                    else
                        _keyByte = Encoding.UTF8.GetBytes(_key);

                    using (DESCryptoServiceProvider des = new DESCryptoServiceProvider())
                    {
                        inputByteArray = Convert.FromBase64String(value);
                        ms = new MemoryStream();
                        cs = new CryptoStream(ms, des.CreateDecryptor
                        (_keyByte, _ivByte), CryptoStreamMode.Write);
                        cs.Write(inputByteArray, 0, inputByteArray.Length);
                        cs.FlushFinalBlock();
                        Encoding encoding = Encoding.UTF8;
                        decrptValue = encoding.GetString(ms.ToArray());
                    }
                }
                catch
                {
                    //TODO: write log 
                }
                finally
                {
                    cs.Dispose();
                    ms.Dispose();
                }
            }
            return decrptValue;
        }
        #endregion

        public static string GetStringFromURL(string url)
        {
            try {
                System.Net.WebClient client = new System.Net.WebClient();
                //Server.UrlDecode(url);
                string new_url = HttpUtility.UrlDecode(url);
                return client.DownloadString(new_url);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public static string GetStringFromURLWeb(string url)
        {
            string jsonString = "";
            try
            {
                // Creates an HttpWebRequest for the specified URL. 
                HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                // Sends the HttpWebRequest and waits for a response.
                HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();

                if (myHttpWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    StreamReader streamReader = new StreamReader(myHttpWebResponse.GetResponseStream());
                    jsonString = streamReader.ReadToEnd();
                }
                // Releases the resources of the response.
                myHttpWebResponse.Close();
                return jsonString;
            }
            /*
            catch (WebException e)
            {
                //Console.WriteLine("\r\nWebException Raised. The following error occured : {0}", e.Status);
                if (e.Status == WebExceptionStatus.ProtocolError)
                {
                    var response = e.Response as HttpWebResponse;
                    if (response != null)
                    {
                        Console.WriteLine("HTTP Status Code: " + (int)response.StatusCode);
                    }
                }
                return System.Net.WebException ? status = (e.Response as HttpWebResponse)?.StatusCode;
            }
            */
            catch (Exception e)
            {
                return e.Message;
            }
        }

        //public HttpResponseMessage GetResponse(string Status,string Message, object data = null) //HttpStatusCode StatusCode
        //{
        //    APIResponse response = new APIResponse(Status.ToString(), Message, data);

        //    return response;
        //}
        //public HttpResponseMessage ReturnExceptionResponse(Exception ex)
        //{
        //    //WebApiApplication.logger.Error(ex.Message, ex);
        //    APIResponse response = new APIResponse("Failure", ex == null ? "" : ex.Message);
        //    return new HttpResponseMessage()
        //    {
        //        //StatusCode = HttpStatusCode.OK,
        //        Content = new ObjectContent<object>(response, HelperUtility.GetFormatter(Request.RequestUri, Configuration), HelperUtility.GetMediaType(Request.RequestUri))
        //    };
        //}

        public static bool checkIP(string ip)
        {
            try
            {
                System.Net.WebClient client = new System.Net.WebClient();
                string address = ConfigurationManager.AppSettings["b_apiUrl"].ToString() + "?blockscript=api&api_key=" + ConfigurationManager.AppSettings["b_apiKey"].ToString() + "&action=test_ipv4&ip=" + ip;

                var reply = client.DownloadString(address);
                //string reply = client.DownloadString(Constants.URL.CHECK_IP + "?id="+ip);
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(reply);
                string jstring = JsonConvert.SerializeXmlNode(doc);

                JObject j = JObject.Parse(jstring);
                string l_ipstatus = j["response"]["status"].Value<string>();
                string l_ipblocked = j["response"]["ip"]["blocked"].Value<string>();
                if (l_ipstatus == "SUCCESS" && l_ipblocked == "YES")
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                return true;
            }
        }

        public static bool CallProcedure(DateTime utcDatetime)
        {
            using (SqlConnection con = new SqlConnection(ISDAL.DefaultConnectionString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("RecoveryICOHistory", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    // cmd.Parameters.AddWithValue("@list", tvm);
                    SqlParameter par = cmd.Parameters.AddWithValue("@BackUpDate", utcDatetime);
                   // par.SqlDbType = SqlDbType.Structured;
                    cmd.ExecuteNonQuery();
                }
                con.Close();
                return true;
            }
        }

    }
}
