namespace BLL.Tools
{
    public class Constants
    {
        public static class PageName
        {
            public const string CMSManage = "CMSManage";
            public const string Users = "Users";
            public const string Dashboard = "Dashboard";
            public const string ICO = "ICO";
            public const string Subscriber = "Subscriber";
            public const string BroadcastMail = "BroadcastMail";
            public const string NewsManage = "NewsManage";

        }
        public static class Types
        {
            public static class CMS
            {
                public const string PAGE = "Page";
                public const string ADS = "Ads";
                public const string MAIL = "Mail";
            }
            public static class Group
            {
                public const string ICO = "ICO";
                public const string OTHER = "Other";
            }

            public static class Stage
            {
                public const string ICO = "ICO";
                public const string EXCHANGE = "EXCHANGE";
                public const string ICO_TODAY = "ICO TODAY";
                public const string ENDED = "ENDED";
                public const string SCAM = "SCAM";
                public const string CLOSED = "CLOSED";
                public const string LENDING = "LENDING";

            }

            public static class ICOCategory
            {
                public const string LENDING = "Lending";
                public const string OTHER = "Other";
            }
        }
       
      
        public static class Table
        {
            public const string ICO = "ICO";
            public const string CMS = "CMS";
            public const string SUBCRIBER = "Subcriber";
        }
        public static class Rebill_FrequencyUnit
        {
            public const string DAY = "DAY";
            public const string WEEK = "WEEK";
            public const string MONTH = "MONTH";
            public const string YEAR = "YEAR";
        }
        public static class ValidationMessage
        {
            public const string CREATE_SUCCESS = "Created Successful.";
            public const string CREATE_FAIL = "Create fAil!";
            public const string UPDATE_SUCCESS = "Data Updated Successful";
            public const string UPDATE_FAIL = "Data Update fail!";

            public const string DATA_UNMATCHED = "data Not match, verification failed. ";
            public const string DATA_NOT_FOUND = "data not found in our system. contact support team.";
            public static class User
            {
                public const string LOGINSUCCESS = "Login Successful, Welcome ";
                public const string USER_STATUS = "This Account is ";
                public const string LOGINFAIL = "Login Fail!";
                public const string EMAIL_TAKEN = "Email address already taken!";
                public const string PHONE_TAKEN = "Phone number already taken!";
                public const string PASSWORD_REQUIRED = "Password lenght must be between 8 and 16!";
                public const string COUNTRY_REQUIRED = "Selected country is unmatched with your current location!";
                public const string VERIFIVATION_SUCCESS = "User Verified Successfully!";
                public const string USER_CREATE_SUCCESS = "Signup successful! - Email address verification needed. Before you can login, please check your email to activate your user account.";
                public const string VALIDATION_FAIL = "Validation Fail!";
                public const string VALIDATION_MAIL_FAIL = "Sorry!! We couldn't albe to sent email. Make sure you email address is active. we rollback your information so you can retry again.";
            }
        }
        
        public static class Role
        {
            public const string ADMIN = "Admin";
            public const string USER = "User";
            public const string STAFF = "Staff";
            public const string SUBADMIN = "SubAdmin";
        }
        
        public static class Status
        {
            public const string APPROVED = "Approved";
            public const string REJECTED = "Rejected";
            public const string ACTIVE = "Active";
            public const string DEACTIVE = "Deactive";
            public const string AVAILABLE = "Available";
            public const string UNVERIFIED = "Unverified";
            public const string DELETED = "Deleted";
            public const string UNREAD = "Unread";
            public const string READED = "Readed";
        }
        public static class PaymentStatus
        {
            public const string PAID = "Paid";
            public const string UNPAID = "Unpaid";
            public const string REJECTED = "Rejected";
            public const string DELETED = "Deleted";
        }

        public static class SMTP_MAIL
        {
            public static string USERNAME {
                get{ 
                return System.Configuration.ConfigurationManager.AppSettings["smtpUser"]; 
                }
            }
            public static string PASSWORD
            {
                get
                {
                    return System.Configuration.ConfigurationManager.AppSettings["smtpPass"];
                }
            }

            public static string SMTP_SERVER
            {
                get
                {
                    return System.Configuration.ConfigurationManager.AppSettings["smtpServer"];
                }
            }
            public static int SMTP_PORT
            {
                get
                {
                    return System.Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["smtpPort"]);
                }
            }
            public static bool ENABLE_SSL
            {
                get
                {
                    return System.Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["EnableSsl"]);
                }
            }

            public static string FROM_MAIL_ADDRESS
            {
                get
                {
                    return System.Configuration.ConfigurationManager.AppSettings["adminEmail"];
                }
            }
        }

        public static class API
        {
            //public const string CoinMarketCap = "https://api.coinmarketcap.com/v1/ticker/";
            public static string CoinMarketCap
            {
                get
                {
                    return System.Configuration.ConfigurationManager.AppSettings["CoinMarketCap"];
                }
            }

            public static string CryptoCompare
            {
                get
                {
                    return System.Configuration.ConfigurationManager.AppSettings["CryptoCompare"];
                }
            }

            public static string CryptoCompare_Symbol
            {
                get
                {
                    return System.Configuration.ConfigurationManager.AppSettings["CryptoCompare_Symbol"];
                }

            }

            public static string IcoReview
            {
                get
                {
                    return System.Configuration.ConfigurationManager.AppSettings["IcoReview"];
                }
            }

            public static string CryptoNator
            {
                get
                {
                    return System.Configuration.ConfigurationManager.AppSettings["CryptoNator"];
                }
            }

            public static string RECAPTCHA_RESPONSE
            {
                get
                {
                    return System.Configuration.ConfigurationManager.AppSettings["RECAPTCHA_RESPONSE"];
                }
            }
            public static string RECAPTCHA_PUBLIC_KEY
            {
                get
                {
                    return System.Configuration.ConfigurationManager.AppSettings["RECAPTCHA_PUBLIC_KEY"];
                }
            }
            public static string RECAPTCHA_PRIVATE_KEY
            {
                get
                {
                    return System.Configuration.ConfigurationManager.AppSettings["RECAPTCHA_PRIVATE_KEY"];
                }
            }
        }

        public static class SITE_URL
        {
            //public const string URL = "";
            public const string URL = "http://localhost:54821/";
            public const string SIGN_UP_LINK = "";
            public const string CONTACT_EMAIL = "cryptoexplore17@gmail.com";

            public const string SITE_NAME = "Crypto Explorer";
            public const string SITE_MINILOGO = "/images/favicon-96x96.png";
            public const string SITE_WHITELOGO = "/images/w2voucher_white.png";
            public const string SITE_LOGO = "/images/w2pvoucher.png";
            public const string TermsCondition = "https://w2payvoucher.com/Home/TermsConditions/";
            public static string ShortUrl_TermsCondition = "";// Mailer.CreateShorturl("https://w2payvoucher.com/Home/TermsConditions/");
            public const string VERIFICATION_URL = URL + "api/Login/Verification/";
            public static string API { get {
                    return System.Configuration.ConfigurationManager.AppSettings["APIURL"];//  "http://api.crypto-explore.com/api/";// "http://localhost:11359/api/";
                }  } 

            public const string DEFAULT_USER_LOGO = "defaultuser.png";
            public const string DEFAULT_IMAGE= "no-image-found.jpg";
        }

    }
}