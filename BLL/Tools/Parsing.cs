﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Tools
{
    using System.Text.RegularExpressions;

    public class Parsing
    {
        public static bool IsNullOrEmpty(string str)
        {
            bool isempty = String.IsNullOrEmpty(str);

            return isempty;

        }
        public static DateTime ParseDateTimeUTC(string dateValue)
        {
            DateTime convertedDate;
            string dateFormat = string.Empty;
            //Triming PDT from end  Example:Tue, 19 Sep 2017 16:49:46 -0700 (PDT)
            int l_iIndex = dateValue.LastIndexOf("(");
            if (l_iIndex > 0)
                dateValue = dateValue.Substring(0, l_iIndex).TrimEnd();

            try
            {
                //Convert to UNIVERSAL TIME ZONE 
                convertedDate = Convert.ToDateTime(dateValue);
                convertedDate = convertedDate.ToUniversalTime(); //DateTime.SpecifyKind(convertedDate, DateTimeKind.Utc);
            }
            catch (Exception)
            {
                convertedDate = DateTime.UtcNow;
            }
            return convertedDate;
        }

        public static bool IsValidNumber(string strNumber)
        {
            Regex objPattern = new Regex(@"^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?");
            return objPattern.IsMatch(strNumber);
        }

        public static bool IsValidPassword(string strPassword)
        {
            Regex objPattern = new Regex(@"^\s{8,16}?");
            return objPattern.IsMatch(strPassword);
        }

        public static bool IsDecimalNumber(string strNumber)
        {
            //Regex objPattern = new Regex(@"^\d{1,3}(\.\d{0,2})?$");
            //return objPattern.IsMatch(strNumber);
            decimal tmp;
            return Decimal.TryParse(strNumber, out tmp);
        }

        public static bool IsValidEmailAddressString(string strToCheck)
        {
            string sEmailPatterns = @"^(([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+([;.](([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+)*$";
            Regex objEmailPattern = new Regex(sEmailPatterns);
            return objEmailPattern.IsMatch(strToCheck);
        }

        public static bool IsValidEmailAddressCSV(string strToCheck)
        {
            strToCheck = strToCheck.Replace(" ", "").Replace(",", ";").Trim(";".ToCharArray());
            string sEmailPatterns =
                @"^(([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+([;.](([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+)*$";
            Regex objEmailPattern = new Regex(sEmailPatterns);
            return objEmailPattern.IsMatch(strToCheck);
        }

        public static bool IsValidUrl(string strToCheck)
        {
            string sUrlPatterns =
                @"^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$";
            Regex objUrlPattern = new Regex(sUrlPatterns);
            return objUrlPattern.IsMatch(strToCheck);
        }

        public static bool IsStringGuid(string str)
        {
            if (str == null) return false;

            var isGuid =
                new Regex(
                    @"^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$",
                    RegexOptions.Compiled);
            return (isGuid.IsMatch(str));
        }

        public static bool IsTimeMinutesValid(string strTimeField)
        {
            // Time textbox - Check for minutes > .5 .50 only allowed
            string mins = strTimeField.Substring(strTimeField.IndexOf('.'));
            return (mins == ".5" || mins == ".0" || mins == ".50" || mins == ".00");
        }
    }
}
