﻿using BLL.ModelViews;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Validations
{
    public class ForgotPasswordValidator
    {
        public static List<ValidationView> Validate(ModelViews.User.Extend view)
        {

            var error = new List<ValidationView>();

            if (view.UserName == null || string.IsNullOrEmpty(view.UserName.ToString()))
            {
                var validationview = new ValidationView();
                validationview.ValidationField = "UserName";
                validationview.ValidationMessage = "UserName/Email is required";
                error.Add(validationview);
            }

            //if (view.Email == null || string.IsNullOrEmpty(view.Email.ToString()))
            //{
            //    var validationview = new ValidationView();
            //    validationview.ValidationField = "Email";
            //    validationview.ValidationMessage = "Email is required";
            //    error.Add(validationview);
            //}

            return error;
        }
    }
  }
