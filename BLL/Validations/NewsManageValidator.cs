﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.ModelViews;

namespace BLL.Validations
{
    public class NewsManage
    {
        public static List<ValidationView> Validate(ModelViews.NewsManage.Extend view)
        {

            var error = new List<ValidationView>();

            if (view.ID == null || string.IsNullOrEmpty(view.ID.ToString()))
            {
                var validationview = new ValidationView();
                validationview.ValidationField = "ID";
                validationview.ValidationMessage = "ID is required";
                error.Add(validationview);
            }

            if (view.HeaderImage == null || string.IsNullOrEmpty(view.HeaderImage.ToString()))
            {
                var validationview = new ValidationView();
                validationview.ValidationField = "HeaderImage";
                validationview.ValidationMessage = "HeaderImage is required";
                error.Add(validationview);
            }

            if (view.Author == null || string.IsNullOrEmpty(view.Author.ToString()))
            {
                var validationview = new ValidationView();
                validationview.ValidationField = "Author";
                validationview.ValidationMessage = "Author is required";
                error.Add(validationview);
            }

            if (view.Title == null || string.IsNullOrEmpty(view.Title.ToString()))
            {
                var validationview = new ValidationView();
                validationview.ValidationField = "Title";
                validationview.ValidationMessage = "Title is required";
                error.Add(validationview);
            }

            if (view.ShortDescription == null || string.IsNullOrEmpty(view.ShortDescription.ToString()))
            {
                var validationview = new ValidationView();
                validationview.ValidationField = "ShortDescription";
                validationview.ValidationMessage = "ShortDescription is required";
                error.Add(validationview);
            }

            if (view.LongDescription == null || string.IsNullOrEmpty(view.LongDescription.ToString()))
            {
                var validationview = new ValidationView();
                validationview.ValidationField = "LongDescription";
                validationview.ValidationMessage = "LongDescription is required";
                error.Add(validationview);
            }

            if (view.StartEventDate == null || string.IsNullOrEmpty(view.StartEventDate.ToString()))
            {
                var validationview = new ValidationView();
                validationview.ValidationField = "StartEventDate";
                validationview.ValidationMessage = "StartEventDate is required";
                error.Add(validationview);
            }

            if (view.EndEventDate == null || string.IsNullOrEmpty(view.EndEventDate.ToString()))
            {
                var validationview = new ValidationView();
                validationview.ValidationField = "EndEventDate";
                validationview.ValidationMessage = "EndEventDate is required";
                error.Add(validationview);
            }

            //if (view.Sort == null || string.IsNullOrEmpty(view.Sort.ToString()))
            //{
            //    var validationview = new ValidationView();
            //    validationview.ValidationField = "CreatedDate";
            //    validationview.ValidationMessage = "CreatedDate is required";
            //    error.Add(validationview);
            //}

            if (view.CreatedDate == null || string.IsNullOrEmpty(view.CreatedDate.ToString()))
            {
                var validationview = new ValidationView();
                validationview.ValidationField = "CreatedDate";
                validationview.ValidationMessage = "CreatedDate is required";
                error.Add(validationview);
            }

            if (view.UpdatedDate == null || string.IsNullOrEmpty(view.UpdatedDate.ToString()))
            {
                var validationview = new ValidationView();
                validationview.ValidationField = "UpdatedDate";
                validationview.ValidationMessage = "UpdatedDate is required";
                error.Add(validationview);
            }

            return error;
        }
    }
}
