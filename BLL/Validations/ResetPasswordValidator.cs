﻿using BLL.ModelViews;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Validations
{
    public class ResetPassword
    {
        public static List<ValidationView> Validate(BLL.ModelViews.ResetPassword view)
        {

            var error = new List<ValidationView>();

            if (view.Password == null || string.IsNullOrEmpty(view.Password.ToString()))
            {
                var validationview = new ValidationView();
                validationview.ValidationField = "Password";
                validationview.ValidationMessage = "Password is required";
                error.Add(validationview);
            }
            if (view.ConfirmNewPassword == null || string.IsNullOrEmpty(view.ConfirmNewPassword.ToString()))
            {
                var validationview = new ValidationView();
                validationview.ValidationField = "ConfirmNewPassword";
                validationview.ValidationMessage = "ConfirmNewPassword is required";
                error.Add(validationview);
            }

            //if (view.ReturnToken == null || string.IsNullOrEmpty(view.ReturnToken.ToString()))
            //{
            //    var validationview = new ValidationView();
            //    validationview.ValidationField = "ReturnToken";
            //    validationview.ValidationMessage = "ReturnToken is required";
            //    error.Add(validationview);
            //}

            return error;
        }
    }
}
