﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Globalization;

namespace WICMS.Domain.Entities
{
    public class AppSettings
    {
        public static DateTime FinModuleStartDate
        {
            get
            {
                string strDate = ConfigurationManager.AppSettings["StartDateForFinanceModule"];

                DateTime finStartDate;
                if (DateTime.TryParse(strDate, out finStartDate))
                {
                    return finStartDate;
                }
                else
                    return DateTime.Now;
            }
        }

        public static DateTime FindDefaultExpiryDate
        {
            get
            {
                string dt = ConfigurationManager.AppSettings["DefaultExpiryDate"].ToString();
                if (string.IsNullOrWhiteSpace(dt))
                {
                    return DateTime.Now.AddMonths(6);
                }
                else
                    return DateTime.ParseExact(dt, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
        }

        public static string MergedInvoicePreifixAlias
        {
            get
            {
                return ConfigurationManager.AppSettings["MergedInvoicePrefix"].ToString();
            }
        }

        public static string WICMS_URL
        {
            get
            {
                return ConfigurationManager.AppSettings["WICMSLogo"];
            }
        }
    }
}