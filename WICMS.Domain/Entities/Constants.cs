﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WICMS.Domain.Entities
{
    public class Constants
    {
        public class MemberType
        {
            public const string MEMBERTYPE_STAFF = "Staff";
        }
        public class EntityName
        {
            public const string CLIENT = "Client";
            public const string UNMAPEDCLIENT = "UnMaped Client";
            public const string UNIVERSITY = "University";
            public const string OPPORTUNITY = "Opportunity";
            public const string SUPPLIER = "Supplier";
            public const string EMPLOYER = "Employer";
            public const string CASE = "Case";
            public const string INSTITUTION = "Institution";
        }

        public class AccessLevelValues
        {
            public const int NO_ACCESS = -1;
            public const int READ_ONLY = 0;
            public const int FULL_ACCESS = 1;
            public const int Edit_Only = 2;

            public const int ADD_EDIT_ONLY = 3;
            public const int ADD_EDIT_DELETE = 4;

            public const string OFFICE_ONLY = "Office";
            public const string USER_ONLY = "User";
        }
    }

    
}
