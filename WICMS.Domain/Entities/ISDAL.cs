﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Linq;
using System.Linq;
using WICMS.Domain.Entities;
using System.Linq.Expressions;
using System.Reflection;
using WICMS.Domain.Utils;
using System.Collections;
using System.Linq.Dynamic;
using System.Globalization;
using System.Text.RegularExpressions;
using WICMS.Domain.Poco;
using System.Web;

namespace WICMS.Domain
{
    public class ResultInfo
    {
        public Object Data { get; set; }
        public int ResultCode { get; set; }
        public Exception ex { get; set; }
        public String CustomMessage { get; set; }
    }
    public class ISDAL
    {
        public static string DefaultConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString; }
        }

        public static string CryptoExplorerConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["CryptoExplorerConnectionString"].ConnectionString; }
        }

        public static WiCmsDataContext GetDataContext()
        {
            return new WiCmsDataContext(CryptoExplorerConnectionString);
        }

        //public static WICMSDataContext GetDataContextAWS()
        //{
        //    return new WICMSDataContext(CryptoExplorerConnectionString);
        //}

        public static T GetPropertyValue<T>(object obj, string propertyName)
        {
            if (obj == null)
                return default(T);

            PropertyInfo pInfo = obj.GetType().GetProperty(propertyName);

            if (pInfo == null)
                return default(T);

            return (T)Convert.ChangeType(pInfo.GetValue(obj, null), typeof(T));
        }
         
    } 

}