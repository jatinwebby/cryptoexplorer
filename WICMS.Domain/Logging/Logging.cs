﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;

namespace WICMS.Domain.Logging
{
    public class Logging
    {
        public static void Log(Exception ex)
        {
            //TODO: Log exception
            if (ex != null)
            {
                CreateLog(ex.Message);
            }
        }

        public static void Log(string ex)
        {
            //TODO: Log exception
            if (!string.IsNullOrEmpty(ex))
                CreateLog(ex);
        }

        private static void CreateLog(string logInfo)
        {
            bool doLoging = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableLogging"]);
            if (doLoging)
            {
                string path = System.Web.HttpContext.Current.Server.MapPath(@"\App_Data\LogInfo" + DateTime.Now.ToString("ddMMyyyy") + ".txt");

                if (!File.Exists(path))
                {
                    File.Create(path);
                }

                if (!File.Exists(path))
                {
                    using (FileStream fs = new FileStream(path, FileMode.Create))
                    {

                    }
                }

                using (StreamWriter writer = new StreamWriter(path, true))
                {
                    writer.Write(logInfo);
                }
            }
        }
    }
}
