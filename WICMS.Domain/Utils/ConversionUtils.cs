﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WICMS.Domain.Utils
{
    public static class ConversionUtils
    {
        public const string Chars = "abcdefghijklmnopqrstuvwxyz0123456789";

        public static String Encode(long input)
        {
            if (input < 0)
                throw new ArgumentOutOfRangeException("input", input, "Input cannot be negative");

            char[] clistArray = Chars.ToCharArray();
            var result = new Stack<char>();
            while (input != 0)
            {
                result.Push(clistArray[input % 36]);
                input /= 36;
            }
            return new string(result.ToArray());
        }

        public static Int64 Decode(string input)
        {
            var reversed = input.ToLower().Reverse();
            long result = 0;
            int pos = 0;
            foreach (char c in reversed)
            {
                result += Chars.IndexOf(c) * (long)Math.Pow(36, pos);
                pos++;
            }
            return result;
        }
    }
}
