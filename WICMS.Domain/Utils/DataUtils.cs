﻿using System;
using System.Collections.Generic;
using ImmiSys.DAL;
using ImmiSys.Domain.Entities;
using System.Text;
using System.Web.Security;
using System.IO;
using ImmiSys.Domain.Managers;

namespace ImmiSys.Domain.Utils
{
    public class DataUtils
    {
        public static List<CountryGeo> GetCountryGeoList()
        {
            return ISDAL.GetAllCountriesGeo();
        }

        public static List<StateGeo> GetStateGeoList(long countryGeoId)
        {
            if (countryGeoId > 0)
            {
                return ISDAL.GetStates(countryGeoId);
            }
            else
                return new List<StateGeo>();

        }

        public static List<CountryVevo> GetCountryVevoList()
        {
            return ISDAL.GetCountriesVevo();
        }

        public static List<CountryCurrency> GetCountryCurrencyList()
        {
            return ISDAL.GetCountriesCurrency();
        }

        public static List<HelpMainTopic> GetHelpMainTopicList()
        {
            return ISDAL.GetHelpMainTopics();
        }

        public static List<string> GetSalutations()
        {
            List<string> salutations = new List<string>();

            salutations.Add("Mr");
            salutations.Add("Mrs");
            salutations.Add("Ms");
            salutations.Add("Master");
            salutations.Add("Miss");
            salutations.Add("Dr");

            return salutations;
        }

        public static List<DocumentRequiredSet> GetDocumentRequiredSets()
        {
            return ISDAL.GetDocumentRequiredSetList();
        }

        public static string GeneratePostalAddress(PostalAddress dbObj)
        {
            string address = string.Empty;
            if (!string.IsNullOrWhiteSpace(dbObj.AddressLine1))
            {
                address = address + dbObj.AddressLine1 + Environment.NewLine;
            }
            if (!string.IsNullOrWhiteSpace(dbObj.AddressLine2))
            {
                address = address + dbObj.AddressLine2 + Environment.NewLine;
            }
            if (!string.IsNullOrWhiteSpace(dbObj.City))
            {
                address = address + dbObj.City + Environment.NewLine;
            }
            if (!string.IsNullOrWhiteSpace(ISDAL.FindStateNameByStateId(Convert.ToInt64(dbObj.StateId))))
            {
                address = address + ISDAL.FindStateNameByStateId(Convert.ToInt64(dbObj.StateId)) + " "; //  +Environment.NewLine;
            }
            if (!string.IsNullOrWhiteSpace(dbObj.PostCode))
            {
                address = address + dbObj.PostCode + Environment.NewLine;
            }
            return address;
        }

        public static string GeneratePostalAddress(Company dbObj)
        {
            string address = string.Empty;
            if (!string.IsNullOrWhiteSpace(dbObj.Address))
            {
                address = address + dbObj.Address + Environment.NewLine;
            }
            if (!string.IsNullOrWhiteSpace(dbObj.City))
            {
                address = address + dbObj.City + Environment.NewLine;
            }
            if (dbObj.StateGeoId!=null)
            {
                address = address + dbObj.StateGeo.Name + (dbObj.PostCode != null ? " " + dbObj.PostCode : "") + Environment.NewLine;
            }
            if (dbObj.CountryGeoId.GetValueOrDefault() > 0)
            {
                var dbCountry = ISDAL.FindCountry(dbObj.CountryGeoId.GetValueOrDefault());
                if (dbCountry != null)
                    address = address + dbCountry.Name;
            }
            return address;
        }

        public static List<string> GetAddressTypes()
        {
            List<string> addressTypes = new List<string>();
            addressTypes.Add("Residential");
            addressTypes.Add("Postal");
            addressTypes.Add("Overseas");
            addressTypes.Add("Other");

            return addressTypes;
        }

        public static List<string> GetOpportunityStatuses()
        {
            List<string> salutations = new List<string>();

            salutations.Add("Open");
            salutations.Add("Converted");
            salutations.Add("Lost");
            salutations.Add("Disqualified");

            return salutations;
        }

        public static Dictionary<string, string> GetOpportunityPossibilities()
        {
            Dictionary<string, string> possibilities = new Dictionary<string, string>();

            possibilities.Add("0", "0 - Not Possible");
            possibilities.Add("1", "1 - Cold Lead");
            possibilities.Add("2", "2 - Warm Lead");
            possibilities.Add("3", "3 - Hot Lead");

            return possibilities;
        }

        public static int? CalculateAge(DateTime? birthDate)
        {
            if (birthDate != null && !DateTime.MinValue.Equals(birthDate))
            {
                return CalculateAge(birthDate.GetValueOrDefault(), DateTime.Today);
            }
            return null;
        }

        public static int CalculateAge(DateTime birthDate, DateTime compareToDate)
        {
            int age = compareToDate.Year - birthDate.Year;
            if (birthDate > compareToDate.AddYears(-age))
                age--;
            return age;
        }

        public static string FormatName(string givenNames, string familyName, string preferredName)
        {
            string name = "(no name)";

            //get client name
            if (!string.IsNullOrWhiteSpace(givenNames))
            {
                int commaPos = givenNames.IndexOf(',');
                name = givenNames.Substring(0, commaPos >= 0 ? commaPos : givenNames.Length);

                if (!string.IsNullOrWhiteSpace(familyName))
                    name = string.Format("{0} {1}", name, familyName);
            }
            else if (!string.IsNullOrWhiteSpace(preferredName))
            {
                name = preferredName;
            }
            return name;
        }

        public static string FormatStaffName(string firstName, string lastName, string preferredName)
        {
            string name = "(no name)";

            //get client name
            if (!string.IsNullOrWhiteSpace(firstName))
            {
                int commaPos = firstName.IndexOf(',');
                name = firstName.Substring(0, commaPos >= 0 ? commaPos : firstName.Length);

                if (!string.IsNullOrWhiteSpace(lastName))
                    name = string.Format("{0} {1}", name, lastName);
            }
            else if (!string.IsNullOrWhiteSpace(preferredName))
            {
                name = preferredName;
            }
            return name;
        }

        public static string FormatName(string givenName, string familyName)
        {
            string name = string.Empty;

            if (!string.IsNullOrEmpty(givenName))
                name = name + givenName + " ";

            if (!string.IsNullOrEmpty(familyName))
                name = name + familyName;

            if (string.IsNullOrEmpty(name))
                name = "Not Avaiable";

            return name;
        }

        public static Dictionary<long, string> GetOpportunityCategories()
        {
            return ISDAL.GetOpportunityCategories();
        }

        public static object GetPropValue(object src, string propName)
        {
            return src.GetType().GetProperty(propName).GetValue(src, null);
        }

        public static string GetBankName(string bankName, string accountNo, string holderName)
        {
            string resultStr = bankName;
            if ((!string.IsNullOrEmpty(accountNo)) && (!string.IsNullOrEmpty(holderName)))
            {
                resultStr = resultStr + " - (" + holderName + " " + accountNo + " )";
            }
            return resultStr;
        }

        public static string GetCaseDetails(string caseReferenceNo, string caseType)
        {
            if ((!string.IsNullOrEmpty(caseReferenceNo)) && (!string.IsNullOrEmpty(caseType)))
            {
                return caseReferenceNo + " - " + caseType;
            }
            else if ((!string.IsNullOrEmpty(caseReferenceNo)) && (string.IsNullOrEmpty(caseType)))
            {
                return caseReferenceNo;
            }
            else if ((string.IsNullOrEmpty(caseReferenceNo)) && (!string.IsNullOrEmpty(caseType)))
            {
                return caseType;
            }
            else
                return null;
        }

        public static string GetClientDetails(string p1, string p2)
        {
            throw new NotImplementedException();
        }

        public static string GetTime(decimal totalMin)
        {
            TimeSpan timeSpan = TimeSpan.FromMinutes((int)totalMin);
            return timeSpan.ToString(@"hh\:mm");
            //decimal hours = (totalMin / 60 >= 1) ? totalMin/60 : 0 ;
            //decimal minutes = totalMin % 60;
            ////return string.Format(hours + ":" + minutes);
            //return string.Format("{0:00}:{1:00}", totalMin / 60, totalMin % 60);
        }

        public static string GetTimeHours(decimal su)
        {
            return string.Format("{0}", Math.Round((su / 60), 2).ToString("N2"));
        }

        public static string GeneratePassword()
        {
            Random random = new Random();
            return random.Next(100001, 999999).ToString();
        }

        public static string GenerateFeedbackMail(string customerDetails, string browserDetails, string rating, string attachment, string description, string title)
        {
            string content = string.Empty;

            content = "Feedback from " + customerDetails + "<br/><br/>";
            content += "Rating : " + rating + "<br/><br/>";
            content += "Feedback Title : " + title + "<br/><br/>";
            content += "Description : " + description + "<br/><br/>";
            if (!string.IsNullOrEmpty(attachment))
                content += "Attachment : " + "<a href='" + attachment + "'" + "download>Download</a>" + "<br/><br/>";
            content += "Browser details : <br/>" + browserDetails + "<br/><br/>";
            content += "Submitted On : " + DateTime.Now + "<br/>";
            return content;
        }

        public static string GenerateSelfRegistrationContent(string p, string email, string tempPassword, long clientId, long staffId)
        {
            Staff staff = ISDAL.FindStaff(staffId);
            Client dbClient = ISDAL.FindClient(clientId);
            if (dbClient != null)
            {
                string clientName = DataUtils.FormatName(dbClient.GivenNames, dbClient.FamilyName);

                var dbCustomer = ISDAL.FindCustomer(ISDAL.FindCustomerIdByClientId(clientId));

                string companyName = "N.A";

                if (dbCustomer != null)
                    companyName = dbCustomer.OrganizationName;

                string emailWithPassword = email + "~" + tempPassword;

                string token = p + "/Account/VerifyMe?token=" + SecurityUtils.Encrypt(emailWithPassword);

                string template = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/WalkinPortal.txt"));

                template = template.Replace("[APPLICANT_NAME]", clientName);
                template = template.Replace("[TOKEN]", token);
                template = template.Replace("[LOGIN]", email);
                template = template.Replace("[PASSWORD]", tempPassword);
                template = template.Replace("[COMPANY_NAME]", companyName);
                template = template.Replace("[SENDER]", (staff != null ? staff.PreferredName : "ImmiSys Team"));

                var dbTemnplateSetting = ISDAL.FindTemplateSettingByCustomer(staff.CustomerId.GetValueOrDefault());
                if (dbTemnplateSetting != null)
                {
                    if (!string.IsNullOrWhiteSpace(dbTemnplateSetting.S3Link))
                    {
                        template = template.Replace("[LOGO_URL]", dbTemnplateSetting.S3Link);
                    }
                    else
                    {
                        template = template.Replace("[LOGO_URL]", AppSettings.IMMISYS_URL);
                    }
                }

                return template;
            }
            else
                return null;

        }

        public static string GenerateClientRegistrationContent(string p, string email, string givenName, long cId, long clientId, string tempPassword)
        {
            var dbCustomer = ISDAL.FindCustomer(cId);
            string token = p + "/Account/VerifyClient?token=" + SecurityUtils.Encrypt(clientId);

            string template = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/WalkinPortal.txt"));

            template = template.Replace("[APPLICANT_NAME]", givenName);
            template = template.Replace("[TOKEN]", token);
            template = template.Replace("[LOGIN]", email);
            template = template.Replace("[PASSWORD]", tempPassword);
            template = template.Replace("[SENDER]", "ImmiSys Team");
            template = template.Replace("[COMPANY_NAME]", dbCustomer.OrganizationName);

            var dbTemnplateSetting = ISDAL.FindTemplateSettingByCustomer(cId);
            if (dbTemnplateSetting != null)
            {
                if (!string.IsNullOrWhiteSpace(dbTemnplateSetting.S3Link))
                {
                    template = template.Replace("[LOGO_URL]", dbTemnplateSetting.S3Link);
                }
                else
                {
                    template = template.Replace("[LOGO_URL]", AppSettings.IMMISYS_URL);
                }
            }

            return template;
            //return "Dear " + givenName + " ,<br/>" + "<br/>" +
            //   "Please click the link below to add or edit your client information." + "<br/>" + "<br/>" +
            //   "Url: <a href='" + p + "/Account/VerifyClient?token=" + SecurityUtils.Encrypt(clientId) + "' target='_blank'> " + p + "/Account/VerifyClient?token=" + SecurityUtils.Encrypt(clientId) + "</a><br/>" +
            //           "User Login : " + email + "<br/>" +
            //           "Password : " + tempPassword + "<br/>" + "<br/>" + "<br />" +
            //           "Best regards" + "<br />" + "ImmiSys Team"; //(customerName != null ? customerName : "ImmiSys Team");
        }

        public static string GenerateSubAgentRegistrationContent(string p, string email, string name, long cId, long subagentId)
        {
            var customerName = ISDAL.GetCustomerNameById(cId);
            return "Dear " + name + " ,<br/>" + "<br/>" +
               "Please click the following link to reset your password." + "<br/>" + "<br/>" +
               "Reset Password link" + "<br/>" +
               "Url:" + p + "/Account/VerifySubAgent?cId=" + SecurityUtils.Encrypt(cId) + "&id=" + SecurityUtils.Encrypt(subagentId) + "<br /><br />"
                       +
                       "Best regards" + "<br/>" + "ImmiSys Team"; //(customerName != null ? customerName : "ImmiSys Team");
        }

        public static string GenerateSubAgentRegistrationEmailContent(string p, string email, string tempPassword, long subagentId, string staffName, long CurrentCustomerId)
        {
            return "Welcome To ImmiSys," + "<br/>" + "<br/>" +
                     "We have created a user log in for you. Please click the link below to access your subagent portal." + "<br/>" + "<br/>" +
                     "Url: <a href='" + p + "/Account/VerifySubAgent?cId=" + SecurityUtils.Encrypt(CurrentCustomerId) + "&id=" + SecurityUtils.Encrypt(subagentId) + "' target='_blank'>" + p + "/Account/VerifySubAgent?cId=" + SecurityUtils.Encrypt(CurrentCustomerId) + "&id=" + SecurityUtils.Encrypt(subagentId) + "</a><br />"
                             + "User Login : " + email + "<br/>" +
                             "Password : " + tempPassword + "<br/>" + "<br/>" + "<br />" +

                             "Best regards" + "<br/>" + (staffName != null ? staffName : "ImmiSys Team");
        }

        public static string GenerateRecurringEmailContent(string customernName, decimal intialAmountTotal, int LicenceCount,bool isCustomerLogedIn=false)
        {
            if (customernName != null)
            {
                string template = "";
                //template = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/RecurringPaymentSuccessfully.txt"));            
                    //var path = System.IO.Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "App_Data\\RecurringPaymentSuccessfully.txt");
                    var path = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/RecurringPaymentSuccessfully.txt");
                    template = File.ReadAllText(path);             
                    template = template.Replace("[APPLICANT_NAME]", customernName);
                    template = template.Replace("[intialAmountTotal]", Convert.ToString(intialAmountTotal));
                    template = template.Replace("[LicenceCount]", Convert.ToString(LicenceCount));
                
                return template;
            }
            else
            {
                return null;
            }
        }
        public static string GenerateRecurringFailedEmailContent(string customernName, decimal intialAmountTotal, int LicenceCount,string weblink = null, string Date = null)
        {
            if (customernName != null)
            {
                string template = "";
                // template = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/RecurringPaymentFailed.txt"));
                var path = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/RecurringPaymentFailed.txt");
                template = File.ReadAllText(path);
                template = template.Replace("[APPLICANT_NAME]", customernName);
                template = template.Replace("[weblink]", weblink);
                template = template.Replace("[insertdate]", Date);                
                //template = template.Replace("[intialAmountTotal]", Convert.ToString(intialAmountTotal));
                //template = template.Replace("[LicenceCount]", Convert.ToString(LicenceCount));             
                return template;
            }
            else
            {
                return null;
            }
        }

        public static string GenerateConfirmationMailContent(string applicantName,Int32 OldLicenceCount,Int32 UpdatedLicenceCount)
        {
            if (applicantName != null)
            {
                string template = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/LicenceConfirmationMail.txt"));
                template = template.Replace("[APPLICANT_NAME]", applicantName);
                template = template.Replace("[OldLicenceCount]", Convert.ToString(OldLicenceCount));
                template = template.Replace("[UpdatedLicenceCount]", Convert.ToString(UpdatedLicenceCount));
                return template;
            }
            else
                return null;
        }

        public static string GenerateRenewConfirmationMailContent(string applicantName, Int32 LicenceCount)
        {
            if (applicantName != null)
            {
                string template = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/RenewLicenceConfirmationMail.txt"));
                template = template.Replace("[APPLICANT_NAME]", applicantName); 
                template = template.Replace("[TOTAL_LICENCE]", Convert.ToString(LicenceCount));
                return template;
            }
            else
                return null;
        }

        public static string GetForgetPasswordContent(string resetToken, string p)
        {
            if (Roles.IsUserInRole(p, Constants.ROLE_CUSTOMER))
            {
                Customer cs = ISDAL.FindCustomerByEmail(p);
                if (cs != null)
                {

                    string template = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/ResetPassword.txt"));

                    template = template.Replace("[APPLICANT_NAME]", cs.ContactName);
                    template = template.Replace("[TOKEN]", resetToken);

                    return template;
                }
                else
                    return null;
            }
            else if (Roles.IsUserInRole(p, Constants.ROLE_STAFF))
            {
                string staffName = ISDAL.FindStaffByEmail(p);
                if (staffName != null)
                {
                    string template = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/ResetPassword.txt"));

                    template = template.Replace("[APPLICANT_NAME]", staffName);
                    template = template.Replace("[TOKEN]", resetToken);

                    return template;
                }
                else
                    return null;
            }
            else
                return null;
        }

        public static string GetSignupUserEmailDetailsStep1Content(string f_stEmail,bool contentForSupport = false)
        {
            string template = (contentForSupport)?File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/SignupUserEmailDetailsStep1_support.txt")) :
                                                  File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/SignupUserEmailDetailsStep1.txt"));
                    template = template.Replace("[EMAIL_ID]", f_stEmail); 
                    return template; 
        }
        public static string GetNotificationMailContent(string f_stName,string f_stNotificationBody)
        {
            string template = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/NotificationMail.txt"));
            template = template.Replace("[NAME]", f_stName);
            template = template.Replace("[NOTIFICATION_CONTENT]", f_stNotificationBody);
            return template;
        }
        public static string LoadTemplateSubject(string subject, string emails, long caseId = 0, long clientId = 0, long customerId = 0, long staffId = 0)
        {
            //check email associated with any client
            string actualTemplateSubject = subject;

            if (!string.IsNullOrEmpty(emails))
            {
                string[] placeholders = { "[CLIENT_GIVEN_NAME]","[CLIENT_FAMILY_NAME]","[CLIENT_PREFERRED_NAME]", 
                                            "[CLIENT_REF_NO]", "[SUBAGENT_NAME]", "[CLIENT_EMAIL]",
                                            "[CLIENT_MOBILE]", "[CLIENT_ADDRESS]", "[SUBAGENT_EMAIL]","[SUBAGENT_CONTACT]"
                                         ,"[COMPANY_NAME]","[COMPANY_CONTACT]","[MY_FIRST_NAME]","[MY_LAST_NAME]","[MY_PREFERRED_NAME]","[MY_EMAIL]"};

                if (emails.Contains(","))
                {
                    string[] emailArray = emails.Split(',');

                    foreach (string item in emailArray)
                    {
                        for (int i = 0; i < placeholders.Length; i++)
                        {
                            if (subject.Contains(placeholders[i]))
                            {
                                //placeholder found,
                                if (placeholders[i].Equals("[CLIENT_GIVEN_NAME]") || placeholders[i].Equals("[CLIENT_FAMILY_NAME]")
                                    || placeholders[i].Equals("[CLIENT_PREFERRED_NAME]") || placeholders[i].Equals("[CLIENT_EMAIL]")
                                    || placeholders[i].Equals("[CLIENT_REF_NO]")
                                    || placeholders[i].Equals("[CLIENT_MOBILE]") || placeholders[i].Equals("[CLIENT_ADDRESS]"))
                                {
                                    #region ClientDetail
                                    Client client = ISDAL.GetClientByEmail(item);
                                    if (client != null)
                                    {
                                        subject = subject.Replace("[CLIENT_GIVEN_NAME]", client.GivenNames);
                                        subject = subject.Replace("[CLIENT_FAMILY_NAME]", client.FamilyName);
                                        subject = subject.Replace("[CLIENT_PREFERRED_NAME]", client.PreferredName);
                                        subject = subject.Replace("[CLIENT_EMAIL]", client.Email);
                                        subject = subject.Replace("[CLIENT_MOBILE]", client.Mobile);
                                        subject = subject.Replace("[CLIENT_REF_NO]", client.ReferenceNumber);
                                    }
                                    #endregion
                                }
                                if (placeholders[i].Equals("[SUBAGENT_NAME]") || placeholders[i].Equals("[SUBAGENT_EMAIL]")
                                    || placeholders[i].Equals("[SUBAGENT_CONTACT]"))
                                {
                                    #region SubagentDetails
                                    SubAgent agent = ISDAL.FindSubagentByEmail(item);
                                    if (agent != null)
                                    {
                                        subject = subject.Replace("[SUBAGENT_NAME]", agent.Name);
                                        subject = subject.Replace("[SUBAGENT_EMAIL]", agent.Email);
                                        subject = subject.Replace("[SUBAGENT_CONTACT]", agent.Mobile);

                                    }
                                    #endregion
                                }
                                if (placeholders[i].Equals("[COMPANY_NAME]") || placeholders[i].Equals("[COMPANY_CONTACT]"))
                                {
                                    #region CompanyDetails

                                    if (customerId > 0)
                                    {
                                        Customer csObject = ISDAL.FindCustomer(customerId);
                                        if (csObject != null)
                                        {
                                            subject = subject.Replace("[COMPANY_NAME]", csObject.OrganizationName);
                                            subject = subject.Replace("[COMPANY_CONTACT]", csObject.ContactMobile);
                                        }
                                    }

                                    #endregion
                                }
                                if (placeholders[i].Equals("[MY_FIRST_NAME]") || placeholders[i].Equals("[MY_LAST_NAME]")
                                    || placeholders[i].Equals("[MY_PREFERRED_NAME]") || placeholders[i].Equals("[MY_EMAIL]"))
                                {
                                    #region StaffDetails

                                    if (staffId > 0)
                                    {
                                        Staff staffObject = ISDAL.FindStaff(staffId);
                                        if (staffObject != null)
                                        {
                                            subject = subject.Replace("[MY_FIRST_NAME]", staffObject.FirstName);
                                            subject = subject.Replace("[MY_LAST_NAME]", staffObject.LastName);
                                            subject = subject.Replace("[MY_PREFERRED_NAME]", staffObject.PreferredName);
                                            subject = subject.Replace("[MY_EMAIL]", staffObject.Email);
                                        }
                                    }

                                    #endregion
                                }
                            }
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < placeholders.Length; i++)
                    {
                        if (subject.Contains(placeholders[i]))
                        {
                            //placeholder found,
                            if (placeholders[i].Equals("[CLIENT_GIVEN_NAME]") || placeholders[i].Equals("[CLIENT_FAMILY_NAME]")
                                   || placeholders[i].Equals("[CLIENT_PREFERRED_NAME]") || placeholders[i].Equals("[CLIENT_EMAIL]")
                                   || placeholders[i].Equals("[CLIENT_REF_NO]")
                                   || placeholders[i].Equals("[CLIENT_MOBILE]") || placeholders[i].Equals("[CLIENT_ADDRESS]"))
                            {
                                #region ClientDetails
                                Client client = ISDAL.GetClientByEmail(emails);
                                if (client != null)
                                {
                                    subject = subject.Replace("[CLIENT_GIVEN_NAME]", client.GivenNames);
                                    subject = subject.Replace("[CLIENT_FAMILY_NAME]", client.FamilyName);
                                    subject = subject.Replace("[CLIENT_PREFERRED_NAME]", client.PreferredName);
                                    subject = subject.Replace("[CLIENT_EMAIL]", client.Email);
                                    subject = subject.Replace("[CLIENT_MOBILE]", client.Mobile);
                                    subject = subject.Replace("[CLIENT_REF_NO]", client.ReferenceNumber);

                                }
                                #endregion
                            }
                            if (placeholders[i].Equals("[SUBAGENT_NAME]") || placeholders[i].Equals("[SUBAGENT_EMAIL]")
                                || placeholders[i].Equals("[SUBAGENT_CONTACT]"))
                            {
                                #region SubagentDetails
                                SubAgent agent = ISDAL.FindSubagentByEmail(emails);
                                if (agent != null)
                                {
                                    subject = subject.Replace("[SUBAGENT_NAME]", agent.Name);
                                    subject = subject.Replace("[SUBAGENT_EMAIL]", agent.Email);
                                    subject = subject.Replace("[SUBAGENT_CONTACT]", agent.Mobile);

                                }
                                #endregion
                            }
                            if (placeholders[i].Equals("[COMPANY_NAME]") || placeholders[i].Equals("[COMPANY_CONTACT]"))
                            {
                                #region CompanyDetails

                                if (customerId > 0)
                                {
                                    Customer csObject = ISDAL.FindCustomer(customerId);
                                    if (csObject != null)
                                    {
                                        subject = subject.Replace("[COMPANY_NAME]", csObject.OrganizationName);
                                        subject = subject.Replace("[COMPANY_CONTACT]", csObject.ContactMobile);
                                    }
                                }

                                #endregion
                            }
                            if (placeholders[i].Equals("[MY_FIRST_NAME]") || placeholders[i].Equals("[MY_LAST_NAME]")
                                  || placeholders[i].Equals("[MY_PREFERRED_NAME]") || placeholders[i].Equals("[MY_EMAIL]"))
                            {
                                #region StaffDetails

                                if (staffId > 0)
                                {
                                    Staff staffObject = ISDAL.FindStaff(staffId);
                                    if (staffObject != null)
                                    {
                                        subject = subject.Replace("[MY_FIRST_NAME]", staffObject.FirstName);
                                        subject = subject.Replace("[MY_LAST_NAME]", staffObject.LastName);
                                        subject = subject.Replace("[MY_PREFERRED_NAME]", staffObject.PreferredName);
                                        subject = subject.Replace("[MY_EMAIL]", staffObject.Email);
                                    }
                                }

                                #endregion
                            }
                        }
                    }
                }
            }
            return subject;
        }

        public static string LoadTemplateBody(string templateBody, string emails, long caseId = 0, long clientId = 0, long customerId = 0, long staffId = 0)
        {
            //check email associated with any client
            string actualTemplateBody = templateBody;

            if (!string.IsNullOrEmpty(emails))
            {
                string[] placeholders = { "[CLIENT_GIVEN_NAME]","[CLIENT_FAMILY_NAME]","[CLIENT_PREFERRED_NAME]", 
                                            "[CLIENT_REF_NO]", "[SUBAGENT_NAME]", "[CLIENT_EMAIL]",
                                            "[CLIENT_MOBILE]", "[CLIENT_ADDRESS]", "[SUBAGENT_EMAIL]","[SUBAGENT_CONTACT]"
                                         ,"[COMPANY_NAME]","[COMPANY_CONTACT]","[MY_FIRST_NAME]","[MY_LAST_NAME]"
                                         ,"[MY_PREFERRED_NAME]","[MY_EMAIL]"};

                if (emails.Contains(","))
                {
                    string[] emailArray = emails.Split(',');

                    foreach (string item in emailArray)
                    {
                        for (int i = 0; i < placeholders.Length; i++)
                        {
                            if (templateBody.Contains(placeholders[i]))
                            {
                                //placeholder found,
                                if (placeholders[i].Equals("[CLIENT_GIVEN_NAME]") || placeholders[i].Equals("[CLIENT_FAMILY_NAME]")
                                      || placeholders[i].Equals("[CLIENT_PREFERRED_NAME]") || placeholders[i].Equals("[CLIENT_EMAIL]")
                                      || placeholders[i].Equals("[CLIENT_REF_NO]")
                                      || placeholders[i].Equals("[CLIENT_MOBILE]") || placeholders[i].Equals("[CLIENT_ADDRESS]"))
                                {
                                    #region ClientDetail

                                    Client client = ISDAL.GetClientByEmail(item);
                                    if (client != null)
                                    {
                                        templateBody = templateBody.Replace("[CLIENT_GIVEN_NAME]", client.GivenNames);
                                        templateBody = templateBody.Replace("[CLIENT_FAMILY_NAME]", client.FamilyName);
                                        templateBody = templateBody.Replace("[CLIENT_PREFERRED_NAME]", client.PreferredName);
                                        templateBody = templateBody.Replace("[CLIENT_EMAIL]", client.Email);
                                        templateBody = templateBody.Replace("[CLIENT_MOBILE]", client.Mobile);
                                        templateBody = templateBody.Replace("[CLIENT_REF_NO]", client.ReferenceNumber);

                                    }
                                    #endregion
                                }
                                if (placeholders[i].Equals("[SUBAGENT_NAME]") || placeholders[i].Equals("[SUBAGENT_EMAIL]")
                                    || placeholders[i].Equals("[SUBAGENT_CONTACT]"))
                                {
                                    #region SubagentRegion
                                    SubAgent agent = ISDAL.FindSubagentByEmail(item);
                                    if (agent != null)
                                    {
                                        templateBody = templateBody.Replace("[SUBAGENT_NAME]", agent.Name);
                                        templateBody = templateBody.Replace("[SUBAGENT_EMAIL]", agent.Email);
                                        templateBody = templateBody.Replace("[SUBAGENT_CONTACT]", agent.Mobile);

                                    }
                                    #endregion
                                }
                                if (placeholders[i].Equals("[COMPANY_NAME]") || placeholders[i].Equals("[COMPANY_CONTACT]"))
                                {
                                    #region CompanyDetails

                                    if (customerId > 0)
                                    {
                                        Customer csObject = ISDAL.FindCustomer(customerId);
                                        if (csObject != null)
                                        {
                                            templateBody = templateBody.Replace("[COMPANY_NAME]", csObject.OrganizationName);
                                            templateBody = templateBody.Replace("[COMPANY_CONTACT]", csObject.ContactMobile);
                                        }
                                    }

                                    #endregion
                                }
                                if (placeholders[i].Equals("[MY_FIRST_NAME]") || placeholders[i].Equals("[MY_LAST_NAME]")
                                  || placeholders[i].Equals("[MY_PREFERRED_NAME]") || placeholders[i].Equals("[MY_EMAIL]"))
                                {
                                    #region StaffDetails

                                    if (staffId > 0)
                                    {
                                        Staff staffObject = ISDAL.FindStaff(staffId);
                                        if (staffObject != null)
                                        {
                                            templateBody = templateBody.Replace("[MY_FIRST_NAME]", staffObject.FirstName);
                                            templateBody = templateBody.Replace("[MY_LAST_NAME]", staffObject.LastName);
                                            templateBody = templateBody.Replace("[MY_PREFERRED_NAME]", staffObject.PreferredName);
                                            templateBody = templateBody.Replace("[MY_EMAIL]", staffObject.Email);
                                        }
                                    }

                                    #endregion
                                }
                            }
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < placeholders.Length; i++)
                    {
                        if (templateBody.Contains(placeholders[i]))
                        {
                            //placeholder found,
                            if (placeholders[i].Equals("[CLIENT_GIVEN_NAME]") || placeholders[i].Equals("[CLIENT_FAMILY_NAME]")
                                          || placeholders[i].Equals("[CLIENT_PREFERRED_NAME]") || placeholders[i].Equals("[CLIENT_EMAIL]")
                                          || placeholders[i].Equals("[CLIENT_REF_NO]")
                                          || placeholders[i].Equals("[CLIENT_MOBILE]") || placeholders[i].Equals("[CLIENT_ADDRESS]"))
                            {
                                #region Client
                                Client client = ISDAL.GetClientByEmail(emails);
                                if (client != null)
                                {
                                    templateBody = templateBody.Replace("[CLIENT_GIVEN_NAME]", client.GivenNames);
                                    templateBody = templateBody.Replace("[CLIENT_FAMILY_NAME]", client.FamilyName);
                                    templateBody = templateBody.Replace("[CLIENT_PREFERRED_NAME]", client.PreferredName);
                                    templateBody = templateBody.Replace("[CLIENT_EMAIL]", client.Email);
                                    templateBody = templateBody.Replace("[CLIENT_MOBILE]", client.Mobile);
                                    templateBody = templateBody.Replace("[CLIENT_REF_NO]", client.ReferenceNumber);

                                }
                                #endregion
                            }
                            if (placeholders[i].Equals("[SUBAGENT_NAME]") || placeholders[i].Equals("[SUBAGENT_EMAIL]")
                                || placeholders[i].Equals("[SUBAGENT_CONTACT]"))
                            {
                                #region Subagent
                                SubAgent agent = ISDAL.FindSubagentByEmail(emails);
                                if (agent != null)
                                {
                                    templateBody = templateBody.Replace("[SUBAGENT_NAME]", agent.Name);
                                    templateBody = templateBody.Replace("[SUBAGENT_EMAIL]", agent.Email);
                                    templateBody = templateBody.Replace("[SUBAGENT_CONTACT]", agent.Mobile);

                                }
                                #endregion
                            }
                            if (placeholders[i].Equals("[COMPANY_NAME]") || placeholders[i].Equals("[COMPANY_CONTACT]"))
                            {
                                #region CompanyDetails

                                if (customerId > 0)
                                {
                                    Customer csObject = ISDAL.FindCustomer(customerId);
                                    if (csObject != null)
                                    {
                                        templateBody = templateBody.Replace("[COMPANY_NAME]", csObject.OrganizationName);
                                        templateBody = templateBody.Replace("[COMPANY_CONTACT]", csObject.ContactMobile);
                                    }
                                }

                                #endregion
                            }
                            if (placeholders[i].Equals("[MY_FIRST_NAME]") || placeholders[i].Equals("[MY_LAST_NAME]")
                                  || placeholders[i].Equals("[MY_PREFERRED_NAME]") || placeholders[i].Equals("[MY_EMAIL]"))
                            {
                                #region StaffDetails

                                if (staffId > 0)
                                {
                                    Staff staffObject = ISDAL.FindStaff(staffId);
                                    if (staffObject != null)
                                    {
                                        templateBody = templateBody.Replace("[MY_FIRST_NAME]", staffObject.FirstName);
                                        templateBody = templateBody.Replace("[MY_LAST_NAME]", staffObject.LastName);
                                        templateBody = templateBody.Replace("[MY_PREFERRED_NAME]", staffObject.PreferredName);
                                        templateBody = templateBody.Replace("[MY_EMAIL]", staffObject.Email);
                                    }
                                }

                                #endregion
                            }
                        }
                    }
                }
            }
            return templateBody;
        }

        public static string GenerateFeedbackMailForStaff(Staff staff)
        {
            if (staff != null)
            {
                string content = string.Empty;

                content = "Dear " + staff.FirstName + "<br/><br/>";
                content += "Your support request has been received. We will get back to you within 24 hours." + "<br/>";
                content += "Best Regards" + "<br/>";
                content += "Support Team";

                return content;
            }
            else
                return null;
        }

        public static string GenerateWelcomeMailToStaff(string token, string staffPreferredName, string customerAdminName = null)
        {
            StringBuilder mailContent = new StringBuilder();
            mailContent.AppendLine("Dear " + staffPreferredName + ",<br /><br />");
            mailContent.AppendLine("Your company's ImmiSys account administrator " + customerAdminName + " has created a user account for you. Please click the link below to set your password:  <br /><br/>");
            mailContent.AppendLine("<a href=" + token + "><strong>" + token + "<strong /></a><br /><br />");

            mailContent.AppendLine("Please note this is an auto generated email. Please do not reply to this email. If you have any questions, please contact your company's Immisys account administrator " + customerAdminName + ".<br /><br />");
            mailContent.AppendLine("Best Regards <br />");
            mailContent.AppendLine("<strong>ImmiSys Team</strong>");
            return mailContent.ToString();
        }

        public static string GetEntityReference(string entityName, long entityId)
        {
            string reference = string.Empty;
            switch (entityName)
            {
                case Constants.SOLR_CLIENT:
                case Constants.EMPLOYER:
                    {
                        reference = ISDAL.FindClient(entityId).GivenNames;
                        break;
                    }
                case Constants.SOLR_OPPORTUNITY:
                    {
                        reference = ISDAL.FindOpportunity(entityId).ReferenceNumber;
                        break;
                    }
                case Constants.SOLR_CASE:
                    {
                        Case dbCase = ISDAL.FindCase(entityId);
                        if (dbCase != null)
                        {
                            reference = dbCase.CaseReference;
                        }
                        break;
                    }
                default:
                    break;
            }

            return reference;
        }

        public static string GetEntityDescription(string entityName, long entityId)
        {
            string description = string.Empty;
            switch (entityName)
            {
                case Constants.SOLR_CLIENT:
                    {
                        Client dbClient = ISDAL.FindClient(entityId);
                        if (dbClient != null)
                        {
                            description = description + dbClient.ReferenceNumber + " - ";
                            description = description + DataUtils.FormatName(dbClient.GivenNames, dbClient.FamilyName, dbClient.PreferredName);
                        }
                        break;
                    }
                case Constants.EMPLOYER:
                    {
                        Client dbClient = ISDAL.FindClient(entityId);
                        if (dbClient != null)
                        {
                            description = "Employer - " + dbClient.GivenNames;
                        }
                        break;
                    }
                case Constants.SOLR_OPPORTUNITY:
                    {
                        Opportunity dbOpportunity = ISDAL.FindOpportunity(entityId);
                        if (dbOpportunity != null)
                        {
                            description = description + dbOpportunity.ReferenceNumber + " - ";
                            description = description + dbOpportunity.Staff.FirstName + " " + dbOpportunity.Staff.LastName;
                        }
                        break;
                    }

                case Constants.SOLR_CASE:
                    {
                        Case dbCase = ISDAL.FindCase(entityId);
                        if (dbCase != null)
                        {
                            //Staff dbStaff = dbCase.Staff;
                            //description = dbCase.CaseReference + " - " + dbStaff.FirstName + " " + dbStaff.LastName;
                            string clientName = DataUtils.FormatName(dbCase.Client.GivenNames, dbCase.Client.FamilyName, dbCase.Client.PreferredName);
                            description = dbCase.CaseReference + " - " + clientName;
                        }
                        break;
                    }
                default:
                    break;
            }
            return description;
        }

        public static string GetEntityName(string p1, long p2)
        {
            if (p1.Equals(Constants.CLIENT, StringComparison.OrdinalIgnoreCase))
            {
                return p1;
            }
            else if (p1.Equals(Constants.EMPLOYER, StringComparison.OrdinalIgnoreCase))
            {
                return "Employer";
            }
            else if (p1.Equals(Constants.OPPORTUNITY, StringComparison.OrdinalIgnoreCase))
            {
                return Constants.SOLR_OPPORTUNITY;
            }
            else
            {
                if (p1.Equals(Constants.CASE, StringComparison.OrdinalIgnoreCase))
                {
                    if (ISDAL.CaseHasEnrolment(p2))
                        return Constants.SOLR_ENROLMENT;
                    else
                        return Constants.SOLR_CASE;
                }
                else
                    return null;
            }
        }

        public static string GetOtherDescription(List<Relationship> list)
        {
            string otherDescription = string.Empty;
            if (list != null)
            {
                foreach (var item in list)
                {
                    otherDescription = (!string.IsNullOrEmpty(otherDescription) ? otherDescription + " , " : " ");

                    if (!string.IsNullOrEmpty(item.Title))
                    {
                        otherDescription = otherDescription + item.Title + ".";
                    }
                    if (!string.IsNullOrEmpty(item.FirstName))
                    {
                        otherDescription = otherDescription + " " + item.FirstName;
                    }
                    if (!string.IsNullOrEmpty(item.Email))
                    {
                        otherDescription = otherDescription + " " + item.Email;
                    }
                    if (!string.IsNullOrEmpty(item.Mobile))
                    {
                        otherDescription = otherDescription + " " + item.Mobile;
                    }
                }
            }
            return otherDescription;
        }

        public static void SyncCountryTable()
        {
            foreach (Country item in ISDAL.GetAllCountries())
            {
                Country dbCountry = new Country();
                dbCountry.Id = item.Id;
                dbCountry.CodeVevo = ISDAL.GetCountryCodeVevo(item.Name);
                dbCountry.Name = item.Name;
                dbCountry.Code = item.Code;

                ISDAL.AddUpdateCountry(dbCountry);
            }
        }

        internal static string GenerateAutogeneratedLoginAlert(string email)
        {
            if (!string.IsNullOrWhiteSpace(email))
            {
                string template = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/LoginFailNotification.txt"));

                template = template.Replace("[EMAIL]", email);

                return template;
            }
            else
                return null;
        }

        public static string GenerateVerificationMail(string applicantName, string token)
        {
            if (!string.IsNullOrWhiteSpace(token))
            {
                string template = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/WelcomeEmailVerification.txt"));

                template = template.Replace("[APPLICANT_NAME]", applicantName);
                template = template.Replace("[TOKEN]", token);

                return template;
            }
            else
                return null;
        }

        public static string GenerateVerificationMail(string applicantName, string token, long? subscribedPlanId)
        {
            if (!string.IsNullOrWhiteSpace(applicantName) && !string.IsNullOrWhiteSpace(token))
            {
                string template = "";

                if (subscribedPlanId == 1)
                {
                    template = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/WelcomeEmailVerificationFreePlan.txt"));
                }
                else
                {
                    template = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/WelcomeEmailVerification.txt"));
                }

                template = template.Replace("[APPLICANT_NAME]", applicantName);
                template = template.Replace("[TOKEN]", token);

                return template;
            }
            else
                return null;
        }



        public static string GetBodyForRequestInvoice(long invoiceId, long receiptId, long staffId, string sender)
        {
            if (invoiceId > 0)
            {
                string description = string.Empty;
                //fetch invoice object
                var dbInvoice = ISDAL.FindInvoice(invoiceId);
                if (dbInvoice != null)
                {
                    var dbPaymentMilestone = ISDAL.FindPaymentMilestoneByInvoiceId(invoiceId);
                    var item = ISDAL.FIndPaymentReceipt(receiptId);
                    var dbClient = ISDAL.FindClient(dbInvoice.ClientId.GetValueOrDefault());
                    var dbCompany = ISDAL.FindCustomer(dbInvoice.CustomerId.GetValueOrDefault());
                    var dbCase = ISDAL.FindCase(dbInvoice.CaseId.GetValueOrDefault());
                    var dbEnrolment = ISDAL.FindEnrolmentByCaseId(dbCase.Id);
                    var dbInstitute = ISDAL.FindCompany(dbEnrolment.InstitutionId.GetValueOrDefault());
                    var dbCourse = ISDAL.FindCourse(dbEnrolment.CourseId.GetValueOrDefault());

                    var dbPaymentSetting = ISDAL.FindPaymentSetting(dbInvoice.CustomerId.GetValueOrDefault(), dbInvoice.CaseId.GetValueOrDefault());
                    if (dbPaymentSetting != null)
                    {
                        StringBuilder content = new StringBuilder();

                        if (dbPaymentSetting.PayCommissionTo.Equals(Constants.SUB_AGENT))
                        {
                            // content for sub agent
                            var dbAgent = ISDAL.FindSubAgent(dbPaymentSetting.SubAgentId.GetValueOrDefault());
                            if (dbAgent != null)
                            {
                                string template = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/RequestInvoiceTemplate.txt"));

                                template = template.Replace("[UNIVERSITY_NAME]", dbInstitute.Name);

                                var dbTax = ISDAL.FindTax(dbPaymentMilestone.TaxCodeId.GetValueOrDefault());
                                decimal? commShare = (dbPaymentSetting.CommissionType.Equals(Constants.COMMISSION_EVERY_INVOICE) ? dbPaymentSetting.Amount : (decimal?)null);
                                decimal comm = (commShare != null ? (item.Amount * (commShare.GetValueOrDefault() / 100)) : dbPaymentSetting.Amount.GetValueOrDefault());

                                decimal? taxAmount = null;

                                if (dbTax != null)
                                    taxAmount = (dbTax.Percentage / 100) * item.Amount;

                                decimal total = comm + (taxAmount != null ? taxAmount.GetValueOrDefault() : 0);
                                decimal? tax = (taxAmount != null ? taxAmount.GetValueOrDefault() : (decimal?)null);

                                decimal taxExclusiveAMount = 0;
                                if (dbTax != null)
                                {
                                    taxExclusiveAMount = item.Amount / (1 + (dbTax.Percentage / 100));
                                }
                                else
                                    taxExclusiveAMount = item.Amount;

                                string taxForAgent = string.Empty;
                                decimal? taxAmountForAgent = null;

                                comm = taxExclusiveAMount * (commShare.GetValueOrDefault() / 100);

                                if (dbPaymentSetting.PayCommissionTo.Equals(Constants.SUB_AGENT))
                                {
                                    var dbSubAgent = ISDAL.FindSubAgent(dbPaymentSetting.SubAgentId.GetValueOrDefault());
                                    if (dbSubAgent != null)
                                    {
                                        if (dbSubAgent.DefaultTaxId.GetValueOrDefault() > 0)
                                        {
                                            var dbAgentTax = ISDAL.FindTax(dbSubAgent.DefaultTaxId.GetValueOrDefault());
                                            if (dbAgentTax != null)
                                            {
                                                taxForAgent = dbAgentTax.Name;
                                                taxAmountForAgent = (comm * dbAgentTax.Percentage / 100);
                                            }
                                        }
                                    }
                                }

                                string courseName = string.Empty;
                                if (dbCourse != null)
                                {
                                    if (!string.IsNullOrWhiteSpace(dbCourse.Name))
                                    {
                                        courseName = dbCourse.Name;
                                    }
                                }

                                string commTaxName = (dbTax != null ? dbTax.Name : "");

                                content.Append("<table>");

                                content.Append("<tbody><tr><td>Student Name : </td><td>" + DataUtils.FormatName(dbClient.GivenNames, dbClient.FamilyName) + "</td></tr><td>Student Id : </td><td>" + dbEnrolment.StudentNo + "</td></tr><tr><td>Case # : </td><td>" + dbCase.CaseReference + "</td></tr><tr><td>University : </td><td>" + dbInstitute.Name + "</td></tr><tr><td>Course : </td><td>" + courseName + "</td></tr><tr><td>Amount received : </td><td>$" + item.Amount.ToString("N2") + "</td></tr><tr><td>Tax code : </td><td>" + commTaxName + "</td></tr><tr><td>Tax exclusive amount : </td><td>$" + taxExclusiveAMount.ToString("N2") + "</td></tr><tr><td>Commission share : </td><td>" + commShare + "%" + "</td></tr><tr><td>Commission : </td><td>$" + comm.ToString("N2") + "</td></tr><tr><td>Tax : </td><td>$" + taxAmountForAgent.GetValueOrDefault().ToString("N2") + "</td></tr><tr><td>Total : </td><td><b>$" + (comm + taxAmountForAgent.GetValueOrDefault()).ToString("N2") + "</b></td></tr></tbody></table>");

                                if (template.Contains("[COMMISSION_DETAILS]"))
                                    template = template.Replace("[COMMISSION_DETAILS]", content.ToString());
                                if (template.Contains("[COMMISSION_AMOUNT]"))
                                    template = template.Replace("[COMMISSION_AMOUNT]", "$" + comm.ToString("N2"));
                                if (template.Contains("[STUDENT_NAME]"))
                                    template = template.Replace("[STUDENT_NAME]", DataUtils.FormatName(dbClient.GivenNames, dbClient.FamilyName));
                                if (template.Contains("[COMPANY_EMAIL]"))
                                    template = template.Replace("[COMPANY_EMAIL]", dbCompany.ContactEmail);
                                if (template.Contains("[SENDER]"))
                                    template = template.Replace("[SENDER]", sender);
                                description = template.ToString();
                            }
                        }
                    }
                }
                return description;
            }
            else
                return null;
        }

        public static string GenerateUserName(string givenName, string familyName)
        {
            string name = string.Empty;

            if (!string.IsNullOrEmpty(givenName))
                name = name + givenName + "";

            if (!string.IsNullOrEmpty(familyName))
                name = name + familyName;

            return name;
        }

        public static string GenerateInvoiceNumber(long customerId, bool isCommissionInvoice, bool isMergedCommInvoice = false)
        {
            if (isMergedCommInvoice)
            {
                return Convert.ToString(ISDAL.FindInvoicePrefixByCustomer(isCommissionInvoice, customerId, isMergedInv: true) +
                    ISDAL.GenerateInvoiceNumberByCustomerId(customerId, isCommInv: isCommissionInvoice, isMergedInv: true) +
                                ISDAL.FindInvoiceSuffixByCustomer(isCommissionInvoice, customerId, isMergedInv: true)).Trim();
            }
            else
            {
                return Convert.ToString(ISDAL.FindInvoicePrefixByCustomer(isCommissionInvoice, customerId) +
                                ISDAL.GenerateInvoiceNumberByCustomerId(customerId, isCommInv: isCommissionInvoice) +
                                ISDAL.FindInvoiceSuffixByCustomer(isCommissionInvoice, customerId)).Trim();
            }
        }

        public static void DeleteFile(string path)
        {
            File.Delete(path);
        }

        public static void UploadCustomersLogosToCloud()
        {
            foreach (var item in ISDAL.GetAllCustomers())
            {
                var dbTemplateSetting = ISDAL.FindTemplateSettingByCustomer(item.Id);

                if (dbTemplateSetting != null)
                {
                    //replace file from local to s3,

                    string logoPhysicalPath = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(string.Format("../InvoiceTemplateFiles/{0}/{1}", item.Id, dbTemplateSetting.LetterHeadFilePath)));

                    if (File.Exists(logoPhysicalPath))
                    {
                        string fileName = dbTemplateSetting.LetterHeadFilePath;

                        string savedFilePath = logoPhysicalPath;

                        string folderKey = DocumentManager.GetFolder(DocumentType.CUSTOMER_LOGO);
                        string key = string.Format("{0}/{1}", folderKey, fileName);
                        string httpLink = string.Empty;

                        S3Document s3Document = DocumentManager.UploadDocumentToS3(item.Id, key, fileName, savedFilePath, DocumentType.CUSTOMER_LOGO, true);

                        //update s3 link to database
                        ISDAL.UpdateCustomerLogo(s3Document.DocumentLink, dbTemplateSetting.Id);


                        //remove the file from local,
                        //File.Delete(logoPhysicalPath);
                    }
                }
            }
        }

        public static string GetCampusAddress(Campus item)
        {
            string address = string.Empty;

            if (item != null)
            {
                if (!string.IsNullOrWhiteSpace(item.StreetAddress))
                    address = address + item.StreetAddress;

                if (!string.IsNullOrWhiteSpace(item.City))
                    address = (!string.IsNullOrWhiteSpace(address) ? address + " , " + item.City : item.City);

                if (item.StateId.GetValueOrDefault() > 0)
                    address = (!string.IsNullOrWhiteSpace(address) ? address + " , " + ISDAL.FindStateName(item.StateId.GetValueOrDefault()) : ISDAL.FindStateName(item.StateId.GetValueOrDefault()));

                if (item.CountryId.GetValueOrDefault() > 0)
                    address = (!string.IsNullOrWhiteSpace(address) ? address + " , " + ISDAL.FindCountryName(item.CountryId.GetValueOrDefault()) : ISDAL.FindCountryName(item.CountryId.GetValueOrDefault()));
            }

            return address;
        }

        public static string GetNotesMailContent(string Description,long ClientID,long StaffId,string CustomerLogo)
        {
            string template = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/AddNotesMail.txt"));
            string ClientName = ISDAL.FindClientNameByClientId(ClientID);
            string StaffName = ISDAL.GetStaffNameById(StaffId);
            template = template.Replace("[LogoUrl]", CustomerLogo);
            template = template.Replace("[NAME]", ClientName);
            template = template.Replace("[NOTES_CONTENT]", Description);
            template = template.Replace("[USER_GIVEN_NAME]", StaffName);
             
            return template;
        }

        public static string GenerateTrainingEmailContent(string customerName, decimal intialAmountTotal, int Hours)
        {
            if (customerName != null)
            {
                string template = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/TrainingPaymentSuccessfully.txt"));
                template = template.Replace("[APPLICANT_NAME]", customerName);
                template = template.Replace("[Amount]", Convert.ToString(intialAmountTotal));
                template = template.Replace("[Hour]", Convert.ToString(Hours));
                return template;
            }
            else
            {
                return null;
            }
        }

        public static string GenerateLinkForPayment(string customernName,string weblink,string Date)
        {
            if (customernName != null)
            {
                string template = "";
                // template = File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/RecurringPaymentFailed.txt"));
                var path = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/PaymentLink.txt");
                template = File.ReadAllText(path);
                template = template.Replace("[APPLICANT_NAME]", customernName);
                template = template.Replace("[weblink]", weblink);
                template = template.Replace("[insertdate]", Date);
                return template;
            }
            else
            {
                return null;
            }
        }

    }
}
