﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ImmiSys.Domain.Utils
{
    public class DateUtils
    {
        public static string GetSmartDate(DateTime? date)
        {
            string dateStr = "";

            if (date == null)
                return dateStr;

            DateTime nDate = date.GetValueOrDefault();

            string format = "dd/MM/yy";
            if (nDate.Date.Equals(DateTime.Now.Date))
            {
                format = "h:mm tt";
            }
            else if (nDate.Month.Equals(DateTime.Now.Month))
            {
                format = "MMM d";
            }

            return nDate.ToString(format);
        }

        public static DateTime ParseDateTime(string dateValue)
        {
            DateTime convertedDate;
            string dateFormat = string.Empty;
            //Triming PDT from end  Example:Tue, 19 Sep 2017 16:49:46 -0700 (PDT)
            int l_iIndex = dateValue.LastIndexOf("(");
            if (l_iIndex > 0)
                dateValue = dateValue.Substring(0, l_iIndex).TrimEnd();

            //if(dateValue.Contains("("))
            //{
            //    string dateData = dateValue.Replace("");
            //}
            string lastCharFormat = dateValue.Substring(dateValue.Trim().Length - 5);
            if (dateValue.Trim().Contains("("))
            {
                dateValue.Remove(lastCharFormat.Length);
                Console.WriteLine(dateValue);
                dateFormat = "ddd, dd MMM yyyy HH:mm:ss K"; //+ lastCharFormat;
            }
            // dateFormat = "ddd, dd MMM yyyy HH:mm:ss K"; 
            else if (lastCharFormat.Trim().Contains("+") == true || lastCharFormat.Trim().Contains("-"))
                dateFormat = "ddd, d MMM yyyy HH:mm:ss zzz";
            else if (dateValue.Trim().Contains(":"))
                dateFormat = "ddd, d MMM yyyy HH:mm:ss";
            else if (dateValue.Trim().Contains("-"))
            {
                string[] datestr = dateValue.Split(new char[] { '-' });

                if (Convert.ToInt32(datestr[0]) > 12 && datestr[2].Length > 2)
                    dateFormat = "dd-MM-yyyy";
                else if (Convert.ToInt32(datestr[0]) > 12 && datestr[2].Length == 2)
                    dateFormat = "dd-MM-yy";
                else if (datestr[2].Length > 2)
                    dateFormat = "MM-dd-yyyy";
                else
                    dateFormat = "MM-dd-yy";
            }
            else if (dateValue.Trim().Contains("/"))
            {
                string[] datestr = dateValue.Split(new char[] { '/' });

                if (Convert.ToInt32(datestr[0]) > 12 && datestr[2].Length > 2)
                    dateFormat = "dd/MM/yyyy";
                else if (Convert.ToInt32(datestr[0]) > 12 && datestr[2].Length == 2)
                    dateFormat = "dd/MM/yy";
                else if (datestr[2].Length > 2)
                    dateFormat = "MM/dd/yyyy";
                else
                    dateFormat = "MM/dd/yy";
            }
            else
                dateFormat = "ddd, d MMM yyyy";


            if (DateTime.TryParseExact(dateValue.Trim(), dateFormat, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out convertedDate))
            {
            }
            else
            {
                convertedDate = DateTime.Now;
            }

            return convertedDate;
        }
        public static DateTime ParseDateTimeUTC(string dateValue)
        {
            DateTime convertedDate;
            string dateFormat = string.Empty;
            //Triming PDT from end  Example:Tue, 19 Sep 2017 16:49:46 -0700 (PDT)
            int l_iIndex = dateValue.LastIndexOf("(");
            if (l_iIndex > 0)
                dateValue = dateValue.Substring(0, l_iIndex).TrimEnd();

            try
            {
                //Convert to UNIVERSAL TIME ZONE 
                convertedDate = Convert.ToDateTime(dateValue);
                convertedDate = convertedDate.ToUniversalTime();//DateTime.SpecifyKind(convertedDate, DateTimeKind.Utc);
            } catch (Exception)
            {
                convertedDate = DateTime.UtcNow;
            }  
            return convertedDate;
        }
        public static bool IsDateTimeOk(string dateValue)
        {
            DateTime convertedDate;
            string dateFormat = string.Empty;
            //if(dateValue.Contains("("))
            //{
            //    string dateData = dateValue.Replace("");
            //}
            string lastCharFormat = dateValue.Substring(dateValue.Trim().Length - 5);
            if (dateValue.Trim().Contains("("))
            {
                dateValue.Remove(lastCharFormat.Length);
                Console.WriteLine(dateValue);
                dateFormat = "ddd, dd MMM yyyy HH:mm:ss K"; //+ lastCharFormat;
            }
            // dateFormat = "ddd, dd MMM yyyy HH:mm:ss K"; 
            else if (dateValue.Trim().Contains("+"))
                dateFormat = "ddd, d MMM yyyy HH:mm:ss zzz";
            else if (dateValue.Trim().Contains(":"))
                dateFormat = "ddd, d MMM yyyy HH:mm:ss";
            else if (dateValue.Trim().Contains("-"))
            {
                string[] datestr = dateValue.Split(new char[] { '-' });

                if (Convert.ToInt32(datestr[0]) > 12 && datestr[2].Length > 2)
                    dateFormat = "dd-MM-yyyy";
                else if (Convert.ToInt32(datestr[0]) > 12 && datestr[2].Length == 2)
                    dateFormat = "dd-MM-yy";
                else if (datestr[2].Length > 2)
                    dateFormat = "MM-dd-yyyy";
                else
                    dateFormat = "MM-dd-yy";
            }
            else if (dateValue.Trim().Contains("/"))
            {
                string[] datestr = dateValue.Split(new char[] { '/' });

                if (Convert.ToInt32(datestr[0]) > 12 && datestr[2].Length > 2)
                    dateFormat = "dd/MM/yyyy";
                else if (Convert.ToInt32(datestr[0]) > 12 && datestr[2].Length == 2)
                    dateFormat = "dd/MM/yy";
                else if (datestr[2].Length > 2)
                    dateFormat = "MM/dd/yyyy";
                else
                    dateFormat = "MM/dd/yy";
            }
            else
                dateFormat = "ddd, d MMM yyyy";


            if (DateTime.TryParseExact(dateValue.Trim(), dateFormat, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out convertedDate))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static DateTime GetDateAfter(DateTime startDate, int afterDays)
        {
            return startDate.AddDays(afterDays).Date;
        }

        public static DateTime UtcToLocal(DateTime source,
      TimeZoneInfo localTimeZone)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(source, localTimeZone);
        }

        public static DateTime UtcToLocalOrDefault(DateTime source,
      TimeZoneInfo localTimeZone)
        {   
            return (localTimeZone != null ? DateUtils.UtcToLocal(source, localTimeZone) : source);
        }

        public static DateTime LocalToUtc(DateTime source,
            TimeZoneInfo localTimeZone)
        {
            source = DateTime.SpecifyKind(source, DateTimeKind.Unspecified);
            return TimeZoneInfo.ConvertTimeToUtc(source, localTimeZone);
        }


        internal static TimeZoneInfo ConvertNameToTimeZone(string timeZone)
        {
            var infos = TimeZoneInfo.GetSystemTimeZones();

            return infos.Where(x => x.DisplayName == timeZone).FirstOrDefault();
        }
    }
}
