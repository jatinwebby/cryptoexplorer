﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WICMS.Domain.Utils
{
    public static class ExceptionDataUtils
    {  
        public static string GetAllFootprints(Exception x)
        {
            if (x == null)
                return string.Empty;

            var st = new System.Diagnostics.StackTrace(x, true);
            var frames = st.GetFrames();
            var traceString = "";
            foreach (var frame in frames)
            {
                if (frame.GetFileLineNumber() < 1)
                    continue;

                traceString +=
                    "File: " + frame.GetFileName() +
                    ", Method:" + frame.GetMethod().Name +
                    ", LineNumber: " + frame.GetFileLineNumber();

                traceString += "  -->  ";
            }

            return traceString;
        } 
    }
}
