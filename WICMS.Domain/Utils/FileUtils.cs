﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using RestSharp;
using RestSharp.Authenticators;
namespace WICMS.Domain.Utils
{
    public class FileUtils
    {
        public static string GetContent(Stream stream)
        {
            string fileContent = "";
            using (StreamReader reader = new StreamReader(stream))
            {
                fileContent = reader.ReadToEnd();
            }
            return fileContent;
        }

        public static void WriteToFile(string filePath, string fileContent)
        {
            using (FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite))
            {
                using (StreamWriter writer = new StreamWriter(fs))
                {
                    writer.Write(fileContent);
                }
            }
        }

        public static Boolean WriteToFile(byte[] bytes, string filePath)
        {
            try
            {
                using (var fs = new FileStream(filePath, FileMode.CreateNew, FileAccess.ReadWrite))
                {
                    using (var bw = new BinaryWriter(fs))
                    {
                        bw.Write(bytes, 0, bytes.Length);
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static void DeleteFile(string filePath)
        {
            File.Delete(filePath);
        }

        public static string GetPublicBucketUrl(string keyName, string bucketName, string regionUrl)
        {
            return string.Format("https://{0}.amazonaws.com/{1}/{2}", regionUrl, bucketName, keyName);
        }


        public static void DownloadFileByURL(string url, string filePath)
        { 
            using (var writer = File.OpenWrite(filePath))
            { 
                RestClient client = new RestClient();
                client.BaseUrl = new Uri(url); 
                RestRequest request = new RestRequest(Method.GET); 
            
                request.ResponseWriter = (responseStream) => responseStream.CopyTo(writer);
                var response = client.Execute(request); 

            }

        }
    }
}
