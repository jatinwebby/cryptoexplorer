﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using ImmiSys.DAL;
using OfficeOpenXml;
using ImmiSys.Domain.Entities;

namespace ImmiSys.Domain.Utils
{
    public enum ImportDataFileFormat { Xlsx, Csv, Txt }

    public class ImportDataEventArgs : EventArgs
    {
        public bool Cancel { get; set; }
        public string FilePath { get; set; }
    }

    public class ImportDataRowImportingEventArgs<T> : EventArgs where T : class
    {
        public bool IgnoreRow { get; set; }
        public string ColumnName { get; set; }
        public bool RowProcessedOnce { get; set; }

        public string EntityName { get; set; }

        public long EntityId { get; set; }

        /// <summary>
        /// Gets or Sets the final value that will be assigned to the item
        /// </summary>
        public object Value { get; set; }
        public T Item { get; set; }
        public Dictionary<string, object> Values { get; set; }
        public Dictionary<string, object> ExtraValues { get; set; }
    }

    public class ImportDataCompleteEventArgs : EventArgs
    {
        public int RowsSucceeded { get; set; }
        public int RowsFailed { get; set; }
    }

    /// <summary>
    /// Utility to import data
    /// </summary>
    public class ImportData<T> where T : class
    {
        public ImportData(string[] columnNames)
            : this(columnNames, null)
        {
        }

        /// <summary>
        /// Instantiates the import data object
        /// </summary>
        /// <param name="columnNames">Property names of the object</param>
        /// <param name="extraValues">Pass parameters that will be returned as it is, on the RowImporting and other events as event arguments</param>
        public ImportData(string[] columnNames, Dictionary<string, object> extraValues)
        {
            this.ColumnNames = columnNames;
            this.ExtraValues = extraValues;
        }

        public delegate void OnRowImporting(object sender, ImportDataRowImportingEventArgs<T> e);
        public delegate void OnImportCompleted(object sender, ImportDataCompleteEventArgs e);

        public event OnRowImporting RowImporting;
        public event OnImportCompleted ImportCompleted;

        private Dictionary<string, object> ExtraValues { get; set; }
        public string[] ColumnNames { get; set; }

        public HttpPostedFileBase PostedFile { get; set; }

        public string EntityName { get; set; }

        public long EntityId { get; set; }

        public string FilePath { get; set; }

        public CourseMoreInfo CourseAdditionalInfo { get; set; }

        public bool IncludeHeader { get; set; }

        public ImportDataFileFormat Extension { get; set; }

        private void PrepareImport()
        {
            if (this.ColumnNames == null || this.ColumnNames.Count() <= 0)
                throw new ArgumentNullException("Columns");

            if (string.IsNullOrWhiteSpace(this.FilePath))
            {
                if (this.PostedFile == null || string.IsNullOrWhiteSpace(this.PostedFile.FileName))
                {
                    throw new ArgumentException("FilePath");
                }
                else
                {
                    string tempDir = HttpContext.Current.Server.MapPath("~/Temp/");

                    if (!Directory.Exists(tempDir))
                        Directory.CreateDirectory(tempDir);

                    this.FilePath = Path.Combine(tempDir, PostedFile.FileName);

                    this.PostedFile.SaveAs(this.FilePath);
                }
            }
        }

        private void ImportFromExcel()
        {
            int rowsSucceeded = 0, rowsFailed = 0;
            object value = null;
            using (var package = new ExcelPackage(new FileInfo(this.FilePath)))
            {
                ExcelWorkbook workbook = package.Workbook;

                if (workbook != null)
                {
                    if (workbook.Worksheets.Count > 0)
                    {
                        var workSheet = package.Workbook.Worksheets.First();

                        int rowCount = workSheet.Dimension.End.Row;
                        int colCount = workSheet.Dimension.End.Column;

                        int startRow = this.IncludeHeader ? 1 : 2;
                        string columnName = string.Empty;

                        ImportDataRowImportingEventArgs<T> eventArgs = new ImportDataRowImportingEventArgs<T>();

                        eventArgs.ExtraValues = this.ExtraValues;

                        for (int j = startRow; j <= rowCount; j++)
                        {
                            //Create the object of passed type
                            var obj = Activator.CreateInstance(typeof(T));

                            eventArgs.RowProcessedOnce = false;

                            for (int i = 1; i <= this.ColumnNames.Count(); i++)
                            {
                                columnName = this.ColumnNames[i - 1];

                                //Get the cell value
                                value = workSheet.Cells[j, i].Value;

                                eventArgs.ColumnName = columnName;
                                eventArgs.Value = value;
                                eventArgs.Item = (T)obj;

                                eventArgs.EntityId = this.EntityId;
                                eventArgs.EntityName = this.EntityName;

                                if (this.RowImporting != null)
                                    this.RowImporting.Invoke(this, eventArgs);

                                if (eventArgs.IgnoreRow)
                                    continue;

                                //Swap the value to ensure the final value if modified by outside code
                                value = eventArgs.Value;

                                //Set the property of the object
                                SetPropertyValue(obj, columnName, value);

                                //Mark the row as processed
                                eventArgs.RowProcessedOnce = true;
                            }
                            try
                            {
                                using (var db = ISDAL.GetDataContext())
                                {
                                    if (!(obj.GetType().Name == "Campus") && !(obj.GetType().Name == "CourseCampus") && !(obj.GetType().Name == "Company") && !(obj.GetType().Name == "Course"))
                                    {
                                        db.GetTable<T>().InsertOnSubmit((T)obj);

                                        //Save the changes
                                        db.SubmitChanges();
                                    }
                                    if (obj.GetType().Name == "Company")
                                    {
                                        //if company, check CustomerId, and Update AgreementExpiryDate,
                                        var dbCompany = (ImmiSys.Domain.Entities.Company)obj;

                                        dbCompany.LegalName = dbCompany.Name;
                                        dbCompany.Name = dbCompany.TradingName;

                                        db.GetTable<T>().InsertOnSubmit((T)obj);

                                        //Save the changes
                                        db.SubmitChanges();

                                        if (!ISDAL.IsAgreementExpiryExist(dbCompany.Id, dbCompany.CustomerId.GetValueOrDefault()))
                                        {
                                            ISDAL.Add<ImmiSys.Domain.Entities.CompanyAgreement>(new Entities.CompanyAgreement()
                                            {
                                                AgreementDate = dbCompany.AgreementExpiryDate,
                                                CompanyId = dbCompany.Id,
                                                CreatedAt = DateTime.UtcNow,
                                                CustomerId = dbCompany.CustomerId
                                            });
                                        }
                                    }
                                    else if (obj.GetType().Name == "Campus")
                                    {
                                        //if company, check CustomerId, and Update AgreementExpiryDate,
                                        var dbCampus = (ImmiSys.Domain.Entities.Campus)obj;

                                        ISDAL.Add<ImmiSys.Domain.Entities.Campus>(new Entities.Campus()
                                        {
                                            Name = dbCampus.Name,
                                            PinCode = dbCampus.PinCode,
                                            StreetAddress = dbCampus.StreetAddress,
                                            City = dbCampus.City,
                                            StateId = dbCampus.StateId,
                                            CompanyId = this.EntityId,
                                            CountryId = dbCampus.CountryId,
                                            CreatedAt = DateTime.UtcNow
                                        });
                                    }
                                    else if (obj.GetType().Name == "Course")
                                    {
                                        //if Course Name / Code exists then update, else add,
                                        var dbCourse = (ImmiSys.Domain.Entities.Course)obj;

                                        if (ISDAL.IsCourseExists(dbCourse.CourseCode, dbCourse.InstitutionId, dbCourse.CricosCode))
                                        {
                                            dbCourse.Id = ISDAL.FindCourseByNameCode(dbCourse.CourseCode, dbCourse.InstitutionId, dbCourse.CricosCode);

                                            //update
                                            ISDAL.UpdateCourse(dbCourse);

                                            ISDAL.MarkActiveCourseCampus(dbCourse.Id);
                                        }
                                        else
                                        {
                                            //check if CourseCode or Cricos code available, then only add,
                                            if (string.IsNullOrWhiteSpace(dbCourse.CourseCode) && string.IsNullOrWhiteSpace(dbCourse.CricosCode))
                                            {
                                                //skip
                                            }
                                            else
                                            {
                                                ISDAL.Add<Course>(dbCourse);
                                            }
                                        }

                                    }
                                    else if (obj.GetType().Name == "CourseCampus")
                                    {
                                        var currentRow = (ImmiSys.Domain.Entities.CourseCampus)obj;
                                        currentRow.CreatedAt = DateTime.UtcNow;

                                        if (currentRow.CampusId.GetValueOrDefault() > 0 && currentRow.CourseId.GetValueOrDefault() > 0)
                                        {
                                            //check if same course campus mapping, exists, then skip,
                                            if (!ISDAL.IsCampusCourseExist(currentRow.CampusId.GetValueOrDefault(), currentRow.CourseId.GetValueOrDefault()))
                                            {
                                                //check if campus course mapping exist, then remove all of them 

                                                // add new entries
                                                ISDAL.Add<ImmiSys.Domain.Entities.CourseCampus>(new Entities.CourseCampus()
                                                {
                                                    CreatedAt = DateTime.UtcNow,
                                                    CampusId = currentRow.CampusId,
                                                    CourseId = currentRow.CourseId
                                                });
                                            }
                                        }
                                    }
                                }
                                rowsSucceeded++;
                            }
                            catch
                            {
                                rowsFailed++;
                            }
                        }

                        ImportDataCompleteEventArgs evArgs = new ImportDataCompleteEventArgs();
                        evArgs.RowsSucceeded = rowsSucceeded;
                        evArgs.RowsFailed = rowsFailed;

                        if (this.ImportCompleted != null)
                            this.ImportCompleted.Invoke(this, evArgs);
                    }
                }
            }
        }

        public void StartImport()
        {
            this.PrepareImport();

            switch (this.Extension)
            {
                case ImportDataFileFormat.Xlsx:
                    this.ImportFromExcel();
                    break;
                case ImportDataFileFormat.Csv:
                    this.ImportFromCsv();
                    break;
                case ImportDataFileFormat.Txt:
                    this.ImportFromTextFile();
                    break;
            }
        }

        private void ImportFromCsv()
        {
            //TODO: Add code to import from CSV
        }

        private void ImportFromTextFile()
        {
            //TODO: Add code to import from text file
        }

        /// <summary>
        /// Sets the value to the property
        /// </summary>
        /// <param name="obj">The object whose property is to be set</param>
        /// <param name="propertyName">The name of the property</param>
        /// <param name="value">The value to be set</param>
        private void SetPropertyValue(object obj, string propertyName, object value)
        {
            PropertyInfo pInfo = GetPropertyInfo(propertyName);
            if (pInfo != null)
            {
                Type type = pInfo.PropertyType;

                //Check for Nullable Generic types properties
                if (pInfo.PropertyType.IsGenericType && pInfo.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    type = Nullable.GetUnderlyingType(pInfo.PropertyType);

                object finalValue = (value == null) ? value : Convert.ChangeType(value, type);

                pInfo.SetValue(obj, finalValue, null);
            }
        }

        private PropertyInfo GetPropertyInfo(string propertyName)
        {
            PropertyInfo[] properties = typeof(T).GetProperties();
            PropertyInfo propInfo = null;

            foreach (PropertyInfo info in properties)
            {
                if (info.Name.Equals(propertyName))
                {
                    propInfo = info;
                    break;
                }
            }
            return propInfo;
        }
    }
}