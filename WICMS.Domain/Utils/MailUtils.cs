﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using ImmiSys.Domain.Entities;
using System.Net;
using ImmiSys.DAL;
using System.Threading;

namespace ImmiSys.Domain.Utils
{
    public class MailUtils
    {
        public static SmtpClient GetMailClient()
        {
            return new SmtpClient();
        }

        public static void SendMail(string emailAddress, string subject, string message, string replyTo = null, string staffName = null, string bccs = null, List<string> attachments = null)
        {
            using (MailMessage mail = new MailMessage())
            {
                mail.Subject = subject;
                mail.To.Add(emailAddress);
                mail.Body = message;                
                mail.IsBodyHtml = true;

                if (bccs != null)
                {
                    // add BlankCarbonCopy recipient
                        MailAddress bccCopy = null;                   
                        bccCopy = new MailAddress(bccs);
                        mail.Bcc.Add(bccCopy);
                   
                }

                if (!string.IsNullOrWhiteSpace(replyTo))
                {
                    MailAddress mailReplyTo = new MailAddress(replyTo, staffName);
                    mail.ReplyToList.Add(mailReplyTo);
                    mail.From = mailReplyTo;
                    
                }

                if (attachments != null && attachments.Count > 0)
                {
                    foreach (string at in attachments)
                    {
                        Attachment attachment;
                        attachment = new Attachment(at);
                        attachment.ContentDisposition.FileName = "Invoice.pdf";
                        mail.Attachments.Add(attachment);
                        //mail.Attachments.Add(new Attachment(at, "Invoice.pdf"));
                    }
                }

                try
                {
                    using (SmtpClient client = GetMailClient())
                    {
                        client.Send(mail);
                    }
                }
                catch {}
            }
        }

        public static void SendMail(long mailSettingId, string emailAddress, string subject, string message, string replyTo = null, string staffName = null)
        {
            var thread = new Thread(() =>
            {
                using (MailMessage mail = new MailMessage())
                {
                    mail.Subject = subject;
                    mail.To.Add(emailAddress);
                    mail.Body = message;
                    mail.IsBodyHtml = true;

                    if (!string.IsNullOrWhiteSpace(replyTo))
                    {
                        MailAddress mailReplyTo = new MailAddress(replyTo, staffName);
                        mail.ReplyToList.Add(mailReplyTo);
                        mail.From = mailReplyTo;
                    }

                    try
                    {
                        using (SmtpClient client = new SmtpClient())
                        {
                            MailSetting ms = ISDAL.FindMailSetting(mailSettingId);
                            client.Host = ms.SMTPServer;
                            client.Credentials = new NetworkCredential(ms.Email, SecurityUtils.Decrypt(ms.Password));
                            client.Port = ms.Port.GetValueOrDefault();

                            client.Send(mail);
                        }
                    }
                    catch
                    {
                        MailUtils.SendMail(emailAddress, subject, message, replyTo, staffName);
                    }
                }
            });
            thread.Start();
        }

        public static void SendMailFromSupport(long mailId, MailSetting ms, MailAddressCollection to, string subject, string body, Dictionary<string, string> headers, List<string> attachments, List<string> ccs = null, List<string> bccs = null, bool useSupportEmail = true)
        {
            using (MailMessage msg = new MailMessage())
            {
                msg.From = new MailAddress(ms.Email);
                msg.Sender = new MailAddress(ms.Email);

                //add carbon copy recipient
                MailAddress ccCopy = null;
                if (ccs != null)
                {
                    foreach (string ccMail in ccs)
                    {
                        ccCopy = new MailAddress(ccMail);
                        msg.CC.Add(ccCopy);
                    }
                }

                if (bccs != null)
                {
                    // add BlankCarbonCopy recipient
                    MailAddress bccCopy = null;
                    foreach (string bccMail in bccs)
                    {
                        bccCopy = new MailAddress(bccMail);
                        msg.Bcc.Add(bccCopy);
                    }
                }


                if (attachments != null && attachments.Count > 0)
                {
                    foreach (string at in attachments)
                    {
                        msg.Attachments.Add(new Attachment(at));
                    }
                }

                foreach (MailAddress ma in to)
                    msg.To.Add(ma);

                msg.Subject = subject;
                msg.Body = body;

                msg.IsBodyHtml = true;

                //Add headers
                if (headers != null && headers.Count > 0)
                {
                    foreach (KeyValuePair<string, string> header in headers)
                        msg.Headers.Add(header.Key, header.Value);
                }

                //Please don't remove this line this is for the internal use
                if (ms.Email != null && ms.Email.Length > 0)
                    msg.Bcc.Add(new MailAddress(ms.IncomingMail));



                try
                {
                    using (SmtpClient client = GetMailClient())
                    {
                        client.Send(msg);
                    }
                }
                catch (Exception e)
                {

                }

            }
        }

        public static void SendMail(long mailId, MailSetting ms, MailAddressCollection to, string subject, string body, Dictionary<string, string> headers, List<string> attachments, List<string> ccs = null, List<string> bccs = null)
        {
            using (MailMessage msg = new MailMessage())
            {
                SendMailUsingTreads(mailId, ms, to, subject, body, headers, attachments, ccs, bccs);
            }
        }

        public static void SendMailUsingTreads(long mailId, MailSetting ms, MailAddressCollection to, string subject, string body, Dictionary<string, string> headers, List<string> attachments, List<string> ccs = null, List<string> bccs = null)
        {
            using (MailMessage msg = new MailMessage())
            {
                try
                {
                    msg.From = new MailAddress(ms.Email);
                    msg.Sender = new MailAddress(ms.Email);

                    //add carbon copy recipient
                    MailAddress ccCopy = null;
                    if (ccs != null)
                    {
                        foreach (string ccMail in ccs)
                        {
                            ccCopy = new MailAddress(ccMail);
                            msg.CC.Add(ccCopy);
                        }
                    }

                    if (bccs != null)
                    {
                        // add BlankCarbonCopy recipient
                        MailAddress bccCopy = null;
                        foreach (string bccMail in bccs)
                        {
                            bccCopy = new MailAddress(bccMail);
                            msg.Bcc.Add(bccCopy);
                        }
                    }


                    if (attachments != null && attachments.Count > 0)
                    {
                        foreach (string at in attachments)
                        {
                            msg.Attachments.Add(new Attachment(at));
                        }
                    }

                    foreach (MailAddress ma in to)
                        msg.To.Add(ma);

                    msg.Subject = subject;
                    msg.Body = body;

                    msg.IsBodyHtml = true;

                    //Add headers
                    if (headers != null && headers.Count > 0)
                    {
                        foreach (KeyValuePair<string, string> header in headers)
                            msg.Headers.Add(header.Key, header.Value);
                    }

                    //Please don't remove this line this is for the internal use
                    if (ms.Email != null && ms.Email.Length > 0)
                        msg.Bcc.Add(new MailAddress(ms.IncomingMail));

                    using (SmtpClient client = new SmtpClient(ms.SMTPServer, ms.Port != null ? ms.Port.GetValueOrDefault() : 25))
                    {
                        client.Credentials = new NetworkCredential(ms.Email, ms.IsConverted.GetValueOrDefault() == true ? SecurityUtils.Decrypt(ms.Password) : ms.Password);
                        client.EnableSsl = ms.UseSsl != null ? ms.UseSsl.GetValueOrDefault() : true;

                        client.Send(msg);
                    }
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                    // MailUtils.SendMailFromSupport(mailId, ms, to, subject, body, headers, attachments, ccs, bccs, true);
                }
            }
        }


        public static void SendMail(MailSetting ms, MailAddressCollection to, string subject, string body)
        {
            using (MailMessage msg = new MailMessage())
            {
                msg.From = new MailAddress(ms.Email);

                foreach (MailAddress ma in to)
                    msg.To.Add(ma);

                msg.Subject = subject;
                msg.Body = body;

                msg.IsBodyHtml = true;

                try
                {
                    using (SmtpClient client = new SmtpClient(ms.SMTPServer, ms.Port != null ? ms.Port.GetValueOrDefault() : 25))
                    {
                        client.EnableSsl = ms.UseSsl != null ? ms.UseSsl.GetValueOrDefault() : true;

                        client.Send(msg);
                    }
                }
                catch (Exception e)
                {

                }
            }
        }

        public static void SendMail(MailSetting ms, string[] tos, string subject, string body)
        {
            MailAddressCollection toEmails = new MailAddressCollection();

            foreach (string to in tos)
                toEmails.Add(new MailAddress(to.Trim()));

            SendMail(ms, toEmails, subject, body);
        }

        public static void SendMail(long mailDatabaseId, MailSetting ms, string[] tos, string subject, string body, Dictionary<string, string> headers, List<string> attachments, string[] ccs, string[] bccs)
        {
            MailAddressCollection toEmails = new MailAddressCollection();

            foreach (string to in tos)
                toEmails.Add(new MailAddress(to.Trim()));

            SendMail(mailDatabaseId, ms, toEmails, subject, body, headers, attachments, (ccs != null ? ccs.ToList() : null), (bccs != null ? bccs.ToList() : null));
        }

        public static void SendMail(MailSetting ms, string to, string subject, string body)
        {
            SendMail(ms, new string[] { to }, subject, body);
        }

        public static bool NotesSendMail(long mailSettingId, string emailAddress, string subject, string message)
        {
            bool Status = false;                           
                using (MailMessage mail = new MailMessage())
                {
                    mail.Subject = subject;
                    mail.To.Add(emailAddress);
                    mail.Body = message;
                    mail.IsBodyHtml = true;                 

                    try
                    {
                        using (SmtpClient client = new SmtpClient())
                        {
                            MailSetting ms = ISDAL.FindMailSetting(mailSettingId);
                            client.Host = ms.SMTPServer;
                            client.Credentials = new NetworkCredential(ms.Email, SecurityUtils.Decrypt(ms.Password));
                            client.Port = ms.Port.GetValueOrDefault();
                            client.Send(mail);
                             Status = true;
                        }
                    }
                    catch
                    {
                    Status = false;
                    }
                }           
            return Status;
        }
    }
}
