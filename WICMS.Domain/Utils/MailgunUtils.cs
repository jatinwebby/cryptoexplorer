﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using WICMS.Domain.Entities;
using RestSharp;
using RestSharp.Authenticators;

namespace WICMS.Domain.Utils
{
    public class MailgunUtils
    {
        public static string ImmiSysHeader = "X-WICMS-Mail";

        public static string MailGunApiKey
        {
            get
            {
                string apiKey = ConfigurationManager.AppSettings["MAILGUN_API"];
                if (!string.IsNullOrEmpty(apiKey))
                    return apiKey;
                else
                    return "key-07b807805e934b4db836ae21219f6d11";
            }
        }

        public static string MailGunDomain
        {
            get
            {
                string mailGunDomain = ConfigurationManager.AppSettings["MAILGUN_DOMAIN"];
                if (!string.IsNullOrEmpty(mailGunDomain))
                    return mailGunDomain;
                else
                    return "application.WICMS.com.au";
            }
        }

        public static string Get(Dictionary<string, object> parameters, string action)
        {
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://api.mailgun.net/v3");
            client.Authenticator = new HttpBasicAuthenticator("api", MailGunApiKey);

            RestRequest request = new RestRequest();
            request.AddParameter("Domain", MailGunDomain, ParameterType.UrlSegment);

            if (action == "messages")
                request.Resource = string.Format("{0}/{1}/{2}/{3}", "domains", MailGunDomain, action, parameters["key"]);
            else
                request.Resource = string.Format("{0}/{1}", MailGunDomain, action);

            foreach (KeyValuePair<string, object> p in parameters)
            {
                request.AddParameter(p.Key, p.Value);
            }

            return client.Execute(request).Content;
        }

        public static string GetEvents(Dictionary<string, object> parameters)
        {
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://api.mailgun.net/v3");
            client.Authenticator = new HttpBasicAuthenticator("api", MailGunApiKey);

            RestRequest request = new RestRequest();
            request.AddParameter("Domain", MailGunDomain, ParameterType.UrlSegment);

            request.Resource = string.Format("{0}/events", MailGunDomain);

            foreach (KeyValuePair<string, object> p in parameters)
            {
                request.AddParameter(p.Key, p.Value);
            }

            return client.Execute(request).Content;
        }

        public static string GetEvents(DateTime from, string eventType, string recipient = null)
        {

            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://api.mailgun.net/v3");
            client.Authenticator = new HttpBasicAuthenticator("api", MailGunApiKey);

            RestRequest request = new RestRequest();
            request.AddParameter("Domain", MailGunDomain, ParameterType.UrlSegment);

            request.Resource = string.Format("{0}/events", MailGunDomain);

            request.AddParameter("begin", from.ToString("ddd, d MMM yyyy hh:00:00 -0000"));
            request.AddParameter("pretty", "yes");
            request.AddParameter("ascending", "yes");
            request.AddParameter("event", "accepted");
            request.AddParameter("limit", 300);
            request.AddParameter("to", recipient);
            return client.Execute(request).Content;

        }

        public static string GetEvents(string param)
        {
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://api.mailgun.net/v3");
            client.Authenticator = new HttpBasicAuthenticator("api", MailGunApiKey);

            RestRequest request = new RestRequest();
            request.AddParameter("Domain", MailGunDomain, ParameterType.UrlSegment);
            request.Resource = string.Format("{0}/events/{1}", MailGunDomain, param);

            return client.Execute(request).Content;
        }

        public static string GetStoredEvents(DateTime from, string recipientAddress = null)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("begin", from.ToString("ddd, d MMM yyyy hh:00:00 -0000"));
            parameters.Add("pretty", "yes");
            parameters.Add("ascending", "yes");
            parameters.Add("event", "stored");
            //parameters.Add("event", "bounces");
            parameters.Add("limit", 300);
            return GetEvents(parameters);
        }

        public static string GetMessageFromUrl(string keyWithUrl)
        {
            var parameters = new Dictionary<string, object>();

            parameters.Add("pretty", "yes");
            parameters.Add("ascending", "yes");
            parameters.Add("event", "stored");

            RestClient client = new RestClient();
            client.BaseUrl = new Uri(keyWithUrl);
            client.Authenticator = new HttpBasicAuthenticator("api", MailGunApiKey);

            RestRequest request = new RestRequest();
            request.AddParameter("Domain", MailGunDomain, ParameterType.UrlSegment);

            foreach (KeyValuePair<string, object> p in parameters)
            {
                request.AddParameter(p.Key, p.Value);
            }

            return client.Execute(request).Content;
        }


        public static string DeleteMessageByURL(string url)
        {
            //Delete message
            RestClient client = new RestClient();
            client.BaseUrl = new Uri(url);
            client.Authenticator = new HttpBasicAuthenticator("api", MailGunApiKey);

            RestRequest request = new RestRequest(Method.DELETE);
            request.AddParameter("mailDomain", MailGunDomain, ParameterType.UrlSegment);
            return client.Execute(request).Content;

        }


        public static void DownloadFile(string url, string filePath)
        {

            using (var writer = File.OpenWrite(filePath))
            {
                
                RestClient client = new RestClient();
                client.BaseUrl = new Uri(url);
                client.Authenticator = new HttpBasicAuthenticator("api", MailGunApiKey);

                RestRequest request = new RestRequest(Method.GET);

                request.AddParameter("mailDomain", MailGunDomain, ParameterType.UrlSegment);

                request.ResponseWriter = (responseStream) => responseStream.CopyTo(writer);
                var response = client.Execute(request);


            }

        }

       

    }

    public class NewMailGunUtils
    {
        public static string EventsDateTimeRecipient(DateTime from, string recipientAddress = null)
        {
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://api.mailgun.net/v3");
            client.Authenticator =
                new HttpBasicAuthenticator("api",
                                            ConfigurationManager.AppSettings["MAILGUN_API"]);
            RestRequest request = new RestRequest();
            request.AddParameter("domain", ConfigurationManager.AppSettings["MAILGUN_DOMAIN"], ParameterType.UrlSegment);
            request.Resource = "{domain}/events";
            request.AddParameter("begin", from.ToString("ddd, d MMM yyyy hh:00:00 -0000"));
            request.AddParameter("ascending", "yes");
            request.AddParameter("limit", 300);
            request.AddParameter("pretty", "yes");
            request.AddParameter("event", "accepted");
            if (!string.IsNullOrWhiteSpace(recipientAddress))  
                request.AddParameter("to", recipientAddress.ToLower()); 
            return client.Execute(request).Content;
        }

        public static IRestResponse GetBounces()
        {
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://api.mailgun.net/v3");
            client.Authenticator =
                new HttpBasicAuthenticator("api", ConfigurationManager.AppSettings["MAILGUN_API"]);
            RestRequest request = new RestRequest();
            request.AddParameter("domain", ConfigurationManager.AppSettings["MAILGUN_DOMAIN"], ParameterType.UrlSegment);
            request.Resource = "{domain}/bounces";
            return client.Execute(request);
        }
    }
}
