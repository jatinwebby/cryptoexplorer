﻿using ImmiSys.DAL;
using ImmiSys.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ImmiSys.Domain.Utils
{
    public class NotificationUtils
    {
        public static string GetNotificationContent(string entityName, long entityId)
        {
            if (entityId > 0 && (!string.IsNullOrEmpty(entityName)))
            {
                string content = string.Empty;

                switch (entityName)
                {
                    case Constants.NOTIFICATION_CASETEMPLATE:
                        {
                            CaseTemplate dbTemplate = ISDAL.FindCaseTemplate(entityId);
                            if (dbTemplate != null)
                            {
                                if (dbTemplate.Name != null)
                                    content = "New Case Template, " + dbTemplate.Name;
                            }
                            break;
                        }

                    case Constants.NOTIFICATION_DOCUMENTCHECKLIST:
                        {
                            DocumentRequiredSet dbChecklist = ISDAL.FindDocumentRequiredSet(entityId);
                            if (dbChecklist != null)
                            {
                                if (dbChecklist.Name != null)
                                    content = "Document Checklist, " + dbChecklist.Name;
                            }
                            break;
                        }

                    case Constants.NOTIFICATION_NEWCLIENT:
                        {
                            content = "New Client Arrive";
                            break;
                        }


                    default:
                        break;
                }
                return content;
            }
            else
                return null;
        }

        public static string GetRedirectUrlForCustomer(long notificationId, long? entityId = null)
        {
            string url = string.Empty;
            if (notificationId > 0)
            {
                Notification dbNotification = ISDAL.FindNotification(notificationId);
                if (dbNotification != null)
                {
                    switch (dbNotification.EntityName)
                    {
                        case Constants.NOTIFICATION_CASETEMPLATE:
                            {
                                url = "CaseTemplate?Type=ImmiSysTemplates";
                                break;
                            }
                        case Constants.NOTIFICATION_DOCUMENTCHECKLIST:
                            {
                                url = "Manage/DocumentSets?Type=ImmisysSet";
                                break;
                            }
                        case Constants.NOTIFICATION_NEWCLIENT:
                            {
                                url = "Manage/Client?id=" + entityId;
                                break;
                            }
                        case Constants.NOTIFICATION_NEWSUBAGENT:
                            {
                                url = "Manage/SubAgent?id=" + entityId;
                                break;
                            }
                        case Constants.NOTIFICATION_NEW_TASK_CREATED:
                            {
                                //Check if no case is associated , then redirect it to Task details,
                                if (ISDAL.IsCaseAssociateWithTask(entityId.GetValueOrDefault()))
                                {
                                    url = "Manage/Case?id=" + ISDAL.FindCaseIdByTask(entityId);
                                }
                                else
                                {
                                    url = "Manage/Tasks";
                                }
                                break;
                            }
                        case Constants.NOTIFICATION_NEW_CASE_NOTE_CREATED:
                            {
                                url = "Manage/Case?id=" + ISDAL.FindCaseIdForNote(entityId);
                                break;
                            }
                        case Constants.NOTIFICATION_NEW_OPPORTUNITY_NOTE_CREATED:
                            {
                                url = "Manage/OpportunityDetails?id=" + ISDAL.FindOpportunityIdForNote(entityId);
                                break;
                            }
                        case Constants.NOTIFICATION_NEW_SUPPLIER_NOTE_CREATED:
                            {
                                url = "Manage/SupplierNew?ActiveTab=SupplierNotes&id=" + ISDAL.FindSupplierIdForNote(entityId);
                                break;
                            }
                        case Constants.NOTIFICATION_NEW_CLIENT_NOTE_CREATED:
                            {
                                url = "Manage/Client/" + ISDAL.FindClientIdForNote(entityId) + "/Notes";
                                break;
                            }
                        case Constants.NOTIFICATION_NEW_EMPLOYER_NOTE_CREATED:
                            {
                                url = "Manage/EmployerDetails?ActiveTab=EmployerNotes&id=" + ISDAL.FindEmployerIdForNote(entityId);
                                break;
                            }
                        case Constants.NOTIFICATION_NEW_AGENT_NOTE_CREATED:
                            {
                                url = "Manage/SubAgent?tabName=SubagentNotes&id=" + ISDAL.FindAgentIdForNote(entityId);
                                break;
                            }
                        case Constants.NOTIFICATION_ADMIN_COMPLAINT_STATUS:
                            {
                                url = "Manage/Complaints?id=" + entityId;
                                break;
                            }
                        case Constants.NOTIFICATION_NEW_OPPORTUNITY_CREATED:
                            {
                                url = "Manage/OpportunityDetails?id=" + entityId;
                                break;
                            }
                        case Constants.NOTIFICATION_NEW_CASE_CREATED:
                            {
                                url = "Manage/Case?id=" + entityId;
                                break;
                            }

                        case Constants.NOTIFICATION_NEW_ENROLMENTS_CREATED:
                            {
                                url = "Manage/Case?id=" + entityId;
                                break;
                            }
                        case Constants.NOTIFICATION_NEW_CLIENT_CREATED:
                            {
                                url = "Manage/Client?id=" + entityId;
                                break;
                            }
                        case Constants.NOTIFICATION_TRANSFER_OPPORTUNITY_CREATED:
                            {
                                long OpportunityID = ISDAL.GetRefrenceIdbyEntityId(Convert.ToInt64(entityId));
                                if (OpportunityID != 0)
                                {
                                    url = "Manage/OpportunityDetails?id=" + OpportunityID;
                                }
                                else
                                {
                                    url = "";
                                }                               
                                break;
                            }
                        case Constants.NOTIFICATION_TRANSFER_ENROLMENTS_CREATED:
                            {
                                long CaseID = ISDAL.GetRefrenceIdbyEntityId(Convert.ToInt64(entityId));
                                if (CaseID != 0)
                                {
                                    url = "Manage/Case?id=" + CaseID;
                                }
                                else
                                {
                                    url = "";
                                }
                                break;
                            }
                        case Constants.NOTIFICATION_TRANSFER_CASE_CREATED:
                            {
                                long CaseID = ISDAL.GetRefrenceIdbyEntityId(Convert.ToInt64(entityId));
                                if (CaseID != 0)
                                {
                                    url = "Manage/Case?id=" + CaseID;
                                }
                                else
                                {
                                url = "";
                            }
                            break;
                            }
                        case Constants.NOTIFICATION_TRANSFER_CLIENT_CREATED:
                            {
                                long ClientID = ISDAL.GetRefrenceIdbyEntityId(Convert.ToInt64(entityId));
                                if (ClientID != 0)
                                {
                                    url = "Manage/Client?id=" + ClientID;
                                }
                                else
                                {
                                    url = "";
                                }
                                break;
                            }
                        case Constants.NOTIFICATION_SHARED_OPPORTUNITY_CREATED:
                            {
                                long OpportunityID = ISDAL.GetRefrenceIdbyEntityId(Convert.ToInt64(entityId));
                                if (OpportunityID != 0)
                                {
                                    url = "Manage/OpportunityDetails?id=" + OpportunityID;
                                }
                                else
                                {
                                    url = "";
                                }
                                break;
                            }
                        case Constants.NOTIFICATION_SHARED_ENROLMENTS_CREATED:
                            {
                                long CaseID = ISDAL.GetRefrenceIdbyEntityId(Convert.ToInt64(entityId));
                                if (CaseID != 0)
                                {
                                    url = "Manage/Case?id=" + CaseID;
                                }
                                else
                                {
                                    url = "";
                                }
                                break;
                            }
                        case Constants.NOTIFICATION_SHARED_CASE_CREATED:
                            {
                                long CaseID = ISDAL.GetRefrenceIdbyEntityId(Convert.ToInt64(entityId));
                                if (CaseID != 0)
                                {
                                    url = "Manage/Case?id=" + CaseID;
                                }
                                else
                                {
                                    url = "";
                                }
                                break;
                            }
                        case Constants.NOTIFICATION_SHARED_CLIENT_CREATED:
                            {
                                long ClientID = ISDAL.GetRefrenceIdbyEntityId(Convert.ToInt64(entityId));
                                if (ClientID != 0)
                                {
                                    url = "Manage/Client?id=" + ClientID;
                                }
                                else
                                {
                                    url = "";
                                }
                                break;                                
                            }
                        case Constants.CASE:
                            {
                                url = "Manage/Case?id=" + ISDAL.FindCaseIdForNote(entityId);
                                break;
                            }
                        case Constants.DOCUMENT_UPLOAD:
                            {
                                long ClientId = ISDAL.GetClientIdByEntityId(entityId.GetValueOrDefault());
                                if (ClientId != 0)
                                {
                                    url = "/Manage/Client?id=" + ClientId + "&section=Documents";
                                }
                                else
                                {
                                    url = "";
                                }
                                break;
                            }
                        case Constants.LICENCE_EXPIRED:
                            {
                                url = "Account/RenewCheckOut";
                                break;
                            }
                        default:
                            break;
                    }
                }
            }
            return url;
        }

        public static string GetNotificationTitle(long notificationId, long? entityId = null)
        {

            string title = string.Empty;
            if (notificationId > 0)
            {
                Notification dbNotification = ISDAL.FindNotification(notificationId);
                if (dbNotification != null)
                {
                    switch (dbNotification.EntityName)
                    {
                        case Constants.NOTIFICATION_CASETEMPLATE:
                            {
                                title = "Case Template";
                                break;
                            }
                        case Constants.NOTIFICATION_DOCUMENTCHECKLIST:
                            {
                                title = "Document Checklist";
                                break;
                            }
                        case Constants.NOTIFICATION_MANUAL:
                            {
                                title = "ImmiSys Notification";
                                break;
                            }
                        case Constants.NOTIFICATION_NEWCLIENT:
                            {
                                try
                                {
                                    string ClientName = ISDAL.FindClientNameById(entityId.GetValueOrDefault());
                                    title = "New Client  Register, '" + ClientName + "'";
                                }
                                catch
                                {
                                    string ClientName = "";
                                    title = "";
                                }
                                break;
                            }
                        case Constants.NOTIFICATION_NEWSUBAGENT:
                            {
                                string subAgentName = ISDAL.FindSubAgentById(entityId.GetValueOrDefault());
                                title = "New Sub-agent Register, '" + subAgentName + "'";
                                break;
                            }
                        case Constants.NOTIFICATION_ADMIN_COMPLAINT_STATUS:
                            {
                                title = "Complaint status";
                                break;
                            }
                        case Constants.NOTIFICATION_NEW_TASK_CREATED:
                            {
                                title = "New task";
                                break;
                            }
                        case Constants.NOTIFICATION_NEW_CASE_NOTE_CREATED:
                            {
                                // title = "New Case note";
                                title = "";
                                break;
                            }
                        case Constants.NOTIFICATION_NEW_EMPLOYER_NOTE_CREATED:
                            {
                                // title = "New Employer note";
                                title = "";
                                break;
                            }
                        case Constants.NOTIFICATION_NEW_SUPPLIER_NOTE_CREATED:
                            {
                                // title = "New Supplier note";
                                title = "";
                                break;
                            }
                        case Constants.NOTIFICATION_NEW_OPPORTUNITY_NOTE_CREATED:
                            {
                               // title = "New Opportunity note";
                                title = "";
                                break;
                            }
                        case Constants.NOTIFICATION_NEW_AGENT_NOTE_CREATED:
                            {
                                // title = "New Agent note";
                                title = "";
                                break;
                            }
                        case Constants.NOTIFICATION_NEW_CASE_CREATED:
                            {
                                string ClientName = ISDAL.FindClientNameByCaseId(entityId.GetValueOrDefault());
                                if (!string.IsNullOrWhiteSpace(ClientName))
                                {
                                    string customerName = ISDAL.FindNotificationAddedBynotificationID(notificationId);
                                    title = "Case for \"" + ClientName + "\" is added to you by " + customerName;
                                }
                                else
                                {
                                    title = "";
                                }                          
                                break;
                            }
                        case Constants.NOTIFICATION_NEW_OPPORTUNITY_CREATED:
                            {
                                string ClientName = ISDAL.FindClientNameByOpportunityId(entityId.GetValueOrDefault());
                                if(! string.IsNullOrWhiteSpace(ClientName))
                                {
                                    string customerName = ISDAL.FindNotificationAddedBynotificationID(notificationId);
                                    title = "Opportunity for \"" + ClientName + "\" is added to you by " + customerName;
                                }
                                else
                                {
                                    title = "";
                                }                       
                                break;
                            }
                        case Constants.NOTIFICATION_NEW_ENROLMENTS_CREATED:
                            {
                                string ClientName = ISDAL.FindClientNameByCaseId(entityId.GetValueOrDefault());
                                if (!string.IsNullOrWhiteSpace(ClientName))
                                {
                                    string customerName = ISDAL.FindNotificationAddedBynotificationID(notificationId);
                                    title = "Enrolment for \"" + ClientName + "\" is added to you by " + customerName;
                                }
                                else
                                {
                                    title = "";
                                }                         
                                break;
                            }
                        case Constants.NOTIFICATION_NEW_CLIENT_CREATED:
                            {                               
                               string ClientName = ISDAL.FindClientNameById(entityId.GetValueOrDefault());
                                if (!string.IsNullOrWhiteSpace(ClientName))
                                {
                                    string customerName = ISDAL.FindNotificationAddedBynotificationID(notificationId);
                                    title = "Client \"" + ClientName + "\" is added to you by " + customerName;
                                }
                                else
                                {
                                    title = "";
                                }                           
                                break;
                            }
                            
                        case Constants.NOTIFICATION_TRANSFER_OPPORTUNITY_CREATED:
                            {
                                string ClientName = ISDAL.FindClientNameByEntityID(entityId.GetValueOrDefault());
                                if (!string.IsNullOrWhiteSpace(ClientName))
                                {
                                    string customerName = ISDAL.FindNotificationAddedBynotificationID(notificationId);
                                    title = "Opportunity for \"" + ClientName + "\" is transferred to you by " + customerName;
                                }
                                else
                                {
                                    title = "";
                                }                           
                                break;
                            }
                        case Constants.NOTIFICATION_TRANSFER_ENROLMENTS_CREATED:
                            {
                                string ClientName = ISDAL.FindClientNameByEntityID(entityId.GetValueOrDefault());
                                if (!string.IsNullOrWhiteSpace(ClientName))
                                {
                                    string customerName = ISDAL.FindNotificationAddedBynotificationID(notificationId);
                                    title = "Enrolment for \"" + ClientName + "\" is transferred to you by " + customerName;
                                }
                                else
                                {
                                    title = "";
                                }                             
                                break;
                            }
                        case Constants.NOTIFICATION_TRANSFER_CASE_CREATED:
                            {
                                string ClientName = ISDAL.FindClientNameByEntityID(entityId.GetValueOrDefault());
                                if (!string.IsNullOrWhiteSpace(ClientName))
                                {
                                    string customerName = ISDAL.FindNotificationAddedBynotificationID(notificationId);
                                    title = "Case for \"" + ClientName + "\" is transferred to you by " + customerName;
                                }
                                else
                                {
                                    title = "";
                                }                         
                                break;
                            }
                        case Constants.NOTIFICATION_TRANSFER_CLIENT_CREATED:
                            {
                                string clientName = ISDAL.FindClientNameByEntityID(entityId.GetValueOrDefault());
                                if (!string.IsNullOrWhiteSpace(clientName))
                                {
                                    string customerName = ISDAL.FindNotificationAddedBynotificationID(notificationId);
                                    title = "Client \"" + clientName + "\" is transferred to you by " + customerName;
                                }
                                else
                                {
                                    title = "";
                                }                            
                                break;
                            }
                        case Constants.NOTIFICATION_SHARED_OPPORTUNITY_CREATED:
                            {
                                string ClientName = ISDAL.FindClientNameByEntityID(entityId.GetValueOrDefault());
                                if (!string.IsNullOrWhiteSpace(ClientName))
                                {
                                    string customerName = ISDAL.FindNotificationAddedBynotificationID(notificationId);
                                    title = "Opportunity for \"" + ClientName + "\" is shared to you by " + customerName;
                                }
                                else
                                {
                                    title = "";
                                }                             
                                break;
                            }
                        case Constants.NOTIFICATION_SHARED_ENROLMENTS_CREATED:
                            {
                                string ClientName = ISDAL.FindClientNameByEntityID(entityId.GetValueOrDefault());
                                if (!string.IsNullOrWhiteSpace(ClientName))
                                {
                                    string customerName = ISDAL.FindNotificationAddedBynotificationID(notificationId);
                                    title = "Enrolment for \"" + ClientName + "\" is shared to you by " + customerName;
                                }
                                else
                                {
                                    title = "";
                                }                        
                                break;
                            }
                        case Constants.NOTIFICATION_SHARED_CASE_CREATED:
                            {
                                string ClientName = ISDAL.FindClientNameByEntityID(entityId.GetValueOrDefault());
                                if (!string.IsNullOrWhiteSpace(ClientName))
                                {
                                    string customerName = ISDAL.FindNotificationAddedBynotificationID(notificationId);
                                    title = "Case for \"" + ClientName + "\" is shared to you by " + customerName;
                                }
                                else
                                {
                                    title = "";
                                }                        
                                break;
                            }
                        case Constants.NOTIFICATION_SHARED_CLIENT_CREATED:
                            {                                
                                string clientName = ISDAL.FindClientNameByEntityID(entityId.GetValueOrDefault());
                                if (!string.IsNullOrWhiteSpace(clientName))
                                {
                                    string customerName = ISDAL.FindNotificationAddedBynotificationID(notificationId);
                                    title = "Client \"" + clientName + "\" is shared to you by " + customerName;                                   
                                }
                               else
                                {
                                    title = "";                                   
                                }
                                break;
                            }
                        default:
                            break;
                    }
                }
            }
            return title;
        }

        public static void GenerateNotificationForStaffs(long CustId, long EntityId, string NotificationContent)
        {
            string notifyType = string.Empty;
            long customerId = CustId;

            notifyType = Constants.NOTIFICATION_NEWCLIENT;

            ISDAL.AddNotification(notifyType, EntityId, NotificationContent, null, false, customerId, 0);
            //generate for staff now
            foreach (var staff in ISDAL.GetStaffMembers(customerId))
            {
                ISDAL.AddNotification(notifyType, EntityId, NotificationContent, customerId, false, customerId, staffId: staff.Id);
            }
        }

        public static void GenerateMessageNotificationForStaff(long CustId, long staffId, long EntityId, string entityType, string NotificationContent, long currentStaffId)
        {
            string notifyType = string.Empty;
            long customerId = CustId;

            notifyType = entityType;

            ISDAL.AddNotification(notifyType, EntityId, NotificationContent, currentStaffId, false, customerId, staffId: staffId);
        }

        public static void GenerateNotificationForCustomerAndStaffs(long CustId, long EntityId, string NotificationContent)
        {
            string notifyType = string.Empty;
            long customerId = CustId;

            notifyType = Constants.NOTIFICATION_NEWSUBAGENT;

            ISDAL.AddNotification(notifyType, EntityId, NotificationContent, null, false, customerId, 0);

            //generate for staff now
            foreach (var staff in ISDAL.GetStaffMembers(customerId))
            {
                ISDAL.AddNotification(notifyType, EntityId, NotificationContent, customerId, false, customerId, staffId: staff.Id);
            }
        }

        public static void GenerateNotificationForCustomerComplaint(long CustId, long EntityId, string NotificationContent)
        {
            string notifyType = string.Empty;
            long customerId = CustId;

            notifyType = Constants.NOTIFICATION_ADMIN_COMPLAINT_STATUS;

            ISDAL.AddNotification(notifyType, EntityId, NotificationContent, null, false, customerId, 0);
        }

    }
}
