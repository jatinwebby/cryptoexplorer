﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
namespace WICMS.Domain.Utils
{
    /// <summary>
    /// Summary description for SecurityUtils
    /// </summary>
    public class SecurityUtils
    {
        private const string ENCRYPTION_KEY = "#!WICMS!#";
        private readonly static byte[] SALT = Encoding.ASCII.GetBytes(ENCRYPTION_KEY.Length.ToString());

        /// <summary>
        /// Encrypts the data to base64 encoding
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string EncryptBase64(object data)
        {
            if (data == null && data == DBNull.Value)
                return null;

            string encryptedData = string.Empty;
            byte[] toEncodeAsBytes = ASCIIEncoding.ASCII.GetBytes(data.ToString());
            encryptedData = Convert.ToBase64String(toEncodeAsBytes);
            return encryptedData;
        }

        /// <summary>
        /// Decrypts data from base64 string
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string DecryptBase64(string data)
        {
            string decryptedData = string.Empty;
            if (!string.IsNullOrEmpty(data))
            {
                byte[] encryptedDataAsBytes = Convert.FromBase64String(data);
                decryptedData = ASCIIEncoding.ASCII.GetString(encryptedDataAsBytes);
            }
            return decryptedData;
        }

        /// <summary>
        /// Encrypts any string using the Rijndael algorithm.
        /// </summary>
        /// <param name="inputText">The string to encrypt.</param>
        /// <returns>A Base64 encrypted string.</returns>
        public static string Encrypt(object inputText)
        {
            if (inputText == null)
                return null;

            RijndaelManaged rijndaelCipher = new RijndaelManaged();
            byte[] plainText = Encoding.Unicode.GetBytes(inputText.ToString());
            PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(ENCRYPTION_KEY, SALT);

            using (ICryptoTransform encryptor = rijndaelCipher.CreateEncryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16)))
            {
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(plainText, 0, plainText.Length);
                        cryptoStream.FlushFinalBlock();
                        return Convert.ToBase64String(memoryStream.ToArray());
                    }
                }
            }
        }

        /// <summary>
        /// Decrypts a previously encrypted string.
        /// </summary>
        /// <param name="inputText">The encrypted string to decrypt.</param>
        /// <returns>A decrypted string.</returns>
        public static string Decrypt(string inputText)
        {
            RijndaelManaged rijndaelCipher = new RijndaelManaged();
            inputText = inputText.Replace(" ", "+");
            byte[] encryptedData = Convert.FromBase64String(inputText);
            PasswordDeriveBytes secretKey = new PasswordDeriveBytes(ENCRYPTION_KEY, SALT);

            using (ICryptoTransform decryptor = rijndaelCipher.CreateDecryptor(secretKey.GetBytes(32), secretKey.GetBytes(16)))
            {
                using (MemoryStream memoryStream = new MemoryStream(encryptedData))
                {
                    using (CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                    {
                        byte[] plainText = new byte[encryptedData.Length];
                        int decryptedCount = cryptoStream.Read(plainText, 0, plainText.Length);
                        return Encoding.Unicode.GetString(plainText, 0, decryptedCount);
                    }
                }
            }
        }
    }
}