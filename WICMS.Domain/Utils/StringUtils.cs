﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
namespace WICMS.Domain.Utils
{
    public class StringUtils
    {
        public static string ToTitleCase(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return value;

            string changedValue = null;

            try
            {
                TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                changedValue = textInfo.ToTitleCase(value);
            }
            catch { }
            return changedValue;
        }
        public static bool IsH_tml(string html)
        {
            return Regex.IsMatch(html, "<(.|\n)*?>");
        }
     
    public static string ConvertToString(Object f_value)
        {
            string l_stResult = String.Empty;
            try
            {
                if (f_value == null)
                    return l_stResult;

                l_stResult = Convert.ToString(f_value);
            }
            catch (Exception) { return l_stResult; }

            return l_stResult;
        }
    }
}
