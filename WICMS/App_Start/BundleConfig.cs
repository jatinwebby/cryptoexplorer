﻿using System.Web;
using System.Web.Optimization;

namespace WiCms
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/Scripts/Header").Include(
                      "~/Scripts/vendor/modernizr-2.6.2.min.js",
                      "~/Scripts/jquery-1.10.2.min.js",
                      "~/Areas/Admin/scripts/lib/moment.js"
                      ));

            bundles.Add(new ScriptBundle("~/Scripts/Footer").Include(
                      "~/Scripts/compressed.js",
                      "~/Scripts/main.js",
                      //"~/Scripts/switcher.js",
                      "~/Scripts/toastr.js",
                      //"~/Scripts/jquery.validate.min.js",
                      "~/Scripts/app.js"
                       ));
            
            bundles.Add(new StyleBundle("~/Content/css").Include(
                    "~/Content/bootstrap.min.css",
                    "~/Content/animations.css",
                    "~/Content/fonts.css", 
                    "~/Content/toastr.css",
                    "~/Content/main.css",
                    "~/Content/shop.css"));

            // ----- Admin Bundle ----- //
            bundles.Add(new StyleBundle("~/Admin/Styles/css").Include(
                  "~/Areas/Admin/Styles/styles.css",
                  "~/Areas/Admin/Styles/bootstrap.css",
                  "~/Areas/Admin/Styles/bootstrap-project.css",
                  "~/Areas/Admin/Styles/bootstrap-responsive.css",
                  "~/Areas/Admin/Styles/toastr.css",
                  "~/Areas/Admin/Styles/font-awesome.css",
                  "~/Areas/Admin/Styles/jquery-ui.css"
                  ));

            bundles.Add(new ScriptBundle("~/Admin/Scripts/lib").Include(
                //"~/Areas/Admin/scripts/lib/jquery-2.1.3.js",
                "~/Areas/Admin/scripts/lib/bootstrap.min.js",
                "~/Areas/Admin/scripts/lib/moment.js",
                "~/Areas/Admin/scripts/lib/toastr.js"
                ));

            bundles.Add(new ScriptBundle("~/Admin/Scripts/app").Include(
                "~/Areas/Admin/scripts/lib/jquery/jquery.validate.js",
                "~/Areas/Admin/scripts/lib/jquery/jquery-ui.custom.js",
                "~/Areas/Admin/scripts/lib/jquery/jquery.dataTables.js",
                "~/Areas/Admin/scripts/app.js"
                ));
            BundleTable.EnableOptimizations = false;
             
        }
    }
}
