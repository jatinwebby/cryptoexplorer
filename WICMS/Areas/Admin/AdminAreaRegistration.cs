﻿using System.Web.Mvc;

namespace WiCms.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        { 
            context.MapRoute(
              name: "AdminAction",
              url: "Admin/{controller}/{action}/{guid}",
              defaults: new { controller = "Login", action = "Index", guid = UrlParameter.Optional },
              constraints:
                  new
                  {
                      guid =
                          @"^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$"
                  });

            context.MapRoute(
              name: "AdminCategoryAction",
              url: "Admin/Category/{action}/{guid}",
              defaults: new { action = "Index", guid = UrlParameter.Optional },
              constraints:
                  new
                  {
                      guid =
                          @"^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$"
                  });

            context.MapRoute(
                "Admin_default",
                "Admin/{controller}/{action}/{id}",
                new
                {
                    Controller = "Login",
                    action = "Index",
                    id = UrlParameter.Optional
                });

            //context.MapRoute("Captcha", "DefaultCaptcha/Generate/", new { controller = "DefaultCaptcha", action = "Generate" });
        }
    }
}