﻿using BLL.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WiCms.Areas.Admin.Controllers
{
    [Authorize]
    public class BroadCastMailController : AdminBaseController
    {
        // GET: Admin/BroadCastMail
        public ActionResult Index()
        {
            ViewBag.PageName = BLL.Tools.Constants.PageName.BroadcastMail;
            var data = BLL.Queries.CMSManage.GetByPageName("BROADCAST_MAIL");
            return View(data);
        }
        public JsonResult GetSubscriber()
        {
            var data = BLL.Queries.Subscriber.GetAll();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost,ValidateInput(false)]
        public JsonResult Post(FormCollection data)
        {
            var results = new BLL.ModelViews.JsonMessageView();
            try
            {

                //var ip = System.Web.HttpContext.Current != null ? System.Web.HttpContext.Current.Request.UserHostAddress : "";
                //// ----- local test ----- //
                //if (ip == "::1")
                //    ip = "116.73.199.88";
                //string link_ip = "http://freegeoip.net/?q=" + ip;

                if (data != null)
                {
                    string mailTo = data["Email"];
                    string mailContent = data["mailContent"];
                    var cms_data = BLL.Queries.CMSManage.GetByPageName("BROADCAST_MAIL");
                    bool res_mail = BLL.Tools.CommonFunction.sendMail(cms_data.Title, mailContent, mailTo, "");
                    if (res_mail == true)
                    {
                        results.success = true;
                        results.message = "Mail Send Successfully.";
                    }
                    else
                    {
                        results.success = false;
                        results.message = "Send Mail Fail.";
                    }
                }
                else
                {
                    results.success = false;
                    results.message = "Mail not send Successfully.";
                    return Json(results, JsonRequestBehavior.AllowGet);
                }
                
            }
            catch (Exception ex)
            {
                Guid Errorguid = CommonFunction.LogManager(Request.FilePath, ex, "Admin/BroadCastMailController/Post()");
                //return RedirectToAction("Index", "LogManager", new { guID = gid });
                results.success = false;
                results.message = "Error Sending Broadcast Mail";
            }
            return Json(results, JsonRequestBehavior.AllowGet);
        }
    }
}