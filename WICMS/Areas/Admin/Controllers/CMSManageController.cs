﻿using BLL.ModelViews;
using BLL.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WiCms.Areas.Admin.Controllers
{
    [Authorize]
    //[RedirectingAction]
    public class CMSManageController : AdminBaseController
    {
        // GET: Admin/CMSManage

        public ActionResult Index()
        {
            try
            {
                ViewBag.PageName = BLL.Tools.Constants.PageName.CMSManage;
                var data = BLL.Queries.CMSManage.GetAll();
                return View(data);
            }
            catch (Exception ex)
            {
                Guid guid = CommonFunction.LogManager(Request.FilePath, ex, "Admin/CMSManageController/Index()");
                //return RedirectToAction("Index", "LogManager", new { guID = gid });
                return Redirect("/CMSManage/Index/");
            }
        }

        public ActionResult Create()
        {
            try
            {
                ViewBag.PageName = BLL.Tools.Constants.PageName.CMSManage;
                var data = new BLL.ModelViews.CMSManage.Extend();
                return View("Details", data);
            }
            catch (Exception ex)
            {
                Guid guid = CommonFunction.LogManager(Request.FilePath, ex, "Admin/CMSManageController/Create()");
                //return RedirectToAction("Index", "LogManager", new { guID = gid });
                return Redirect("/CMSManage/Index/");
            }
        }

        public ActionResult Details(string guid)
        {
            try
            {
                ViewBag.PageName = BLL.Tools.Constants.PageName.CMSManage;
                Guid id;
                bool res = Guid.TryParse(guid, out id);
                if (res == false)
                {
                    BLL.ModelViews.URLResponse response = new BLL.ModelViews.URLResponse()
                    {
                        Title = "CMS Manage",
                        Message = "Invalid Request...",
                        Page_Link = Request.UrlReferrer + "",//System.Web.HttpContext.Current.Request.UrlReferrer
                        Link_Text = "GO BACK"
                    };
                    TempData["mydata"] = response;
                    URLResponse error_data = TempData["mydata"] as URLResponse;
                    return View("PageError", error_data);
                }
                //Get Data From Passing GUID
                var data = BLL.Queries.CMSManage.GetByGuid(id);
                if (data == null)
                {
                    URLResponse response = new URLResponse()
                    {
                        Title = "CMS Manage",
                        Message = "Invalid Request...",
                        Page_Link = Request.UrlReferrer + "",
                        Link_Text = "GO BACK"
                    };
                    TempData["mydata"] = response;
                    URLResponse error_data = TempData["mydata"] as URLResponse;
                    //return RedirectToAction("Index", "ErrorHandle");
                    return View("PageError", error_data);
                }
                return View(data);
            }
            catch (Exception ex)
            {
                Guid Errorguid = CommonFunction.LogManager(Request.FilePath, ex, "Admin/CMSManageController/Details()");
                //return RedirectToAction("Index", "LogManager", new { guID = gid });
                return Redirect("/CMSManage/Index/");
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Post(BLL.ModelViews.CMSManage.Extend collection)
        {
            var results = new JsonMessageView();

            try
            {
                var error = BLL.Validations.CMSManage.Validate(collection);
                if (error.Count > 0)
                {
                    results.success = false;
                    results.message = "Validation Failed";
                    results.validation = error;
                    //return JsonConvert.SerializeObject(results);
                    return Json(results, JsonRequestBehavior.AllowGet);
                }

                if (collection.Guid == Guid.Empty)
                {
                     
                    collection.PageName = collection.PageName.ToUpper().Replace(" ", "_");
                    var data = BLL.Commands.CMSManage.Create(collection);
                    if (data.Guid != Guid.Empty)
                    {
                        results.success = true;
                        results.message = "Success creating " + collection.Title ;
                    }
                    else
                    {
                        results.success = false;
                        results.message = "There was an error creating " + collection.Title;
                    }
                       
                }
                else
                {
                    // Update record
                    var data = BLL.Commands.CMSManage.Update(collection);
                    
                    if (data.Guid != Guid.Empty)
                    {
                        results.success = true;
                        results.message = "Success updating " + collection.Title;
                    }
                    else
                    {
                        results.success = false;
                        results.message = "There was an error updating " + collection.Title;
                    }
                }
            }
            catch (Exception ex)
            {
                Guid guid = CommonFunction.LogManager(Request.FilePath, ex, "Admin/CMSManageController/Post()");
                results.success = false;
                results.message = "Error occuring when added & updated CMS data.";
            }

            // return RedirectToAction("Index");
            return Json(results, JsonRequestBehavior.AllowGet);
        }
    }
}