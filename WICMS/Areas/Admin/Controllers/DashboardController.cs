﻿using BLL.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace WiCms.Areas.Admin.Controllers
{
   // [RedirectingAction]
   [Authorize]
    public class DashboardController : AdminBaseController
    {
        public ActionResult Index()
        {
            try
            {
                //var data = new BLL.ModelViews.TblLoginMaster.Extend();
                ViewBag.PageName = BLL.Tools.Constants.PageName.Dashboard;
                //return View(data);
                try
                {
                    if (!WebSecurity.Initialized)
                    {
                        WebSecurity.InitializeDatabaseConnection("DefaultConnection", "User", "Id", "UserName", true);
                    }
                    if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
                       // return Redirect("/Admin/Dashboard");
                    return View();
                    else
                        return Redirect("/Admin/Login");
                }
                catch (Exception ex)
                {
                    Guid guid = CommonFunction.LogManager(Request.FilePath, ex, "Admin/LoginController/Index()");
                    return Redirect("Admin/Login");
                }
                //return View();
            }
            catch (Exception ex)
            {
                Guid guid = CommonFunction.LogManager(Request.FilePath, ex, "Admin/DashboardController/Index()");
                //return RedirectToAction("Index", "LogManager", new { guID = gid });
                return Redirect("/Dashboard/Index/" + guid);
            }
        }

    }
}