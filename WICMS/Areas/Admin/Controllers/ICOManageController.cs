﻿using BLL.ModelViews;
using BLL.Tools;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using HtmlAgilityPack;
using System.Reflection;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;

namespace WiCms.Areas.Admin.Controllers
{
    //[RedirectingAction]
    [Authorize]
    public class ICOManageController : AdminBaseController
    {
       
        public ViewResult Index()
        {
            //var IcoData = BLL.Queries.ICO.GetAll();
            //return View(IcoData);
            ViewBag.PageName = BLL.Tools.Constants.PageName.ICO;
            return View();
        }

        public JsonResult BindIcoData()
        {
            string msg = string.Empty;
            string result = string.Empty;

           // var IcoData = BLL.Queries.ICO.GetAll();
            var resultdata = BLL.Queries.ICO.GetAll().Select(s => new { s.ID, s.Guid, s.CoinIcon, s.CoinCode, s.CoinName, s.Website, s.ReferrerUrl, s.Rate, s.Stage, s.Price, s.IsRecommend });
            if (resultdata == null)
            {
                result = "Error:" + msg;
            }
            else
            {
                result = msg;
            }
            return Json(new { data = resultdata }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            try
            {
                ViewBag.PageName = BLL.Tools.Constants.PageName.ICO;
                var data = new BLL.ModelViews.ICO.Extend();
                ViewBag.CategoryList = BLL.Queries.Category.GetAllByGroup(BLL.Tools.Constants.Types.Group.ICO);
                return View("Details", data);
            }
            catch (Exception ex)
            {
                Guid guid = CommonFunction.LogManager(Request.FilePath, ex, "Admin/ICOManageController/Create()");
                //return RedirectToAction("Index", "LogManager", new { guID = gid });
                return Redirect("/ICOManage/Index/");
            }
        }

        public ActionResult Details(string guid)
        {
            try
            {
                ViewBag.PageName = BLL.Tools.Constants.PageName.ICO;
                Guid id;
                bool res = Guid.TryParse(guid, out id);
                if (res == false)
                {
                    BLL.ModelViews.URLResponse response = new BLL.ModelViews.URLResponse()
                    {
                        Title = "CMS Manage",
                        Message = "Invalid Request...",
                        Page_Link = Request.UrlReferrer + "",//System.Web.HttpContext.Current.Request.UrlReferrer
                        Link_Text = "GO BACK"
                    };
                    TempData["mydata"] = response;
                    URLResponse error_data = TempData["mydata"] as URLResponse;
                    return View("PageError", error_data);
                }
                //Get Data From Passing GUID
                var data = BLL.Queries.ICO.GetByGuid(id);
                if (data == null)
                {
                    URLResponse response = new URLResponse()
                    {
                        Title = "CMS Manage",
                        Message = "Invalid Request...",
                        Page_Link = Request.UrlReferrer + "",
                        Link_Text = "GO BACK"
                    };
                    TempData["mydata"] = response;
                    URLResponse error_data = TempData["mydata"] as URLResponse;
                    //return RedirectToAction("Index", "ErrorHandle");
                    return View("PageError", error_data);
                }
                ViewBag.CategoryList = BLL.Queries.Category.GetAllByGroup(BLL.Tools.Constants.Types.Group.ICO);

                return View(data);
            }
            catch (Exception ex)
            {
                Guid Errorguid = CommonFunction.LogManager(Request.FilePath, ex, "Admin/ICOManageController/Details()");
                //return RedirectToAction("Index", "LogManager", new { guID = gid });
                return Redirect("/ICOManage/Index/");
            }
        }

        //[HttpPost, ValidateInput(true, Exclude = "YourFieldName")]
        [HttpPost, ValidateInput(false)]
        public ActionResult Post(BLL.ModelViews.ICO.Extend collection)
        {
            //Request.Form["CategoryIDs"]
            var results = new JsonMessageView();
            try
            {
                var error = BLL.Validations.ICO.Validate(collection);
                if (error.Count > 0)
                {
                    results.success = false;
                    results.message = "Validation Failed";
                    results.validation = error;
                    return Json(results, JsonRequestBehavior.AllowGet);
                }

                if (collection.Guid == Guid.Empty)
                {
                    // Create record
                    //collection.NextRound = Convert.ToDateTime(collection.NextRound);
                    var data = BLL.Commands.ICO.Create(collection);
                    
                    if (data.ID != 0)
                    {
                        results.success = true;
                        //results.message = "Success updating " + collection.CoinName;
                        results.message = "Inserted Successfully.";
                    }
                    else
                    {
                        results.success = false;
                        results.message = "Not Inserted Successfully.";
                        //results.message = "There was an error updating " + collection.CoinName;
                    }

                }
                else
                {
                    // Update record
                    var data = BLL.Commands.ICO.Update(collection);
                   
                    if (data != null)
                    {
                        results.success = true;
                        results.message = "Success updating " + collection.CoinName;
                    }
                    else
                    {
                        results.success = false;
                        results.message = "There was an error updating " + collection.CoinName;
                    }
                }
            }
            catch (Exception ex)
            {
                Guid guid = CommonFunction.LogManager(Request.FilePath, ex, "Admin/ICOManageController/Post()");
                //return RedirectToAction("Index", "LogManager", new { guID = gid });
                //return Redirect("/LogManager/Index/" + guid);
                results.success = false;
                results.message = "Error occure while creating ICOManage";
            }

            return Json(results, JsonRequestBehavior.AllowGet);
        }

        /*    [HttpPost]
            public string Post(BLL.ModelViews.ICO.Extend collection)
            {
                var results = new JsonMessageView();

                try
                {
                    Boolean l_blFound = false;

                    //Code for validating the CAPTCHA
                    if (this.IsCaptchaValid("Captcha is not valid"))
                    {
                        var data = BLL.Queries.ICO.GetByICO_Password(collection.ICOname, collection.Password);
                        if (data != null)
                        {
                            BLL.Tools.SessionConfig.adminICOModel = data.ICOname;
                            BLL.Tools.SessionConfig.AdminID = data.ID;
                            BLL.Tools.SessionConfig.AdminLanguageID = BLL.Queries.TblLanguageMaster.GetFirstActiveLanguage();
                            l_blFound = true;
                        }

                        if (l_blFound)
                        {
                            results.success = true;
                            results.message = "Success Login ";
                        }
                        else
                        {
                            results.success = false;
                            results.message = "Fail to Login ";
                        }
                    }
                    else
                    {
                        results.success = false;
                        results.message = "Invalid captcha";
                        CaptchaUtils.CaptchaManager.StorageProvider = new CookieStorageProvider();
                    }
                    return JsonConvert.SerializeObject(results);
                }
                catch (Exception ex)
                {
                    Guid guid = CommonFunction.LogManager(Request.FilePath, ex, "Admin/ICOController/Post()");
                    //return RedirectToAction("Index", "LogManager", new { guID = gid });
                    results.success = false;
                    results.message = "Login Failed";
                    return JsonConvert.SerializeObject(results);
                }
            }*/

        public ActionResult Logout()
        {
            try
            {
                return Redirect("/Account/Logout");
            }
            catch (Exception ex)
            {
                Guid guid = CommonFunction.LogManager(Request.FilePath, ex, "Admin/ICOManageController/Logout()");
                //return RedirectToAction("Index", "LogManager", new { guID = gid });
               
                return Redirect("/ICOManage/Index/");
            }
        }

        [HttpPost]
        public JsonResult ChangeRecommand(Guid guid, bool isRecommand)
        {
            var results = new JsonMessageView();
            try
            {
                //Set Isdelete As True For Current Record.
                var isUpdated = BLL.Commands.ICO.UpdateRecommand(guid, isRecommand);
                if (isUpdated)
                {
                    results.success = true;
                    results.message = "Changed Recommand State";
                }
                else
                {
                    results.success = false;
                    results.message = "Fail to Change Recommand ";
                }
            }
            catch (Exception ex)
            {
                Guid Errorguid = CommonFunction.LogManager(Request.FilePath, ex, "Admin/ICOController/Delete()");
                //return RedirectToAction("Index", "LogManager", new { guID = gid });
                results.success = false;
                results.message = "Error Deleting Data";
            }
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdatePrice()
        {
            var results = new JsonMessageView();
            try
            {              
                List<BLL.ModelViews.ICO.Extend> listICO = new List<BLL.ModelViews.ICO.Extend>();
                var l_ICO = BLL.Queries.ICO.GetFiveMUpdated();

                    HtmlWeb web = new HtmlWeb();
                    var htmlDoc = web.Load(BLL.Tools.Constants.API.IcoReview);

                    HtmlNode[] table1_nodes = htmlDoc.DocumentNode.SelectNodes("//table[@id='tablepress-4']/tbody/tr").ToArray();
                    foreach (HtmlNode item1 in table1_nodes)
                    {
                        var name = item1.SelectSingleNode("td[@class='column-1']/a[1]").InnerText;
                        double price = 0, rate = 0;
                        var temp_price = item1.SelectSingleNode("td[@class='column-9']").InnerText;
                        var temp_rate = item1.SelectSingleNode("td[@class='column-2']").InnerText;

                        if (temp_rate != "PENDING" && temp_rate != "CLOSED" && temp_rate != "")
                        {
                            //string r = rate.ToString();
                            //string s_rate = r.Split('/');
                            temp_rate = temp_rate.Replace("/5", "");
                            rate = Convert.ToDouble(temp_rate);
                        }
                        if (!string.IsNullOrWhiteSpace(temp_price))
                        {
                            temp_price = temp_price.Replace("$", "");
                                int value = 0;
                                if (int.TryParse(temp_price, out value))
                                {
                                    // temp_price is an int
                                    //price = Convert.ToDouble("0"+value);
                                    price = Convert.ToDouble(value);
                                }
                                //else{ // temp_price is not an int }
                        }
                        if (l_ICO.Any(w => w.CoinName == name))
                        {
                            if (!listICO.Any(w => w.CoinName == name))
                            {
                                var old_data = l_ICO.FirstOrDefault(m => m.CoinName == name);
                                listICO.Add(new ICO.Extend() { CoinName = name, Price = price, MarketCap = old_data.MarketCap, CirculatingSupply = old_data.CirculatingSupply, MaxSupply = old_data.MaxSupply, Rate = rate });
                            }
                        }
                    }

                    HtmlNode[] table2_nodes = htmlDoc.DocumentNode.SelectNodes("//table[@id='tablepress-2']/tbody/tr").ToArray();
                    foreach (HtmlNode item2 in table2_nodes)
                    {
                        var name = item2.SelectSingleNode("td[@class='column-1']/a").InnerText;
                        double price = 0, rate = 0;
                        var temp_price = item2.SelectSingleNode("td[@class='column-9']").InnerText;

                        var temp_rate = item2.SelectSingleNode("td[@class='column-2']").InnerText;

                        if (temp_rate != "PENDING" && temp_rate != "CLOSED" && temp_rate != "")
                        {
                            temp_rate = temp_rate.Replace("/5", "");
                            rate = Convert.ToDouble(temp_rate);
                        }

                        if (!string.IsNullOrWhiteSpace(temp_price))
                        {
                            temp_price = temp_price.Replace("$", "");
                            int value = 0;
                            if (int.TryParse(temp_price, out value))
                            {
                                // temp_price is an int
                                //price = Convert.ToDouble("0"+value);
                                price = Convert.ToDouble(value);
                            }
                            //else{ // temp_price is not an int }
                    }
                    if (l_ICO.Any(w => w.CoinName == name))
                        {
                            if (!listICO.Any(w => w.CoinName == name))
                            {
                                var old_data = l_ICO.FirstOrDefault(m => m.CoinName == name);
                                listICO.Add(new ICO.Extend() { CoinName = name, Price = price, MarketCap = old_data.MarketCap, CirculatingSupply = old_data.CirculatingSupply, MaxSupply = old_data.MaxSupply, Rate = rate });
                            }
                        }
                    }

                    var response_CoinMarketCap = CommonFunction.GetStringFromURL(Constants.API.CoinMarketCap);
                    response_CoinMarketCap = response_CoinMarketCap.Replace("24h_volume_usd", "volume_usd_24h");
                    List<BLL.ModelViews.CoinMarketCap> result_CoinMarket = BLL.Tools.Converstions.JsonDeserialize<List<BLL.ModelViews.CoinMarketCap>>(response_CoinMarketCap);

                    //var l_ICO = BLL.Queries.ICO.GetFiveMUpdated();
                    var code_list = l_ICO.Select(c => c.CoinCode).ToList();
                    var res = result_CoinMarket.Where(w => code_list.Contains(w.symbol)).Count();
                    foreach (var item in l_ICO)
                    {
                        var d = result_CoinMarket.FirstOrDefault(w => w.name.ToLower() == item.CoinName.ToLower() || w.symbol == item.CoinCode);
                        if (d != null)
                        {
                            item.Price = Convert.ToDouble(d.price_usd);
                            item.MarketCap = Convert.ToDouble(d.market_cap_usd);
                            item.CirculatingSupply = Convert.ToDouble(d.available_supply);
                            item.MaxSupply = Convert.ToDouble(d.max_supply);
                            if (!listICO.Any(w => w.CoinName == item.CoinName))
                            {
                                listICO.Add(new ICO.Extend() { CoinName = item.CoinName, Price = item.Price, MarketCap = Convert.ToDouble(d.market_cap_usd), CirculatingSupply = item.CirculatingSupply, MaxSupply = item.MaxSupply, Rate = item.Rate });
                            }
                        }
                        else
                        {
                            var response_CryptoCompare_Symbol = CommonFunction.GetStringFromURL(String.Format(Constants.API.CryptoCompare_Symbol, item.CoinCode));
                            dynamic result_CC = Newtonsoft.Json.JsonConvert.DeserializeObject(response_CryptoCompare_Symbol, typeof(object));
                            //string response = result_CC["Response"];
                            if (result_CC["Response"] != "" && result_CC["Response"] == "Error")
                            {
                                //response_CryptoNator != "" && 
                                var response_CryptoNator = CommonFunction.GetStringFromURL(String.Format(Constants.API.CryptoNator, item.CoinCode));
                                if (!response_CryptoNator.StartsWith("<!DOCTYPE html>") && response_CryptoNator != "Too many automatic redirections were attempted." && response_CryptoNator != "Unable to connect to the remote server")
                                {
                                    var nator_res = BLL.Tools.Converstions.JsonDeserialize<dynamic>(response_CryptoNator);
                                    //string success_response = nator_res.success;
                                    //if (nator_res != "") 
                                    //{   Unable to connect to the remote server
                                    if (nator_res.success != "False")
                                    {
                                        item.Price = Convert.ToDouble(nator_res["ticker"].price);
                                        if (!listICO.Any(w => w.CoinName == item.CoinName))
                                        {
                                            listICO.Add(new ICO.Extend() { CoinName = item.CoinName, Price = item.Price, MarketCap = item.MarketCap, CirculatingSupply = item.CirculatingSupply, MaxSupply = item.MaxSupply, Rate = item.Rate });
                                        }
                                    }
                                    //}
                                }
                            }
                            else
                            {
                                item.Price = result_CC["RAW"][item.CoinCode]["USD"].PRICE;
                                item.CirculatingSupply = result_CC["RAW"][item.CoinCode]["USD"].SUPPLY;
                                item.MarketCap = result_CC["RAW"][item.CoinCode]["USD"].MKTCAP;
                                if (!listICO.Any(w => w.CoinName == item.CoinName))
                                {
                                    listICO.Add(new ICO.Extend() { CoinName = item.CoinName, Price = item.Price, MarketCap = item.MarketCap, CirculatingSupply = item.CirculatingSupply, MaxSupply = item.MaxSupply, Rate = item.Rate });
                                }
                            }
                        }
                    }
                BLL.Commands.ICO.UpdatePrice(listICO);
                //results.message = data;      
                results.success = true;
                results.message = "Success Updating Price";
            }
            catch (Exception ex)
            {
                Guid Errorguid = CommonFunction.LogManager(Request.FilePath, ex, "Admin/ICOManageController/UpdatePrice()");
                //return RedirectToAction("Index", "LogManager", new { guID = gid });
                results.success = false;
                results.message = "Error Updating Price.";
            }           
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult BackupICOHistory(DateTime selDate)
        {
            var results = new JsonMessageView();
            try
            {
               var isTrue = BLL.Tools.CommonFunction.CallProcedure(selDate); //DateTime.UtcNow
                if(isTrue == true)
                {
                    results.success = true;
                    results.message = "Successfully BackUp ICOHistory Data of " + selDate + "1 Years Ago.";
                }               
            }
            catch(Exception ex)
            {
                Guid Errorguid = CommonFunction.LogManager(Request.FilePath, ex, "Admin/ICOManageController/BackupICOHistory()");
                results.success = false;
                results.message = "Error BackUp ICOHistory.";
            }
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public string Delete(Guid guid)
        {
            var results = new JsonMessageView();
            try
            {
                //Set Isdelete As True For Current Record.
                var isDeleted = BLL.Commands.ICO.Delete(guid);
                if (isDeleted)
                {
                    results.success = true;
                    results.message = "'Delete";
                }
            }
            catch (Exception ex)
            {
                Guid Errorguid = CommonFunction.LogManager(Request.FilePath, ex, "Admin/ICOController/Delete()");
                //return RedirectToAction("Index", "LogManager", new { guID = gid });
                results.success = false;
                results.message = "Error Deleting Data";
            }

            return JsonConvert.SerializeObject(results);
        }
    }
}