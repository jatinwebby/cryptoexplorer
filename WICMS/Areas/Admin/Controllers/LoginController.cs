﻿using BLL.ModelViews;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using BLL.Tools;
using WebMatrix.WebData;
using BLL.Validations;
using System.Net;
using System.Web.Security;
using BLL.ModelViews;

namespace WiCms.Areas.Admin.Controllers
{
    public class LoginController : AdminBaseController
    {
        // GET: Admin/Login
        public ActionResult Index()
        {
            try
            {
                if (!WebSecurity.Initialized)
                {
                    WebSecurity.InitializeDatabaseConnection("DefaultConnection", "User", "Id", "UserName", true);
                }
                if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
                    return Redirect("/Admin/Dashboard");
                else
                   //return Redirect("/Admin/Login");
                return View();
            }
            catch (Exception ex)
            {
                Guid guid = CommonFunction.LogManager(Request.FilePath, ex, "Admin/LoginController/Index()");
                return Redirect("/Login/Index/");
            }
        }

        //FormCollection collection   BLL.ModelViews.User.Extend collection
        [HttpPost]
        public JsonResult Post(FormCollection collection)
        {
                var results = new JsonMessageView();
               
                try
                {
                        var captchaResponse = BLL.Tools.CommonFunction.ValidateCaptcha(Request["g-recaptcha-response"]);
                        //when response is false check for the error message
                        if (captchaResponse.Success)
                        {
                            //  var query = BLL.Queries.TblLoginMaster.GetAll();collection["UserName"], collection["Password"]collection.UserName, collection.Password
                            var l_success = WebSecurity.Login(collection["UserName"], collection["Password"]);

                                        if (l_success)
                                        {
                                            results.success = true;
                                            results.message = "Success Login.";
                                        }
                                        else
                                        {
                                            results.success = false;
                                            results.message = "Fail to Login,Please enter correct USERNAME & PASSWORD.";                                            
                                        }
                        }
                        else
                        {
                            var error = captchaResponse.ErrorCodes[0].ToLower();
                            switch (error)
                            {
                                case ("missing-input-secret"):
                                    results.success = false;
                                    results.message = "The secret parameter is missing.";
                                    break;
                                case ("invalid-input-secret"):
                                    results.success = false;
                                    results.message = "The secret parameter is invalid or malformed.";
                                    break;
                                case ("missing-input-response"):
                                    results.success = false;
                                    results.message = "The response parameter is missing.";
                                    break;
                                case ("invalid-input-response"):
                                    results.success = false;
                                    results.message = "The response parameter is invalid or malformed.";
                                    break;
                                default:
                                    results.success = false;
                                    results.message = "Error occured. Please try again";
                                    break;
                            }
                            return Json(results, JsonRequestBehavior.AllowGet);
                        }
                    }
                catch (Exception ex)
                {
                    Guid guid = CommonFunction.LogManager(Request.FilePath, ex, "Admin/LoginController/Post()");
                    //return RedirectToAction("Index", "LogManager", new { guID = gid });
                    results.success = false;
                    results.message = "Login Failed";
                }

            return Json(results, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Logout()
        {
            try
            {
                WebSecurity.Logout();
                return Redirect("/Admin/Login");
            }
            catch (Exception ex)
            {
                Guid guid = CommonFunction.LogManager(Request.FilePath, ex, "Admin/LoginController/Logout()");
                //return RedirectToAction("Index", "LogManager", new { guID = gid });
                return Redirect("/Login/Index/");
            }
        }

        public ActionResult ChangePassword()
        {
            try
            {
                if (!System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
                    return RedirectToAction("Index", "Login");
                else
                {     
                    var data = BLL.Queries.User.GetByUsername(System.Web.HttpContext.Current.User.Identity.Name);
                    return View(data);
                }
            }
            catch (Exception ex)
            {
                Guid guid = CommonFunction.LogManager(Request.FilePath, ex, "Admin/LoginController/ChangePassword()");
                //return RedirectToAction("Index", "LogManager", new { guID = gid });
                return Redirect("/Login/Index/");
            }
        }

        [HttpPost]
        public string ChangePasswordPost(FormCollection collection)
        {

            var results = new JsonMessageView();
            try
            {
                MembershipUser check = Membership.GetUser(System.Web.HttpContext.Current.User.Identity.Name, false);

                if (WebSecurity.IsAuthenticated)
                {
                    //var query = BLL.Queries.User.GetByUsername(System.Web.HttpContext.Current.User.Identity.Name);
                    //WebSecurity.ChangePassword(collection["UserName"], collection["Password"], collection["NewPassword"]);
                    if (WebSecurity.ChangePassword(collection["UserName"], collection["Password"], collection["NewPassword"]))
                    {
                        //@BLL.Tools.CommonFunction.Decrypt(Model.UserName)
                        //var data = BLL.Commands.User.PasswordUpdate(collection);
                        //if (data != null)
                        //{
                        results.success = true;
                        results.message = "Password Changed successfully";
                        Session.Abandon();
                        //}
                    }
                    else
                    {
                        results.success = false;
                        results.message = "There was an error updating Password ";

                    }
                }
            }
            catch (Exception ex)
            {
                Guid guid = CommonFunction.LogManager(Request.FilePath, ex, "Admin/LoginController/ChangePasswordPost()");
                //return RedirectToAction("Index", "LogManager", new { guID = gid });
                results.success = false;
                results.message = "Password not changed.";
            }
            return JsonConvert.SerializeObject(results);    
        }

        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        public JsonResult ForgotPassword(BLL.ModelViews.User.Extend collection)
        {
            var results = new JsonMessageView();
            try { 
           
                    var error = BLL.Validations.ForgotPasswordValidator.Validate(collection);
                    if (error.Count > 0)
                    {
                        results.success = false;
                        results.message = "Validation Failed";
                        results.validation = error;
                        return Json(results, JsonRequestBehavior.AllowGet);
                    }
                    if (WebSecurity.UserExists(collection.UserName))
                    {
                            string To = collection.UserName; //UserID, Password, SMTPPort, Host;
                            string token = WebSecurity.GeneratePasswordResetToken(collection.UserName);
                            if (token == null)
                            {
                                    // If user does not exist or is not confirmed.  return View("Index");
                                    results.success = false;
                                    results.message = collection.UserName + " User not exist.";
                            }
                            else
                            {
                                var data=BLL.Queries.User.GetByUsername(collection.UserName);

                                //Create URL with above token  //email = collection.Email,
                                var lnkHref = "<a href='" + Url.Action("ResetPassword", "Login", new { user=collection.UserName, code = token }, "http") + "'>Reset Password</a>";
                               
                                //HTML Template for Send email  
                                string subject = "Your changed password";
                                string body = "<b>Please find the Password Reset Link. </b><br/>" + lnkHref;
                               
                                ////Call send email methods.  
                                bool checkmail=CommonFunction.sendMail(subject, body, data.Email, "");
                                if (checkmail == true)
                                {
                                        results.success = true;
                                        results.message = "Reset Password Mail Send Successfully.";
                                }
                           }
                   }
                   else
                   {
                    var data=BLL.Queries.User.GetUserByEMail(collection.UserName);
                    string token = WebSecurity.GeneratePasswordResetToken(data.UserName);
                    if (token == null)
                    {
                        // If user does not exist or is not confirmed.  return View("Index");
                        results.success = false;
                        results.message = collection.UserName + " User not exist.";
                    }
                    else
                    {
                        //var data1 = BLL.Queries.User.GetByUsername(collection.UserName);
                        //Create URL with above token  //email = collection.Email,
                        var lnkHref = "<a href='" + Url.Action("ResetPassword", "Login", new { user = data.UserName, code = token }, "http") + "'>Reset Password</a>";

                        //HTML Template for Send email  
                        string subject = "Your changed password";
                        string body = "<b>Please find the Password Reset Link. </b><br/>" + lnkHref;

                        ////Call send email methods.  
                        bool checkmail = CommonFunction.sendMail(subject, body, collection.UserName, "");
                        if (checkmail == true)
                        {
                            results.success = true;
                            results.message = "Reset Password Mail Send Successfully.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Guid guid = CommonFunction.LogManager(Request.FilePath, ex, "Admin/LoginController/ForgotPassword()");
                //return RedirectToAction("Index", "LogManager", new { guID = gid });
                results.success = false;
                results.message = "Error occure in Forgot Password";
            }

            return Json(results, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ResetPassword(string user,string code)
        {
            BLL.ModelViews.ResetPassword model = new BLL.ModelViews.ResetPassword();
            model.ReturnToken = code;
            ViewBag.ReturnToken = code;
            ViewBag.UserName = user;
            //ViewBag.Email = email;
            return View(model);
        }
        [HttpPost]
        public JsonResult ResetPassword(BLL.ModelViews.ResetPassword model)
        {
            var results = new JsonMessageView();
            try
            {
                var error = BLL.Validations.ResetPassword.Validate(model);
                if (error.Count > 0)
                {
                    results.success = false;
                    results.message = "Validation Failed";
                    results.validation = error;
                    return Json(results, JsonRequestBehavior.AllowGet);
                }
                //var token=ViewBag.ReturnToken;
                //var uname=ViewBag.UserName;
                //var email=ViewBag.Email;
                bool resetResponse = WebSecurity.ResetPassword(model.ReturnToken, model.Password);
                if (resetResponse)
                {
                    //ViewBag.Message = "Successfully Changed";
                    results.success = true;
                    results.message = "Successfully Changed";
                }
                else
                {
                    //ViewBag.Message = "Something went horribly wrong!";
                    results.success = true;
                    results.message = "Something went horribly wrong!";
                }
            }        
            catch (Exception ex)
            {
                Guid guid = CommonFunction.LogManager(Request.FilePath, ex, "Admin/LoginController/ResetPassword()");
                //return RedirectToAction("Index", "LogManager", new { guID = gid });
                results.success = false;
                results.message = "Error occure in Reset Password";
            }
            return Json(results, JsonRequestBehavior.AllowGet);
       }

    }
}