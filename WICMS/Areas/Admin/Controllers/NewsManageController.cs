﻿using BLL.ModelViews;
using BLL.Tools;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WiCms.Areas.Admin.Controllers
{
    [Authorize]
    public class NewsManageController : AdminBaseController
    {
        // GET: Admin/NewsManage
        public ActionResult Index()
        {
            ViewBag.PageName = BLL.Tools.Constants.PageName.NewsManage;
            var data = BLL.Queries.NewsManage.GetAll();
            return View(data);
        }

        public ActionResult Create()
        {
            try
            {
                ViewBag.PageName = BLL.Tools.Constants.PageName.NewsManage;
                var data = new BLL.ModelViews.NewsManage.Extend();
                return View("Details", data);
            }
            catch (Exception ex)
            {
                Guid guid = BLL.Tools.CommonFunction.LogManager(Request.FilePath, ex, "Admin/NewsManageController/Create()");
                //return RedirectToAction("Index", "LogManager", new { guID = gid });
                return Redirect("/NewsManage/Index/");
            }
        }

        public ActionResult Details(string guid)
        {
            try
            {
                ViewBag.PageName = BLL.Tools.Constants.PageName.NewsManage;
                Guid id;
                bool res = Guid.TryParse(guid, out id);
                if (res == false)
                {
                    BLL.ModelViews.URLResponse response = new BLL.ModelViews.URLResponse()
                    {
                        Title = "News Manage",
                        Message = "Invalid Request...",
                        Page_Link = Request.UrlReferrer + "",//System.Web.HttpContext.Current.Request.UrlReferrer
                        Link_Text = "GO BACK"
                    };
                    TempData["mydata"] = response;
                    URLResponse error_data = TempData["mydata"] as URLResponse;
                    return View("PageError", error_data);
                }
                //Get Data From Passing GUID
                var data = BLL.Queries.NewsManage.GetByGuid(id); //guid
                if (data == null)
                {
                    URLResponse response = new URLResponse()
                    {
                        Title = "News Manage",
                        Message = "Invalid Request...",
                        Page_Link = Request.UrlReferrer + "",
                        Link_Text = "GO BACK"
                    };
                    TempData["mydata"] = response;
                    URLResponse error_data = TempData["mydata"] as URLResponse;
                    //return RedirectToAction("Index", "ErrorHandle");
                    return View("PageError", error_data);
                }

                return View(data);
            }
            catch (Exception ex)
            {
                Guid Errorguid = CommonFunction.LogManager(Request.FilePath, ex, "Admin/NewsManageController/Details()");
               
                return Redirect("/NewsManage/Index/");
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Post(BLL.ModelViews.NewsManage.Extend collection)
        {
            var results = new BLL.ModelViews.JsonMessageView();

            try
            {
                var error = BLL.Validations.NewsManage.Validate(collection);
                if (error.Count > 0)
                {
                    results.success = false;
                    results.message = "Validation Failed";
                    results.validation = error;
                    //return JsonConvert.SerializeObject(results);
                    return Json(results, JsonRequestBehavior.AllowGet);
                }

                if (collection.Guid == Guid.Empty)
                {

                    
                    var data = BLL.Commands.NewsManage.Create(collection);
                    if (data.Guid != Guid.Empty)
                    {
                        results.success = true;
                        results.message = "Success creating " + collection.Title;
                    }
                    else
                    {
                        results.success = false;
                        results.message = "There was an error creating " + collection.Title;
                    }

                }
                else
                {
                    // Update record
                    var data = BLL.Commands.NewsManage.Update(collection);

                    if (data.Guid != Guid.Empty)
                    {
                        results.success = true;
                        results.message = "Success updating " + collection.Title;
                    }
                    else
                    {
                        results.success = false;
                        results.message = "There was an error updating " + collection.Title;
                    }
                }
            }
            catch (Exception ex)
            {
                Guid guid = CommonFunction.LogManager(Request.FilePath, ex, "Admin/NewsManageController/Post()");
                results.success = false;
                results.message = "Error occuring when added & updated News data.";
            }

            // return RedirectToAction("Index");
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public string Delete(Guid guid)
        {
            var results = new JsonMessageView();
            try
            {
                //Set Isdelete As True For Current Record.
                var isDeleted = BLL.Commands.NewsManage.Delete(guid);
                if (isDeleted)
                {
                    results.success = true;
                    results.message = "'Delete";
                }
            }
            catch (Exception ex)
            {
                Guid Errorguid = CommonFunction.LogManager(Request.FilePath, ex, "Admin/ICOController/Delete()");
                //return RedirectToAction("Index", "LogManager", new { guID = gid });
                results.success = false;
                results.message = "Error Deleting Data";
            }

            return JsonConvert.SerializeObject(results);
        }
    }
}