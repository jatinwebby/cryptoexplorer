
using System;
using System.Web.Mvc;
using BLL.ModelViews;
using Newtonsoft.Json;

namespace WiCms.Areas.Admin.Controllers
{
    [Authorize]
    public class SubscriberManageController : AdminBaseController
    {

        public ActionResult Index()
        {
            ViewBag.PageName = BLL.Tools.Constants.PageName.Subscriber;
            var data = BLL.Queries.Subscriber.GetAll();
            return View(data);
        }

        public ActionResult Details(string guid)
        {
            Guid id;
            bool res = Guid.TryParse(guid, out id);
            if (res == false)
            {
                BLL.ModelViews.URLResponse response = new BLL.ModelViews.URLResponse()
                {
                    Title = "CMS Manage",
                    Message = "Invalid Request...",
                    Page_Link = Request.UrlReferrer + "",
                    Link_Text = "GO BACK"
                };
                TempData["mydata"] = response;
                URLResponse error_data = TempData["mydata"] as URLResponse;
                return View("PageError", error_data);
            }
            var data = BLL.Queries.Subscriber.GetByGuid(id);
            if (data == null)
            {
                URLResponse response = new URLResponse()
                {
                    Title = "CMS Manage",
                    Message = "Invalid Request...",
                    Page_Link = Request.UrlReferrer + "",
                    Link_Text = "GO BACK"
                };
                TempData["mydata"] = response;
                URLResponse error_data = TempData["mydata"] as URLResponse;
                return View("PageError", error_data);
            }
            return View(data);
        }

        public ActionResult Create()
        {
            var data = new BLL.ModelViews.Subscriber.Extend();
            return View("Details", data);
        }

        [HttpPost]
        public string Post(BLL.ModelViews.Subscriber.Extend collection)
        {
            var results = new JsonMessageView();

            var error = BLL.Validations.Subscriber.Validate(collection);
            if (error.Count > 0)
            {
                results.success = false;
                results.message = "Validation Failed";
                results.validation = error;
                return JsonConvert.SerializeObject(results);
            }


            if (collection.Guid == Guid.Empty)
            {
                // Create record
                var data = BLL.Commands.Subscriber.Create(collection);
                if (data.Guid  != Guid.Empty)
                {
                    results.success = true;
                    results.message = "Success creating "  ;
                }
                else
                {
                    results.success = false;
                    results.message = "There was an error creating "  ;
                }

            }
            else
            {
                // Update record
                var data = BLL.Commands.Subscriber.Update(collection);
                if (data != null)
                {
                    results.success = true;
                    results.message = "Success updating " ;
                }
                else
                {
                    results.success = false;
                    results.message = "There was an error updating "  ;
                }
            }


            return JsonConvert.SerializeObject(results);
        }

         //[HttpPost]
        //public string Create(BLL.ModelViews.Subscriber.Extend collection)
        //{
            //var results = new JsonMessageView();
            //
            //return JsonConvert.SerializeObject(results);
        //}
        //
        //[HttpPost]
        //public string Edit(int id, BLL.ModelViews.Client.Extend collection)
        //{
            //var results = new JsonMessageView();
            //
            //return JsonConvert.SerializeObject(results);
        //}

        [HttpPost]
        public string Delete(Guid guid)
        {
            var results = new BLL.ModelViews.JsonMessageView();

            var isDeleted = BLL.Commands.Subscriber.Delete(guid);
            if (isDeleted)
            {
                results.success = true;
                results.message = "'Delete";
            }

            return JsonConvert.SerializeObject(results);
        }
    }
}
