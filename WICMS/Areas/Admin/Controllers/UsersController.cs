﻿using BLL.ModelViews;
using BLL.Tools;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;

namespace WiCms.Areas.Admin.Controllers
{

    [Authorize]
    //[RedirectingAction]
    public class UsersController : AdminBaseController
    {
        // GET: Admin/Users
        /* public ActionResult Login()
        {
            try
            {
                var data = new BLL.ModelViews.TblLoginMaster.Extend();

                if (BLL.Tools.SessionConfig.adminUserModel != null)
                    //return RedirectToAction("Index", "Dashboard");
                    return Redirect("/Admin/Dashboard");
                else
                    return View("Index", data);
            }
            catch (Exception ex)
            {
                Guid guid = CommonFunction.LogManager(Request.FilePath, ex, "Admin/LoginController/Index()");
                //return RedirectToAction("Index", "LogManager", new { guID = gid });
                return Redirect("/LogManager/Index/" + guid);
            }
        }*/

        public ActionResult Index()
        {
            ViewBag.PageName = BLL.Tools.Constants.PageName.Users;
            var data = BLL.Queries.User.GetAll();
            return View(data);
        }

        public ActionResult Create()
        {
            try
            {
                ViewBag.PageName = BLL.Tools.Constants.PageName.Users;
                var data = new BLL.ModelViews.User.Extend();
                return View("Details", data);
            }
            catch (Exception ex)
            {
                Guid guid = CommonFunction.LogManager(Request.FilePath, ex, "Admin/UsersController/Create()");
                //return RedirectToAction("Index", "LogManager", new { guID = gid });
                return Redirect("/LogManager/Index/" + guid);
            }
        }

        public ActionResult Details(string guid)
        {
            try
            {
                ViewBag.PageName = BLL.Tools.Constants.PageName.Users;
                Guid id;
                bool res = Guid.TryParse(guid, out id);
                if (res == false)
                {
                    BLL.ModelViews.URLResponse response = new BLL.ModelViews.URLResponse()
                    {
                        Title = "CMS Manage",
                        Message = "Invalid Request...",
                        Page_Link = Request.UrlReferrer + "",//System.Web.HttpContext.Current.Request.UrlReferrer
                        Link_Text = "GO BACK"
                    };
                    TempData["mydata"] = response;
                    URLResponse error_data = TempData["mydata"] as URLResponse;
                    return View("PageError", error_data);
                }
                //Get Data From Passing GUID
                var data = BLL.Queries.User.GetByGuid(id);
                if (data == null)
                {
                    URLResponse response = new URLResponse()
                    {
                        Title = "CMS Manage",
                        Message = "Invalid Request...",
                        Page_Link = Request.UrlReferrer + "",
                        Link_Text = "GO BACK"
                    };
                    TempData["mydata"] = response;
                    URLResponse error_data = TempData["mydata"] as URLResponse;
                    //return RedirectToAction("Index", "ErrorHandle");
                    return View("PageError", error_data);
                }
                //MembershipUser check = System.Web.Security.Membership.GetUser(System.Web.HttpContext.Current.User.Identity.Name, false);

                //if (check == null)
                //{
                //   data.Password = check.GetPassword();
                //}
                return View(data);
            }
            catch (Exception ex)
            {
                Guid Errorguid = CommonFunction.LogManager(Request.FilePath, ex, "Admin/UsersController/Details()");
                //return RedirectToAction("Index", "LogManager", new { guID = gid });
                return Redirect("/LogManager/Index/" + Errorguid);
            }
        }

        [HttpPost]
        public ActionResult Post(BLL.ModelViews.User.Extend collection)
        {
            var results = new JsonMessageView();

            try
            {
                var error = BLL.Validations.User.Validate(collection);
                if (error.Count > 0)
                {
                    results.success = false;
                    results.message = "Validation Failed";
                    results.validation = error;
                    return Json(results, JsonRequestBehavior.AllowGet);
                }

                if (collection.Guid == Guid.Empty)
                {
                    // Create record
                    // Insertion For First Default Selected Language 
                    //collection.Role = BLL.Tools.Constants.Role.ADMIN;
                    collection.CreatedDate = DateTime.UtcNow;
                    collection.UpdatedDate = DateTime.UtcNow;
                    collection.Guid = Guid.NewGuid();

                    //if(collection.Status == "on")
                    //{
                    //    collection.Status = "Active";
                    //}
                    //else
                    //{
                    //    collection.Status = "Deactive";
                    //}
                   
                    //var data = BLL.Commands.User.Create(collection);
                    // if (data != null)
                     var data=WebSecurity.CreateUserAndAccount(collection.UserName, collection.Password, new { Guid= collection.Guid, FirstName = collection.FirstName, LastName = collection.LastName, Email = collection.Email, PhoneNumber = collection.PhoneNumber,Address=collection.Address,City=collection.City,State=collection.State,PostCode=collection.PostCode,Country=collection.Country,Role= collection.Role,CreatedDate = collection.CreatedDate,UpdatedDate=collection.UpdatedDate,Status= collection.Status }, false);
                    if (data == null)
                    {
                        results.success = true;
                        //results.message = "Success updating " + collection.FirstName + " " + collection.LastName;
                        results.message = "Inserted Successfully.";
                    }
                    else
                    {
                        results.success = false;
                        //results.message = "There was an error updating " + collection.FirstName + " " + collection.LastName;
                        results.message = "Not Inserted Successfully.";
                    }

                }
                else
                {
                    // Update record
                   // collection.Role = BLL.Tools.Constants.Role.USER;
                    //if (collection.Status == null)
                    //{
                    //    collection.Status = "Deactive";
                    //}
                    //else
                    //{
                    //    collection.Status = "Active";
                    //}
                    var data = BLL.Commands.User.Update(collection);
                    if (data != null)
                    {
                        results.success = true;
                        //results.message = "Success updating " + collection.FirstName + " " + collection.LastName;
                        results.message = "Success updating ";
                    }
                    else
                    {
                        results.success = false;
                        //results.message = "There was an error updating " + collection.FirstName + " " + collection.LastName;
                        results.message = "There was an error updating ";
                    }
                }
            }
            catch (Exception ex)
            {
                Guid guid = CommonFunction.LogManager(Request.FilePath, ex, "Admin/UsersController/Post()");
                //return RedirectToAction("Index", "LogManager", new { guID = gid });
                //return Redirect("/LogManager/Index/" + guid);
                results.success = false;
                results.message = "Error occur while creating User.";
            }

            return Json(results, JsonRequestBehavior.AllowGet);
        }

    /*    [HttpPost]
        public string Post(BLL.ModelViews.User.Extend collection)
        {
            var results = new JsonMessageView();

            try
            {
                Boolean l_blFound = false;

                //Code for validating the CAPTCHA
                if (this.IsCaptchaValid("Captcha is not valid"))
                {
                    var data = BLL.Queries.User.GetByUser_Password(collection.Username, collection.Password);
                    if (data != null)
                    {
                        BLL.Tools.SessionConfig.adminUserModel = data.Username;
                        BLL.Tools.SessionConfig.AdminID = data.ID;
                        BLL.Tools.SessionConfig.AdminLanguageID = BLL.Queries.TblLanguageMaster.GetFirstActiveLanguage();
                        l_blFound = true;
                    }

                    if (l_blFound)
                    {
                        results.success = true;
                        results.message = "Success Login ";
                    }
                    else
                    {
                        results.success = false;
                        results.message = "Fail to Login ";
                    }
                }
                else
                {
                    results.success = false;
                    results.message = "Invalid captcha";
                    CaptchaUtils.CaptchaManager.StorageProvider = new CookieStorageProvider();
                }
                return JsonConvert.SerializeObject(results);
            }
            catch (Exception ex)
            {
                Guid guid = CommonFunction.LogManager(Request.FilePath, ex, "Admin/UserController/Post()");
                //return RedirectToAction("Index", "LogManager", new { guID = gid });
                results.success = false;
                results.message = "Login Failed";
                return JsonConvert.SerializeObject(results);
            }
        }*/

        public ActionResult Logout()
        {
            try
            {
                return Redirect("/Account/Logout");
            }
            catch (Exception ex)
            {
                Guid guid = CommonFunction.LogManager(Request.FilePath, ex, "Admin/UsersController/Logout()");
                //return RedirectToAction("Index", "LogManager", new { guID = gid });
                return Redirect("/Users/Index/");
            }
        }

        [HttpPost]
        public string Delete(Guid guid)
        {
            var results = new JsonMessageView();
            try
            {
                //Set Isdelete As True For Current Record.
                var isDeleted = BLL.Commands.User.Delete(guid);
                ((SimpleMembershipProvider)Membership.Provider).DeleteAccount("username");
                ((SimpleMembershipProvider)Membership.Provider).DeleteUser("username", true);
                if (isDeleted)
                {
                    results.success = true;
                    results.message = "'Delete";
                }
            }
            catch (Exception ex)
            {
                Guid Errorguid = CommonFunction.LogManager(Request.FilePath, ex, "Admin/UsersController/Delete()");
                //return RedirectToAction("Index", "LogManager", new { guID = gid });
                results.success = false;
                results.message = "Error Deleting Data";
            }

            return JsonConvert.SerializeObject(results);
        }
    }
}