﻿

$(document).ready(function () {

    var oTable = $('#myDataTable').dataTable({
        "bServerSide": true,
        "sAjaxSource": "/Member/AjaxHandler",
        "bProcessing": true,
        "aoColumns": [
                        {
                            "sName": "NCoachID",
                            "bSearchable": false,
                            "bSortable": false


                        },
                        //{ "sName": "" },

                        { "sName": "" },
			            { "sName": "StrMemberFirstName" },
			            { "sName": "StrMemberLastName" },
                        { "sName": "StrMemberPhoneNo" },
                        { "sName": "StrMemberAddress" },
                        { "sName": "BIsActiveMember" }

        ]
    });
});
