﻿
$(document).ready(function () {

    var oTable = $('#myDataTable').dataTable({
        "bServerSide": true,
        "sAjaxSource": "/Coach/AjaxHandler",
        "bProcessing": true,
        "aoColumns": [
                        {
                            "sName": "NCoachID",
                            "bSearchable": false,
                            "bSortable": false


                        },
                        //{ "sName": "" },
                        
                        { "sName": "" },
			            { "sName": "StrCoachFirstName" },
			            { "sName": "StrCoachLasttName" },
                        { "sName": "StrCoachPhoneNo" },
                        { "sName": "StrCoachAddress" },
                        { "sName": "BIsActiveCoach" }

        ]
    });
});
