﻿
$(document).ready(function () {

    var oTable = $('#myDataTable').dataTable({
    	"bServerSide": true,
    	"sAjaxSource": "/Player/AjaxHandler",
    	"bProcessing": true,
    	"aoColumns": [
                        {
                            "sName": "NPlayerID",
                            "bSearchable": false,
                            "bSortable": false

                            
                        },
                        //{ "sName": "" },
                        { "sName": "" },
                        { "sName": "" },
			            { "sName": "StrPlayerFirstName" },
			            { "sName": "StrPlayerAddress" },
                        { "sName": "StrPlayerPicture" },
                        { "sName": "Statistics" },
                        { "sName": "Gallery" },
                        { "sName": "Video" },
                        { "sName": "BIsRecruiterWatchList" },
                        { "sName": "BIsActivePlayer" }
                        //{ "sName": "Guid" }
		            ]
    });
});

