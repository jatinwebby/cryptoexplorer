﻿$(document).ready(function () {

    $.ajax({
        type: "POST",
        url: "/Categories/GetallSuggestion",
        datatype: "json",
        contenttype: "application/json",
        success: function (data) {
            $("#TxtSearch").autocomplete({
                source: data
            });
        },
    });
});

function SetLanguageID(val) {
    $.ajax({
        type: "POST",
        url: "/Categories/SetcurrentLanguage",
        datatype: "json",
        contenttype: "application/json",
        data: {
            value: val
        },
        success: function (data) {
            var objJson = jQuery.parseJSON(data);
            if (objJson.success) {
                toastr.success(objJson.message);
            }
            window.location.href = window.location.href;
        },
    });
}

function CheckSearch(e) {
    //alert($("#TxtSearch").val());
    if (e.keyCode == 13) {
        var val = $("#TxtSearch").val();
        if (val.length >= 3) {
            url = "/SearchResult/" + encodeURI(val) + "/Search";
            console.log(url);
            window.location.href = url;
        }
    }
}