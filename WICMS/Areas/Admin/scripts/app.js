﻿//---------------------------------------
var oTable;
$(document).ready(function () {

    $('#title').focus();

    //---------------------------------------
    //--- jquery.dataTables ---
    oTable = $('table.dataTable').dataTable({ 
        "bPaginate": false,
        "bLengthChange": true,
        "bFilter": true,
        "bSort": true,
        "bSortCellsTop": true,
        "sDom": '<"top">t<"bottom"><"clear">',
        aoColumnDefs: [{aTargets: [-1], 
            bSortable: false
        }],
        "bDestroy": true
    });

    //$(".dataTable").addClass("text-center");

    $("thead input").keyup(function () {
        /* Filter on the column (the index) of this element */
        oTable.fnFilter(this.value, $(".dataTable-input input").index(this));
    });
    //---------------------------------------

    //---------------------------------------
    toastr.options = {
        "positionClass": "toast-top-center"
    };

    //if ($(".dataTable").dataTable().fnSettings().aoData.length == 0) {
    //    $(".dataTable").addClass("text-center");
    //    return false;
    //}
});

function UpdateRecord(objForm, formButton, controller, baseUrl) {
    var postUrl = '/' + controller + '/Post';
    var formData = $(objForm).serialize();
    var parentGuid = $(objForm).find('#parentGuid').val();
    if (parentGuid == undefined) {
        parentGuid = "";
    }
    else {
        parentGuid = "Index/" + parentGuid;
    }
    SpinningIconOnForButton(formButton);

    $.post(postUrl, formData, function (data) {
        var objJson = jQuery.parseJSON(data);
        if (objJson.success) {
            toastr.success(objJson.message);
            if (baseUrl === null) {
                WaitAndRedirect("/" + controller + "/" + parentGuid + "?st=");
            } else {
                WaitAndRedirect("/" + baseUrl + "/" + controller + "/" + parentGuid + "?st=");
            }
            
        } else {
            SpinningIconOffForButton(formButton);
            toastr.error(objJson.message + '. Please try again!');
            ProcessServerValidation(objForm, objJson.validation);
            return false;
        };
    });
};

function UpdateRecordLogin(objForm, formButton, controller, baseUrl) {
    var postUrl = '/' + controller + '/Post';
    var formData = $(objForm).serialize();
    var parentGuid = $(objForm).find('#parentGuid').val();
    if (parentGuid == undefined) {
        parentGuid = "";
    }
    else {
        parentGuid = "Index/" + parentGuid;
    }
    SpinningIconOnForButton(formButton);

    $.post(postUrl, formData, function (objJson) {
        //var objJson = jQuery.parseJSON(data);
        if (objJson.success) {
            toastr.success(objJson.message);
            if (baseUrl === null) {
                WaitAndRedirect("/" + controller + "/" + parentGuid + "?st=");
            } else {
                WaitAndRedirect("/" + baseUrl + "/" + controller + "/" + parentGuid + "?st=");
            }

        } else {
            SpinningIconOffForButton(formButton);
            toastr.error(objJson.message + '. Please try again!');
            //ProcessServerValidation(objForm, objJson.validation);
            // window.location.reload();
            $("#password").val("");
            //$('#password').removeAttr('value');
            //Recaptcha.reload();
            if (window.grecaptcha) { grecaptcha.reset() };
        };
    });
};

function UpdateRecordstats(objForm, controller, baseUrl, id) {
    var postUrl = '/' + controller + '/Post';
    var formData = $(objForm).serialize();
    var parentGuid = $(objForm).find('#' + id).val();
    if (parentGuid == undefined) {
        parentGuid = "";
    }
    else {
        parentGuid = "index/" + parentGuid;
    }

    $.post(postUrl, formData, function (data) {
        var objJson = jQuery.parseJSON(data);
        if (objJson.success) {
            toastr.success(objJson.message);
            if (baseUrl === null) {
                //return true;
                WaitAndRedirect("/" + controller + "/" + parentGuid + "?st=");
            } else {
                WaitAndRedirect("/" + baseUrl + "/" + controller + "/" + parentGuid + "?st=");
            }

        } else {
            toastr.error(objJson.message + '. Please try again!');
            ProcessServerValidation(objForm, objJson.validation);
            return false;
        };
    });
};

function UpdateRecordChangePassword(objForm, formButton, controller, baseUrl) {
    var postUrl = '/' + controller + '/ChangePasswordPost';
    var formData = $(objForm).serialize();
    var parentGuid = $(objForm).find('#parentGuid').val();
    if (parentGuid == undefined) {
        parentGuid = "";
    }
    else {
        parentGuid = "index/" + parentGuid;
    }
    SpinningIconOnForButton(formButton);

    $.post(postUrl, formData, function (data) {
        var objJson = jQuery.parseJSON(data);
        if (objJson.success) {
            toastr.success(objJson.message);
            if (baseUrl === null) {
                WaitAndRedirect("/" + controller + "/" + parentGuid + "?st=");
            } else {
                WaitAndRedirect("/" + baseUrl + "/" + controller + "/" + parentGuid + "?st=");
            }

        } else {
            SpinningIconOffForButton(formButton);
            toastr.error(objJson.message + '. Please try again!');
            ProcessServerValidation(objForm, objJson.validation);
            return false;
        };
    });
};

function ProcessServerValidation(objForm, data) {
    var validator = $(objForm).validate();
    $.each(data, function (i, vObj) {
        var obj = {};
        obj[vObj.ValidationField] = vObj.ValidationMessage;
        validator.showErrors(obj);
    });
}

function DeleteRecordRemoveTr(element, controller) {
    var trDelete = $(element).closest('tr');
    
    // Spinning Icon Setup ------------
    var trIcon = $(element).children('i');
    //var trIconClasses = $(trIcon).attr('class');
    SpinningIconOnForRemoveTr(trIcon, trDelete);
    // ---------------------------------

    var guid = $(trDelete).data('id');
    var postUrl = '/' + controller + '/Delete/' + guid;

    var result = confirm("This Will Delete The Record From All Languages. Do You Really Want To Delete This Record?");
    if (result == true) {
       $.post(postUrl, null, function (data) {
            var objJson = jQuery.parseJSON(data);

            if (objJson.success) {
                //toastr.success(controller + ' record deleted successfully!');
                toastr.success('Record deleted successfully!');
                RemoveTr(trDelete);
            } else {
                SpinningIconOffForRemoveTr(trIcon, trDelete);
                toastr.error('There was an error deleting ' + controller + ' record. Please try again!');
            };

        });
    } else {
        SpinningIconOffForRemoveTr(trIcon, trDelete);
    };
}

function DeleteRecordRemoveTrstats(element, controller, method) {
    var trDelete = $(element).closest('tr');

    // Spinning Icon Setup ------------
    var trIcon = $(element).children('i');
    //var trIconClasses = $(trIcon).attr('class');
    SpinningIconOnForRemoveTr(trIcon, trDelete);
    // ---------------------------------

    var guid = $(trDelete).data('id');
    var postUrl = '/' + controller + '/' + method + '/' + guid;

    var result = confirm("Delete this record?");
    if (result == true) {
        $.post(postUrl, null, function (data) {
            var objJson = jQuery.parseJSON(data);

            if (objJson.success) {
                //toastr.success(controller + ' record deleted successfully!');
                toastr.success('Record deleted successfully!');
                RemoveTr(trDelete);
            } else {
                SpinningIconOffForRemoveTr(trIcon, trDelete);
                toastr.error('There was an error deleting ' + controller + ' record. Please try again!');
            };

        });
    } else {
        SpinningIconOffForRemoveTr(trIcon, trDelete);
    };
}

function RemoveTr(trDelete) {
    $(trDelete).removeClass().addClass('deleteTr');
    $(trDelete).find("td").removeClass().addClass('deleteTd');

    $(trDelete).fadeOut(1300, function () {
        $(trDelete).remove();
    });  
}

function SpinningIconOnForRemoveTr(element, parentContainer) {
    $(parentContainer).addClass('danger');
    // $(element).removeClass();
    $(element).addClass('fa-spinner fa-spin');
}

function SpinningIconOffForRemoveTr(element, parentContainer) {
    $(parentContainer).removeClass('danger');
    $(element).removeClass('fa-spinner fa-spin');
    // $(element).addClass(ogClass);
}

function SpinningIconOnForButton(element) {
    $(element).attr('disabled', true);
    $(element).html(CreateSpinningIcon());
}

function SpinningIconOffForButton(element) {
    $(element).attr('disabled', false);
    $(element).html("Submit");
}

function CreateSpinningIcon() {
    var strHtml = "<i class='fa fa-spinner fa-spin'></i> Processing ... ";
    return strHtml;

}

function WaitAndRedirect(url) {
    setTimeout(function () {
        window.location = url;
    }, 1500);
}

function ListBoxMoveItems(selectListBox, targetListBox) {
    var selectedOpts = $(selectListBox).find(':selected');
    $(targetListBox).append($(selectedOpts).clone());
    $(selectedOpts).remove();
    // Reorder Listbox
    SortTexBoxList(selectListBox);
    SortTexBoxList(targetListBox);
}

function SortTexBoxList(texboxList) {
    var $r = $(texboxList).find('option');

    $r.sort(function (a, b) {
        if (a.text < b.text) return -1;
        if (a.text == b.text) return 0;
        return 1;
    });

    $($r).remove();

    $(texboxList).append($($r));
}

function UpdateDraggableSortNicheLayout(nicheItems) {
    var postUrl = "/Niche/UpdateDraggableSortNicheLayout";
    var formData = { itemsList: nicheItems };

    $.post(postUrl, formData, function (data) {
        var objJson = jQuery.parseJSON(data);
        if (objJson.success) {
            // alert(objJson.success);
        } else {
            alert("There was an error .. try again");
        }
    });
}

// ------------------------------------------------------------
// ------------------------------------------------------------