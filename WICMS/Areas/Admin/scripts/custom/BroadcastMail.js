﻿$(document).ready(function () {
    $("#form-main").submit(function () {
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
        var data = $("#form-main").serialize();
        var postUrl = "/BroadCastMail/Post";
        $.post(postUrl, data, function (result) {

            if (result.success) {
                toastr.success(result.message);
                window.location.href = "/Admin/BroadCastMail";
                // window.location.href
            }
            else {
                toastr.error(result.message);
            }
        });
        return false;
    });
    $("#btnAddMail").click(function () {
        var email = $("#Email").val();
        var form_data = $("#frm1").serialize();
        //alert(email);{ Email: email }
        var url = "/Admin/SubscriberManage/Post";
        $.post(url, form_data, function (result) {
            var data = $.parseJSON(result);

            //$.parseJSON(result).success; false            
            //$.parseJSON(result)["success"]; false
            
            if (data.success) {
                toastr.success(data.message);
                window.location.href = "/Admin/BroadCastMail";
            }
            else {
                //alert(data.message);
                //toastr.error(data.message);
                var obj = "";
                $.each(data.validation, function (i, vObj) {
                    obj += vObj.ValidationMessage + "<br />";
                });
                toastr.error(obj);
            }
        });
        return false;
    });
});