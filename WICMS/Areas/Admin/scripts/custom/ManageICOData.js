﻿$(document).ready(function () {
   
   var table = $('#ICOData').dataTable({
        "responsive": true,
        "pageLength": 100,
        "scrollCollapse": true,
        "order": [[2, "asc"]],
        "ajax": "/Admin/ICOManage/BindIcoData",
        'createdRow': function (row, data, dataIndex) {
            $(row).attr('data-id', data.Guid);
        },
        "columns": [
            //{
            //    "data": null, title: 'EDIT', width: "30px", className: "text-center", responsivePriority: 1,
            //    render: function (data, type, row, meta) {
            //        if (type === 'display') {
            //            data = '<a href="/Admin/ICOManage/Details?guid=' + row.Guid + '"><i class="fa fa-edit"></i></a> ';
            //        }
            //       return data;
            //    }
            //},
            //{
            //    "data": null, title: 'DELETE', width: "30px", className: "text-center", responsivePriority: 2,
            //    render: function (data, type, row, meta) {
            //        if (type === 'display') {
            //            data = '<a href="#" class="tr-delete" onclick="myDelete(this);"><i class="fa fa-trash-o"></i></a>';
            //        }
            //        return data;
            //    }
            //},
            {
                "data": null, title: 'ACTION', width: "30px", className: "text-center", responsivePriority: 1,
                render: function (data, type, row, meta) {
                    if (type === 'display') {
                        data = '<a href="/Admin/ICOManage/Details?guid=' + row.Guid + '"><i class="fa fa-edit"></i></a> | <a href="#" class="tr-delete" onclick="myDelete(this);"><i class="fa fa-trash-o"></i></a> | <a href="/ICO/Details?guid=' + row.Guid + '"><i class="fa fa-info"></i></a>';
                    }
                    return data;
                }
            },
            {
                "data": "CoinName", title: 'Coin Name', width: "60px", className: "text-left", responsivePriority: 3,
                render: function (data, type, row, meta) {
                    if (type === 'display') {
                        data = '<img src="' + row.CoinIcon + '" onerror="this.src=\'/images/no-image-found.jpg\'" height="25" width="25" /> <a href="' + (row.Website == null ? '' : row.Website) + '" ' + (row.ReferrerUrl == null ? '' : ' class="linkRefurl" data-refurl="' + row.ReferrerUrl + '"') + ' >' + data + '</a>';
                    }
                    return data;
                }
            },
            {
                "data": "Rate", title: 'Rate/5', width: "35px", className: "text-center", responsivePriority: 4,
                render: function (data, type, row, meta) {
                    return data;
                }
            },
            {
                "data": "Stage", title: 'Stage', width: "40px", className: "text-center", responsivePriority: 5,
                render: function (data, type, row, meta) {
                    return data;
                }                    
            },
            {
                "data": "Price", title: 'Price', width: "50px", className: "text-center", responsivePriority: 6,
                render: function (data, type, row, meta) {
                    return "$ " + data;
                }
            },
            {
                "data": "IsRecommend", title: 'Recommand', width: "40px", className: "text-center", responsivePriority: 2,
                render: function (data, type, row, meta) {
                    if(type == 'display'){
                        data = '<input name="my-checkbox" type="checkbox" class="js-switch"' + (row.IsRecommend == true ? "checked" : "") + ' onchange="changeRecommand("' + row.Guid + '",this)" >';
                        //data='<input name="my-checkbox" type="checkbox" class="js-switch" ("' + row.IsRecommend + "?" "checked":"") onchange="changeRecommand("' +row.Guid + ")',this)">'';
                    }
                    return data;
                }
               
            }
        ]

    });
   
   
  // 'draw', function ()
    //$('#ICOData').on('page.dt', function () {
        $('#ICOData').on('draw.dt', function () {
            var chk = $("span.switchery");
            if (chk.length == 0) {
                // chk.destroy();
                var Switchery = require('switchery');
                $("input.js-switch").each(function (index, item) {
                    var init = new Switchery(item);
                });
            }
            else {
                $("span.switchery").remove();
                var Switchery = require('switchery');
                $("input.js-switch").each(function (index, item) {
                    var init = new Switchery(item);
                });
            }
        //});            
    })
    .DataTable();
   
    //$('#ICOData tbody').on('a.paginate_button', function () {
    //    var Switchery = require('switchery');
    //    $("input.js-switch").each(function (index, item) {
    //        var init = new Switchery(item);
    //    });
    //});
        //$('#ICOData tbody').on('change', 'input[type="checkbox"]', function () {
                //var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
                //elems.forEach(function (html) {
                //    var switchery = new Switchery(html);
                //});               
        //});

    $("#ICOData").delegate("span.switchery", "click", function () {
        var tr = $(this).parent().parent();
            $.post({
                url: "/admin/ICOManage/ChangeRecommand",
                data: { "guid": tr.data('id'), "isRecommand": tr.find("[type=checkbox]")[0].checked },
                dataType: 'json',
                success: function (result) {
                    if (!result.success)
                        toastr.error(result.message);
                },
                error: function () {
                    toastr.error('Send request fail!');
                }
            });
    });

    //$('#example tbody').on('click', 'tr', function () {
    //    var data = table.row(this).data();
    //    alert('You clicked on ' + data[0] + '\'s row');
    //});
    //$("#ICOData tbody span.switchery").click(function (event, state) {  
    //});

    var dt = $("td");
    $('#ICOData tbody tr').attr('data-id','value');
    
});
function myDelete(th) {
    //$("a.tr-delete").on("click", function () {
        //$("a.tr-delete").click( function () {
        DeleteRecordRemoveTr(th, 'Admin/ICOManage');
    //});
}