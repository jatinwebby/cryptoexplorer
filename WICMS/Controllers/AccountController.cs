﻿using BLL.Tools;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace WiCms.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            try
            {
                if (!WebSecurity.Initialized)
                {
                    WebSecurity.InitializeDatabaseConnection("DefaultConnection", "User", "Id", "UserName", true);
                }
                if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
                    return Redirect("/Home");
                else
                    return Redirect("/Admin/Login");
            }
            catch (Exception ex)
            {
                Guid guid = CommonFunction.LogManager(Request.FilePath, ex, "/AccountController/Login()");
                return Redirect("/Account/Index/");
            }
        }
        public ActionResult Logout()
        {
            WebSecurity.Logout();
            return Login();
        }

        [HttpPost]
        public JsonResult LoginPost(FormCollection form)
        {
            var results = new BLL.ModelViews.JsonMessageView();
           
            try
            {
                var captchaResponse = BLL.Tools.CommonFunction.ValidateCaptcha(Request["g-recaptcha-response"]);

               
                if (captchaResponse.Success)
                {
                    string l_username = form["username"];
                    string l_password = form["password"];
                    bool success = WebSecurity.Login(l_username, BLL.Tools.CommonFunction.Encrypt(l_password) , true);
                        if (success)
                        {
                            var userid = WebSecurity.GetUserId(form["username"]);
                            var l_UserData=BLL.Queries.User.GetById(userid);
                            if (l_UserData != null)
                            {
                                results.success = true;
                                results.message = "Login Successful, Welcome " + l_UserData.UserName;
                            }
                            else
                            {
                                results.success = false;
                                results.message = "Login Fail!";
                            }
                        }
                        else
                        {
                            results.success = false;
                            results.message = "Login Fail!";
                        }
                }
                else
                {
                    var error = captchaResponse.ErrorCodes[0].ToLower();
                    switch (error)
                    {
                        case ("missing-input-secret"):
                            results.success = false;
                            results.message = "The secret parameter is missing.";
                            break;
                        case ("invalid-input-secret"):
                            results.success = false;
                            results.message = "The secret parameter is invalid or malformed.";
                            break;
                        case ("missing-input-response"):
                            results.success = false;
                            results.message = "The response parameter is missing.";
                            break;
                        case ("invalid-input-response"):
                            results.success = false;
                            results.message = "The response parameter is invalid or malformed.";
                            break;
                        default:
                            results.success = false;
                            results.message = "Error occured. Please try again";
                            break;
                    }
                    return Json(results, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Guid gid = CommonFunction.LogManager(Request.FilePath, ex, "Error Occur on User Login");
                results.success = false;
                results.message = "Error Occur on User Login";
               // Response.Redirect("/LogManager/Index/" + gid);
            }
            return Json(results,JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult Register(BLL.ModelViews.User.Extend f_UserData)
        {
            try
            {
                // ----- Temp Default Data ----- //
                f_UserData.Role = "Admin";
                f_UserData.FirstName = "Admin";
                f_UserData.LastName = "User";
                f_UserData.Email = "ravi@webbyinfotech.com";
                f_UserData.Country = "India";
                f_UserData.UpdatedDate = DateTime.Now;
                f_UserData.CreatedDate = DateTime.Now;
                f_UserData.Password = BLL.Tools.CommonFunction.Encrypt(f_UserData.Password);
                WebSecurity.CreateUserAndAccount(f_UserData.UserName,f_UserData.Password,new { f_UserData.UserName, f_UserData.Password, f_UserData.Role, f_UserData.FirstName, f_UserData.LastName, f_UserData.Email, f_UserData.Country });
                Response.Redirect("~/account/login");
            }
            catch (Exception ex)
            {
                Guid gid = CommonFunction.LogManager(Request.FilePath, ex, "Error Occuring on Registration.");
                //Response.Redirect("/LogManager/Index/" + gid);
            }
            return View();
        }
    }
}