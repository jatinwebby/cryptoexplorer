﻿using BLL.Tools;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;

namespace WiCms.Controllers
{
    public class BaseController : Controller
    {
        public BaseController()
        {
            try

            {
                if (!WebSecurity.Initialized)
                {
                    try
                    {
                        WebSecurity.InitializeDatabaseConnection("DefaultConnection", "User", "Id", "UserName", true);
                        //WebSecurity.InitializeDatabaseConnection("DefaultConnection", "[dbo].[User]", "UserId", "UserName", autoCreateTables: true);
                        //WebSecurity.InitializeDatabaseConnection("DefaultConnection", "UserProfile", "UserId", "UserName", autoCreateTables: true);
                    }
                    catch (Exception ex)
                    {
                        throw new InvalidOperationException("The ASP.NET Simple Membership database could not be initialized. For more information, please see http://go.microsoft.com/fwlink/?LinkId=256588", ex);
                    }
                }
            }
            catch (Exception e)
            {
               // WebSecurity.Logout();
            }
        }


        public string RenderRazorViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext,
                                                                         viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View,
                                             ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        public long CurrentCustomerId
        {
            get
            {
                if (System.Web.HttpContext.Current.User == null)
                { return 0; }
                else
                {
                    if (System.Web.HttpContext.Current.User.IsInRole(Constants.Role.ADMIN))
                        return 0;
                    else
                    {
                        return BLL.Queries.User.GetIDByUsername(System.Web.HttpContext.Current.User.Identity.Name);
                    }
                }
            }
        }

         

        public TimeZoneInfo CurrentStaffOfficeTimeZone
        {
            get
            {
                //Convert Date UTC to office Local Time
                //var timeZone = ISDAL.FindStaffTimeZoneByEntity(MemberType.MEMBERTYPE_STAFF, CurrentStaffId);
                return null;// timeZone;
            }
        } 


        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //string controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            //string actionName = filterContext.ActionDescriptor.ActionName;
            //double days = ISDAL.CheckExpiryDays(CurrentCustomerId);
            //if (days > 7)
            //{
            //    //redirect to RenewCheckout after 7 days of Licence Expired
            //    if ((controllerName != "Account" && actionName != "Login") && (controllerName != "Account" && actionName != "CheckOut") && (controllerName != "Account" && actionName != "PaymentConfirmation") && (controllerName != "Home" && actionName != "MockPayment"))
            //    {
            //        filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
            //        {
            //            controller = "Account",
            //            action = "RenewCheckOut",
            //        }));
            //    }
            //}
        }

    }

    public class DownloadFileActionResult : ActionResult
    {

        public GridView ExcelGridView { get; set; }
        public string fileName { get; set; }


        public DownloadFileActionResult(GridView gv, string pFileName)
        {
            ExcelGridView = gv;
            fileName = pFileName;
        }


        public override void ExecuteResult(ControllerContext context)
        {

            //Create a response stream to create and write the Excel file
            HttpContext curContext = HttpContext.Current;
            curContext.Response.Clear();
            curContext.Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
            curContext.Response.Charset = "";
            curContext.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            curContext.Response.ContentType = "application/vnd.ms-excel";

            //Convert the rendering of the gridview to a string representation 
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            ExcelGridView.RenderControl(htw);

            //Open a memory stream that you can use to write back to the response
            byte[] byteArray = Encoding.ASCII.GetBytes(sw.ToString());
            MemoryStream s = new MemoryStream(byteArray);
            StreamReader sr = new StreamReader(s, Encoding.ASCII);

            //Write the stream back to the response
            curContext.Response.Write(sr.ReadToEnd());
            curContext.Response.End();

        }
    }
}