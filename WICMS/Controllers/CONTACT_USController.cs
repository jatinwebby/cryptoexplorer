﻿using BLL.Tools;
using BLL.Validations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WiCms.Controllers
{
    public class CONTACT_USController : Controller
    {
        // GET: CONTACT_US
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult Post(FormCollection data)
        {
            var results = new BLL.ModelViews.JsonMessageView();
            try
            {
                var ip = System.Web.HttpContext.Current != null ? System.Web.HttpContext.Current.Request.UserHostAddress : "";
                // ----- local test ----- //
                if (ip == "::1")
                    ip = "116.73.199.88";

                var captchaResponse = BLL.Tools.CommonFunction.ValidateCaptcha(Request["g-recaptcha-response"]);
                //Request.Url.Host == "localhost"
                if (captchaResponse.Success)
                {
                    if (data != null)
                    {
                        string firstName = data["FirstName"];
                        string lastName = data["LastName"];
                        string mailFrom = data["EmailId"];
                        long phoneNo = Convert.ToInt64(data["PhoneNo"]);
                        string message = data["Message"];
                        string fullName = firstName + " "+ lastName;
                        string body = "Name:" + fullName + " MobNo:" + phoneNo + " \n " + message;
                        var cms_data = BLL.Queries.CMSManage.GetByPageName("CONTACT_US_MAIL");

                        string link_ip = "http://freegeoip.net/?q=" + ip;

                        cms_data.Content = cms_data.Content.Replace("##name##", fullName).Replace("##phone##", Convert.ToString(phoneNo)).Replace("##email##", mailFrom).Replace("##subject##", cms_data.Title).Replace("##message##", message).Replace("##iplink##", link_ip).Replace("##ipaddress##", ip);
                        bool res_mail=BLL.Tools.CommonFunction.sendMail(cms_data.Title, cms_data.Content, "", mailFrom);
                        if(res_mail== true) { 
                                    results.success = true;
                                    results.message = "Mail Send Successfully.";
                        }
                        else
                        {
                                    results.success = false;
                                    results.message = "Mail not send Successfully.";
                        }
                    }
                    else
                    {
                       results.success = false;
                       results.message = "Please fill up all Required Field.";
                    }
                }
                else
                {

                    var error = captchaResponse.ErrorCodes[0].ToLower();
                    switch (error)
                    {
                        case ("missing-input-secret"):
                            results.success = false;
                            results.message = "The secret parameter is missing in reCaptcha.";
                            break;
                        case ("invalid-input-secret"):
                            results.success = false;
                            results.message = "The secret parameter is invalid or malformed in reCaptcha.";
                            break;
                        case ("missing-input-response"):
                            results.success = false;
                            results.message = "The reCaptcha parameter is missing.";
                            break;
                        case ("invalid-input-response"):
                            results.success = false;
                            results.message = "The reCaptcha parameter is invalid or malformed.";
                            break;
                        default:
                        results.success = false;
                        results.message = "Error occured. Please try again";
                        break;
                    }
                    //return Json(results, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Guid Errorguid = CommonFunction.LogManager(Request.FilePath, ex, "User/CONTACT_USController/Post()");
                //return RedirectToAction("Index", "LogManager", new { guID = gid });
                results.success = false;
                results.message = "Error Sending Mail";
            }
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Details()
        {
            return View();
        }
    }
}