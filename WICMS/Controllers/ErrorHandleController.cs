﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL.ModelViews;

namespace WiCms.Controllers
{
    public class ErrorHandleController : Controller
    {
        // GET: ErrorHandle
        //[ActionName("404")]
        //[HandleError]
        public ActionResult Index()
        {
            //URLResponse data = new URLResponse();
            ////data.CustomerID = int.Parse(Request.QueryString["CustomerID"]);
            //data.Title = Request.QueryString["Title"];
            //data.Message = Request.QueryString["Message"];
            //data.Page_Link = Request.QueryString["Page_Link"];
            //data.Link_Text = Request.QueryString["Link_Text"];

            URLResponse data = TempData["mydata"] as URLResponse;

            return View(data);
            //return View("Error");
        }
        
        public ActionResult NotFound()
        {
            return View();
        }

        public ActionResult Error()
        {
            return View();
        }
        public ActionResult PageError()
        {
            return View();
        }
        public ActionResult Runtime()
        {
            return View();
        }
    }
}