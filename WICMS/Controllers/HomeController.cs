﻿using BLL.Mappers;
using BLL.Tools;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL.ModelViews;

namespace WiCms.Controllers
{
    //[Authorize]//(Roles = "Admin, Client, Staff, Customer, TempUser, UnAllocatedClient, SubAgent")]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        //[WebMethod]
        public ActionResult UpdatePrcieJob()
        {
            var results = new JsonMessageView();
            List<BLL.ModelViews.ICO.Extend> listICO = new List<BLL.ModelViews.ICO.Extend>();
            var l_ICO = BLL.Queries.ICO.GetFiveMUpdated();
            try
            {               
                HtmlWeb web = new HtmlWeb();
                var htmlDoc = web.Load(BLL.Tools.Constants.API.IcoReview);

                HtmlNode[] table1_nodes = htmlDoc.DocumentNode.SelectNodes("//table[@id='tablepress-4']/tbody/tr").ToArray();
                foreach (HtmlNode item1 in table1_nodes)
                {
                    var name = item1.SelectSingleNode("td[@class='column-1']/a[1]").InnerText;
                    double price = 0, rate = 0;
                    var temp_price = item1.SelectSingleNode("td[@class='column-9']").InnerText;
                    var temp_rate = item1.SelectSingleNode("td[@class='column-2']").InnerText;

                    if (temp_rate != "PENDING" && temp_rate != "CLOSED" && temp_rate != "")
                    {
                        temp_rate = temp_rate.Replace("/5", "");
                        rate = Convert.ToDouble(temp_rate);
                    }
                    if (!string.IsNullOrWhiteSpace(temp_price))
                    {
                        temp_price = temp_price.Replace("$", "");
                        int value = 0;
                        if (int.TryParse(temp_price, out value))
                        {
                            // temp_price is an int
                            //price = Convert.ToDouble("0"+value);
                            price = Convert.ToDouble(value);
                        }
                        //else{ // temp_price is not an int }
                    }
                    if (l_ICO.Any(w => w.CoinName == name))
                    {
                        if (!listICO.Any(w => w.CoinName == name))
                        {
                            var old_data = l_ICO.FirstOrDefault(m => m.CoinName == name);
                            listICO.Add(new BLL.ModelViews.ICO.Extend() { CoinName = name, Price = price, MarketCap = old_data.MarketCap, CirculatingSupply = old_data.CirculatingSupply, MaxSupply = old_data.MaxSupply, Rate = rate });
                        }
                    }
                }

                HtmlNode[] table2_nodes = htmlDoc.DocumentNode.SelectNodes("//table[@id='tablepress-2']/tbody/tr").ToArray();
                foreach (HtmlNode item2 in table2_nodes)
                {
                    var name = item2.SelectSingleNode("td[@class='column-1']/a").InnerText;
                    double price = 0, rate = 0;
                    var temp_price = item2.SelectSingleNode("td[@class='column-9']").InnerText;

                    var temp_rate = item2.SelectSingleNode("td[@class='column-2']").InnerText;

                    if (temp_rate != "PENDING" && temp_rate != "CLOSED" && temp_rate != "")
                    {
                        temp_rate = temp_rate.Replace("/5", "");
                        rate = Convert.ToDouble(temp_rate);
                    }

                    if (!string.IsNullOrWhiteSpace(temp_price))
                    {
                        temp_price = temp_price.Replace("$", "");
                        int value = 0;
                        if (int.TryParse(temp_price, out value))
                        {
                            // temp_price is an int
                            //price = Convert.ToDouble("0"+value);
                            price = Convert.ToDouble(value);
                        }
                        //else{ // temp_price is not an int }
                    }
                    if (l_ICO.Any(w => w.CoinName == name))
                    {
                        if (!listICO.Any(w => w.CoinName == name))
                        {
                            var old_data = l_ICO.FirstOrDefault(m => m.CoinName == name);
                            listICO.Add(new BLL.ModelViews.ICO.Extend() { CoinName = name, Price = price, MarketCap = old_data.MarketCap, CirculatingSupply = old_data.CirculatingSupply, MaxSupply = old_data.MaxSupply, Rate = rate });
                        }
                    }
                }

                var response_CoinMarketCap = CommonFunction.GetStringFromURL(Constants.API.CoinMarketCap);
                response_CoinMarketCap = response_CoinMarketCap.Replace("24h_volume_usd", "volume_usd_24h");
                List<BLL.ModelViews.CoinMarketCap> result_CoinMarket = BLL.Tools.Converstions.JsonDeserialize<List<BLL.ModelViews.CoinMarketCap>>(response_CoinMarketCap);

                //var l_ICO = BLL.Queries.ICO.GetFiveMUpdated();
                var code_list = l_ICO.Select(c => c.CoinCode).ToList();
                var res = result_CoinMarket.Where(w => code_list.Contains(w.symbol)).Count();
                foreach (var item in l_ICO)
                {
                    try
                    {
                        var d = result_CoinMarket.FirstOrDefault(w => w.name.ToLower() == item.CoinName.ToLower() || w.symbol == item.CoinCode);
                        if (d != null)
                        {
                            item.Price = Convert.ToDouble(d.price_usd);
                            item.MarketCap = Convert.ToDouble(d.market_cap_usd);
                            item.CirculatingSupply = Convert.ToDouble(d.available_supply);
                            item.MaxSupply = Convert.ToDouble(d.max_supply);
                            if (!listICO.Any(w => w.CoinName == item.CoinName))
                            {
                                listICO.Add(new BLL.ModelViews.ICO.Extend() { CoinName = item.CoinName, Price = item.Price, MarketCap = Convert.ToDouble(d.market_cap_usd), CirculatingSupply = item.CirculatingSupply, MaxSupply = item.MaxSupply, Rate = item.Rate });
                            }
                        }
                        else
                        {
                            try
                            {
                                var response_CryptoCompare_Symbol = CommonFunction.GetStringFromURL(String.Format(Constants.API.CryptoCompare_Symbol, item.CoinCode));
                                dynamic result_CC = Newtonsoft.Json.JsonConvert.DeserializeObject(response_CryptoCompare_Symbol, typeof(object));
                                //string response = result_CC["Response"];
                                if (result_CC["Response"] != "" && result_CC["Response"] == "Error")
                                {
                                    try
                                    {
                                        //response_CryptoNator != "" && 
                                        var response_CryptoNator = CommonFunction.GetStringFromURL(String.Format(Constants.API.CryptoNator, item.CoinCode));
                                        if (!response_CryptoNator.StartsWith("<!DOCTYPE html>") && response_CryptoNator != "Too many automatic redirections were attempted." && response_CryptoNator != "Unable to connect to the remote server")
                                        {
                                            var nator_res = BLL.Tools.Converstions.JsonDeserialize<dynamic>(response_CryptoNator);
                                            //string success_response = nator_res.success; dynamic nator_res
                                            //if (nator_res != "") 
                                            //{   Unable to connect to the remote server
                                            if (!string.IsNullOrEmpty(Convert.ToString(nator_res)) && nator_res.success != "False")
                                            {
                                                item.Price = Convert.ToDouble(nator_res["ticker"].price);
                                                if (!listICO.Any(w => w.CoinName == item.CoinName))
                                                {
                                                    listICO.Add(new BLL.ModelViews.ICO.Extend() { CoinName = item.CoinName, Price = item.Price, MarketCap = item.MarketCap, CirculatingSupply = item.CirculatingSupply, MaxSupply = item.MaxSupply, Rate = item.Rate });
                                                }
                                            }
                                            //}
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Guid Errorguid = CommonFunction.LogManager("Global.asax/timer_Elapsed()/response_CryptoNator", ex, "Exception Caught.");
                                    }
                                }
                                else
                                {
                                    item.Price = result_CC["RAW"][item.CoinCode]["USD"].PRICE;
                                    item.CirculatingSupply = result_CC["RAW"][item.CoinCode]["USD"].SUPPLY;
                                    item.MarketCap = result_CC["RAW"][item.CoinCode]["USD"].MKTCAP;
                                    if (!listICO.Any(w => w.CoinName == item.CoinName))
                                    {
                                        listICO.Add(new BLL.ModelViews.ICO.Extend() { CoinName = item.CoinName, Price = item.Price, MarketCap = item.MarketCap, CirculatingSupply = item.CirculatingSupply, MaxSupply = item.MaxSupply, Rate = item.Rate });
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Guid Errorguid = CommonFunction.LogManager("Global.asax/timer_Elapsed()/response_CryptoCompare_Symbol", ex, "Exception Caught.");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Guid Errorguid = CommonFunction.LogManager("Global.asax/timer_Elapsed()/result_CoinMarket", ex, "Exception Caught.");
                    }
                }
                BLL.Commands.ICO.UpdatePrice(listICO);
                results.success = true;
                results.message = "Success Updating Price";
            }
            catch (System.Threading.ThreadAbortException ex)
            {
                // ignore it
                Guid Errorguid = CommonFunction.LogManager("Global.asax/timer_Elapsed()/ThreadAbortException", ex, "Exception Caught.");
            }
            catch (Exception ex)
            {                
                Guid Errorguid = CommonFunction.LogManager("Global.asax/timer_Elapsed()", ex, "Exception Caught.");
            }
            return View();
        }

    }
}