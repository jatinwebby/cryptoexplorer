﻿using BLL.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL.ModelViews;

namespace WiCms.Controllers
{
    //[Authorize]
    public class ICOController : BaseController
    {
        // GET: ICO
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(string guid)
        {
            ViewBag.ReturnUrl = Request.UrlReferrer;
            try
            {
                Guid id;        
                bool res= Guid.TryParse(guid,out id);
                if (res == false)
                {
                    URLResponse response = new URLResponse()
                    {
                        Title = "ICO Detail",
                        Message = "Invalid Request...",
                        Page_Link = "/ICO/Index",
                        Link_Text = "GO BACK"
                    };
                    TempData["mydata"] = response;
                    URLResponse error_data = TempData["mydata"] as URLResponse;
                    //return RedirectToAction("Index", "ErrorHandle");
                    return View("PageError", error_data);
                }
                ViewBag.Guid = id;
                var data = BLL.Queries.ICO.GetByGuid(id);
               
                if (data == null)
                { 
                    URLResponse response = new URLResponse() {
                        Title ="ICO Detail",
                        Message = "Invalid Request...",
                        Page_Link = "/ICO/Index",
                        Link_Text ="GO BACK"
                    };
                    //string url = string.Format("/ErrorHandle/Index?Title={0}&Message={1}&Page_Link={2}&Link_Text={3}",response.Title,response.Message,response.Page_Link,response.Link_Text);
                    //return Redirect(url);
                    TempData["mydata"] = response;
                    URLResponse error_data = TempData["mydata"] as URLResponse;
                    //return RedirectToAction("Index", "ErrorHandle");
                    return View("PageError", error_data);
                }
                var price = data.Price;
                ViewBag.Price = price;
                return View(data);
            }
            catch (Exception ex)
            {
                Guid Errorguid = CommonFunction.LogManager(Request.FilePath, ex, "User/ICOController/Details()");
                return Redirect("/ICO/Index/");
            }
        }

        public ActionResult HistoryData(Guid guid)
        {
            try
            {
                ViewBag.Guid = guid;                
                return View();
            }
            catch (Exception ex)
            {
                Guid Errorguid = CommonFunction.LogManager(Request.FilePath, ex, "User/ICOController/HistoryData()");
                return Redirect("/ICO/Index/");
            }
        }
    }
}