﻿using BLL.ModelViews;
using BLL.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WiCms.Controllers
{
    public class NewsController : Controller
    {
        // GET: News
        public ActionResult Index()
        {
            ViewBag.PageName = BLL.Tools.Constants.PageName.NewsManage;
            //var data = BLL.Queries.NewsManage.GetAll();
            return View();
        }

        public ActionResult Details(string guid)
        {
            try
            {
                ViewBag.PageName = BLL.Tools.Constants.PageName.NewsManage;
                Guid id;
                bool res = Guid.TryParse(guid, out id);
                if (res == false)
                {
                    URLResponse response = new URLResponse()
                    {
                        Title = "News Detail",
                        Message = "Invalid Request...",
                        Page_Link = "/News/Index",
                        Link_Text = "GO BACK"
                    };
                    TempData["mydata"] = response;
                    URLResponse error_data = TempData["mydata"] as URLResponse;
                    return View("PageError", error_data);
                }
                ViewBag.Guid = id;
                var data = BLL.Queries.NewsManage.GetByGuid(id);
                //data.StartEventDate = Convert.ToDateTime(data.StartEventDate).ToLocalTime();
                if (data == null)
                {
                    URLResponse response = new URLResponse()
                    {
                        Title = "News Detail",
                        Message = "Invalid Request...",
                        Page_Link = "/News/Index",
                        Link_Text = "GO BACK"
                    };
                    TempData["mydata"] = response;
                    URLResponse error_data = TempData["mydata"] as URLResponse;
                    return View("PageError", error_data);
                }
                
                return View(data);
            }
            catch(Exception ex)
            {
                Guid Errorguid = CommonFunction.LogManager(Request.FilePath, ex, "User/NewsController/Details()");
                return Redirect("/News/Index/");
            }
            
        }
    }
}