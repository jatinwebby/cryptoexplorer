﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WiCms.Controllers
{
    public class PageController : Controller
    {
        public ActionResult Index(string title="")//PageName
        {
            var data = BLL.Queries.CMSManage.GetByPageName(title);
            return View(data);
        }
    }
}