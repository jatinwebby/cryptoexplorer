﻿//---------------------------------------
var oTable;
var g_oMoneyFormatter = null;
// ----- Open Referrer Url on each linkRefurl class click ----- //
$(document).on("click", ".linkRefurl", function () {
    window.open($(this).data('refurl'));
    event.preventDefault();
});

$(document).ready(function () {
    // ----- Hide and Show Menu Header Logo on scroll possion ----- //
    scrollFunction();
    window.onscroll = function () { scrollFunction() };
    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            document.getElementById("CryptoExploreLogo").style.top = "0";
        } else {
            document.getElementById("CryptoExploreLogo").style.top = "-200px";
        }
    }

    // ----- Formate Strign to Money ----- //
    // ----eg----. g_oMoneyFormatter.formate(value);
    g_oMoneyFormatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',
        maximumFractionDigits: 2,
        minimumFractionDigits: 0,
        // the default value for minimumFractionDigits depends on the currency
        // and is usually already 2
    });

    //----- Fetch Ads from CMS API -----//
    if ($('#APIURL').val() != undefined) {

        CMSRender($('#APIURL').val() + "CMSManage/GetRandomAds/?ID=ADS_HEADER", 'adsHeader');
        CMSRender($('#APIURL').val() + "CMSManage/GetRandomAds/?ID=ADS_RIGHT", 'adsRight');
        //CMSRender($('#APIURL').val() + "CMSManage/GetRandomAds/?ID=ADS_LEFT", 'adsLeft');
        CMSRender($('#APIURL').val() + "CMSManage/GetRandomAds/?ID=ADS_FOOTER", 'adsFooter');
    }

    //-----Validate Subscribe Form using Jquery-----//
    $("#frmSubscribe").validate({
        rules: {
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            email: {
                required: "Plaese enter your Email Id.",
                email: "Please enter valid Email Id."
            }
        }
    });

    //----- Register Subscribe----- //
    $('#frmSubscribe').submit(function () {
        if (!$('#frmSubscribe').valid()) {
            return false;;
        }
        var formdata = $(this).serialize();
        $.post($('#APIURL').val() + "Subscriber/AddEditDetail", formdata, function (result) {
            if (result.Status == "Success") {
                toastr.success(result.Message);
                $("#footer-mailchimp").val('');
                //$('input[type=text]').each(function () {
                //    $(this).val('');
                //});
            } else {
                toastr.error(result.Message);
                $("#footer-mailchimp").val('');
                //ProcessServerValidation(objForm, result.validation);
                return false;
            };
        })
        return false;
    });
    
    /* 
    $('#title').focus();
    //---------------------------------------
    //--- jquery.dataTables ---
    oTable = $('table.dataTable').dataTable({ 
        "bPaginate": false,
        "bLengthChange": true,
        "bFilter": true,
        "bSort": true,
        "bSortCellsTop": true,
        "sDom": '<"top">t<"bottom"><"clear">',
        aoColumnDefs: [{aTargets: [-1], 
            bSortable: false
        }],
        "bDestroy": true
    });

    

    $("thead input").keyup(function () {
        /* Filter on the column (the index) of this element *
        oTable.fnFilter(this.value, $(".dataTable-input input").index(this));
    });
    //---------------------------------------

    //---------------------------------------
    toastr.options = {
        "positionClass": "toast-top-center"
    };

    */

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-full-width",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "0",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "slideDown",
        "hideMethod": "slideUp"
    };
});

function RenderBody(url) {

    $.get(url, "", function (result) {
        if (result.Status == "Success") {
            $('#pnlBody').html(' <section class="ls section_padding_top_100 section_padding_bottom_150"><div class="container">' + result.Data.Content + '</div></section>');
        } else {
            toastr.error(result.Message);
            ProcessServerValidation(objForm, result.validation);
            return false;
        };
    });
}

function CMSRender(url,divId,callback) {
    $.get(url, "", function (result) {
        if (result.Status == "Success") {
            $('.' + divId).html(result.Data.Content);
            $('.' + divId).attr("title", result.Data.Title);
            if (callback != undefined)
                callback();
        } else {
            if (result.Message != "")
                toastr.error(result.Message);
            return false;
        };
    });
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

//-------------Added for Select Custome Conrtoller user and baseUrl---------------//
function PostFormData(objForm, formButton, controller, baseUrl) {
    var postUrl =  controller ;
    var formData = $(objForm).serialize();
    SpinningIconOnForButton(formButton);

    $.post(postUrl, formData, function (result) {
        SpinningIconOffForButton(formButton); 
        if (result.success) {
            toastr.success(result.message);
            if (baseUrl != null && baseUrl != "") {
                console.log(baseUrl);
                WaitAndRedirect(baseUrl);
            }
        } else {
            toastr.error(result.message);
            ProcessServerValidation(objForm, result.validation);
            return false;
        };
    });
};

//-------------Added for Select Custome Conrtoller user and baseUrl---------------//
function PostSerializeData(objData, formButton, PostUrl, baseUrl) {
    SpinningIconOnForButton(formButton);

    $.post(PostUrl, objData, function (result) {
        SpinningIconOffForButton(formButton); 
        if (result.success) {
            toastr.success(result.message);
            if (baseUrl != null && baseUrl != "") {
                console.log(baseUrl);
                WaitAndRedirect(baseUrl);
            }
        } else {
            toastr.error(result.message);
            ProcessServerValidation(data, result.validation);
            return false;
        };
    });
};

function UpdateRecord(objForm, formButton, controller, baseUrl) {
    var postUrl = '/' + controller + '/Post';
    var formData = $(objForm).serialize();
    var parentGuid = $(objForm).find('#parentGuid').val();
    if (parentGuid == undefined) {
        parentGuid = "";
    }
    else {
        parentGuid = "index/" + parentGuid;
    }
    SpinningIconOnForButton(formButton);

    $.post(postUrl, formData, function (data) {
        var objJson = jQuery.parseJSON(data);
        if (objJson.success) {
            toastr.success(objJson.message);
            if (baseUrl === null) {
                WaitAndRedirect("/" + controller + "/" + parentGuid + "?st=");
            } else {
                WaitAndRedirect("/" + baseUrl + "/" + controller + "/" + parentGuid + "?st=");
            }
            
        } else {
            SpinningIconOffForButton(formButton);
            toastr.error(objJson.message + '. Please try again!');
            ProcessServerValidation(objForm, objJson.validation);
            return false;
        };
    });
};

function UpdateRecordstats(objForm, controller, baseUrl, id) {
    var postUrl = '/' + controller + '/Post';
    var formData = $(objForm).serialize();
    var parentGuid = $(objForm).find('#' + id).val();
    if (parentGuid == undefined) {
        parentGuid = "";
    }
    else {
        parentGuid = "index/" + parentGuid;
    }

    $.post(postUrl, formData, function (data) {
        var objJson = jQuery.parseJSON(data);
        if (objJson.success) {
            toastr.success(objJson.message);
            if (baseUrl === null) {
                //return true;
                WaitAndRedirect("/" + controller + "/" + parentGuid + "?st=");
            } else {
                WaitAndRedirect("/" + baseUrl + "/" + controller + "/" + parentGuid + "?st=");
            }

        } else {
            toastr.error(objJson.message + '. Please try again!');
            ProcessServerValidation(objForm, objJson.validation);
            return false;
        };
    });
};

function UpdateRecordChangePassword(objForm, formButton, controller, baseUrl) {
    var postUrl = '/' + controller + '/ChangePasswordPost';
    var formData = $(objForm).serialize();
    var parentGuid = $(objForm).find('#parentGuid').val();
    if (parentGuid == undefined) {
        parentGuid = "";
    }
    else {
        parentGuid = "index/" + parentGuid;
    }
    SpinningIconOnForButton(formButton);

    $.post(postUrl, formData, function (data) {
        var objJson = jQuery.parseJSON(data);
        if (objJson.success) {
            toastr.success(objJson.message);
            if (baseUrl === null) {
                WaitAndRedirect("/" + controller + "/" + parentGuid + "?st=");
            } else {
                WaitAndRedirect("/" + baseUrl + "/" + controller + "/" + parentGuid + "?st=");
            }

        } else {
            SpinningIconOffForButton(formButton);
            toastr.error(objJson.message + '. Please try again!');
            ProcessServerValidation(objForm, objJson.validation);
            return false;
        };
    });
};

function ProcessServerValidation(objForm, data) {
    // var validator = $(objForm).validate();
    var obj = "";
    $.each(data, function (i, vObj) {
        obj += vObj.ValidationMessage+"<br />";
        //validator.showErrors(obj);
    });
    Command: toastr["error"](obj)
}

function DeleteRecordRemoveTr(element, controller) {
    var trDelete = $(element).closest('tr');
    
    // Spinning Icon Setup ------------
    var trIcon = $(element).children('i');
    //var trIconClasses = $(trIcon).attr('class');
    SpinningIconOnForRemoveTr(trIcon, trDelete);
    // ---------------------------------

    var guid = $(trDelete).data('id');
    var postUrl = '/' + controller + '/Delete/' + guid;

    var result = confirm("Delete this record?");
    if (result == true) {
       $.post(postUrl, null, function (data) {
            var objJson = jQuery.parseJSON(data);

            if (objJson.success) {
                toastr.success('Record deleted successfully!');
                RemoveTr(trDelete);
            } else {
                SpinningIconOffForRemoveTr(trIcon, trDelete);
                toastr.error('There was an error deleting ' + controller + ' record. Please try again!');
            };

        });
    } else {
        SpinningIconOffForRemoveTr(trIcon, trDelete);
    };
}

function DeleteRecordRemoveTrstats(element, controller, method) {
    var trDelete = $(element).closest('tr');

    // Spinning Icon Setup ------------
    var trIcon = $(element).children('i');
    //var trIconClasses = $(trIcon).attr('class');
    SpinningIconOnForRemoveTr(trIcon, trDelete);
    // ---------------------------------

    var guid = $(trDelete).data('id');
    var postUrl = '/' + controller + '/' + method + '/' + guid;

    var result = confirm("Delete this record?");
    if (result == true) {
        $.post(postUrl, null, function (data) {
            var objJson = jQuery.parseJSON(data);

            if (objJson.success) {
                toastr.success(controller + ' record deleted successfully!');
                RemoveTr(trDelete);
            } else {
                SpinningIconOffForRemoveTr(trIcon, trDelete);
                toastr.error('There was an error deleting ' + controller + ' record. Please try again!');
            };

        });
    } else {
        SpinningIconOffForRemoveTr(trIcon, trDelete);
    };
}

// Added for delete record
function DeleteRecord(controller, guid, kendoGrid) {
    var Grid = $('#' + (kendoGrid == undefined ? "grid" : kendoGrid));
    //$("#iTrash").addClass('danger');
    //$("#iTrash").addClass('fa-spinner fa-spin');

    var result = confirm("Delete this record?");

    if (result == true) {
        $.ajax({
            type: 'Post',
            url: "/" + controller + '/Delete/',
            data: { id: guid },
            success: function (data) {
                if (data.success) {
                    toastr.success(data.message);
                    $(Grid).data('kendoGrid').dataSource.read();
                } else {
                    toastr.error(data.message);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                //$("#iTrash").removeClass('danger');
                //$("#iTrash").removeClass('fa-spinner fa-spin');
                toastr.error('There was an error deleting ' + controller + ' record. Please try again!');
            }
        });
    }
}

function RemoveTr(trDelete) {
    $(trDelete).removeClass().addClass('deleteTr');
    $(trDelete).find("td").removeClass().addClass('deleteTd');

    $(trDelete).fadeOut(1300, function () {
        $(trDelete).remove();
    });
}

function SpinningIconOnForRemoveTr(element, parentContainer) {
    $(parentContainer).addClass('danger');
    // $(element).removeClass();
    $(element).addClass('fa-spinner fa-spin');
}

function SpinningIconOffForRemoveTr(element, parentContainer) {
    $(parentContainer).removeClass('danger');
    $(element).removeClass('fa-spinner fa-spin');
    // $(element).addClass(ogClass);
}

function SpinningIconOnForButton(element) {
    $(element).attr('disabled', true);
    $(element).attr("value", $(element).html())
    $(element).html(CreateSpinningIcon());
}

function SpinningIconOffForButton(element) {
    $(element).attr('disabled', false);
    $(element).html($(element).attr("value"));
    //$(element).html("Submit");
}

function CreateSpinningIcon() {
    var strHtml = "<i class='fa fa-spinner fa-spin'></i> Processing ... ";
    return strHtml;

}

function WaitAndRedirect(url) {
    setTimeout(function () {
        window.location = url;
    }, 1500);
}

function ListBoxMoveItems(selectListBox, targetListBox) {
    var selectedOpts = $(selectListBox).find(':selected');
    $(targetListBox).append($(selectedOpts).clone());
    $(selectedOpts).remove();
    // Reorder Listbox
    SortTexBoxList(selectListBox);
    SortTexBoxList(targetListBox);
}

function SortTexBoxList(texboxList) {
    var $r = $(texboxList).find('option');

    $r.sort(function (a, b) {
        if (a.text < b.text) return -1;
        if (a.text == b.text) return 0;
        return 1;
    });

    $($r).remove();

    $(texboxList).append($($r));
}

function UpdateDraggableSortNicheLayout(nicheItems) {
    var postUrl = "/Niche/UpdateDraggableSortNicheLayout";
    var formData = { itemsList: nicheItems };

    $.post(postUrl, formData, function (data) {
        var objJson = jQuery.parseJSON(data);
        if (objJson.success) {
            // alert(objJson.success);
        } else {
            alert("There was an error .. try again");
        }
    });
}

// ------------------------------------------------------------
// ------------------------------------------------------------