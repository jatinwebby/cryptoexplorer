﻿$(document).ready(function () {

    $(".loadChartsData").click(function () {
        $("#reportrange").show();
        $("#reportrang1").hide();
    });

    $(function () {
        var start = moment().subtract(29, 'days');
        var end = moment();
        function cb(start, end) {
            $('#reportrange span').html(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
        }
        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            opens: 'left',
            //buttonClasses: ['btn btn-default'],
            //applyClass: 'btn-small btn-primary',
            //cancelClass: 'btn-small',
            format: 'DD/MM/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'apply',
                cancelLabel: 'cancel',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom Range',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('day')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }

        }, cb);
        cb(start, end);

        $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
            //do something, like clearing an input
            var startDate = picker.startDate.format('YYYY/MM/DD');
            var endDate = picker.endDate.format('YYYY/MM/DD');
            //2018/02/22
            var check=$("#chartContainer").hasClass('active');
            if (check == true) {
                chartsData(startDate, endDate);
            }            
        });

        var s_date = $('#reportrange').data('daterangepicker').startDate._d;
        var e_date = $('#reportrange').data('daterangepicker').endDate._d;

        var startDate = start;
        var endDate = end;

        var sdate = startDate.format('YYYY/MM/DD');
        var edate = endDate.format('YYYY/MM/DD');

                var check=$("#chartContainer").hasClass('active');
                if (check == true) {
                    chartsData(sdate, edate);
                }
                else {
                    chartsData(sdate, edate);  
                }
        });
    });

function chartsData(sdate, edate) {

        $("#loader_img").show();

        var dataPoints = [];
        var dataPoints1 = [];
        var dataPoints2 = [];
        var guid = $("#txtguid").val();
        var cname = $("#txtCoinName").val();
        var ccode = $("#txtCoinCode").val();

        var chart = new CanvasJS.Chart("renderChart",
            {
                theme: "light2",
                title: {
                    text: cname + "(" + ccode + ") Charts",
                    margin: 50,
                    //MaxWidth: 10,
                    //FontSize: 2,
                    //FontColor: "gray",
                },
                exportEnabled: true,
                axisX: {
                    //labelFormatter: function (e) {
                    //    return CanvasJS.formatDate(e.value, "DD MMM");
                    //},
                    labelAngle: 0
                },
                axisY: [{
                    labelFormatter: function (e) {
                        return "$ " + CanvasJS.formatNumber(e.value);
                        //return "$" + CanvasJS.formatNumber(e.value, "#,#00"); #6d78ad
                    },
                    includeZero: false,
                    title: "USD Price",
                    titleMaxWidth: 200,
                    titleFontSize: 15,
                    lineColor: "#9aa1c6",
                    titleFontColor: "#9aa1c6",
                    labelFontColor: "#9aa1c6",
                    lineThickness: 1
                }],
                axisY2:[{
                    labelFormatter: function (e) {
                        return "$ " + CanvasJS.formatNumber(e.value);
                        //return "$" + CanvasJS.formatNumber(e.value, "#,#00");
                    },
                    includeZero: false,
                    title: "Bitcoin Price",
                    titleMaxWidth: 200,
                    titleFontSize: 15,
                    lineColor: "#51cda0",
                    titleFontColor: "#51cda0",
                    labelFontColor: "#51cda0",
                    lineThickness: 1
                }],
                toolTip: {
                    shared: true
                },
                legend: {
                    cursor: "pointer",
                    itemclick: function (e) {
                        alert("Legend item clicked with type : " + e.dataSeries.type);
                    }
                },
                //legend: {
                //    cursor: "pointer",
                //    //itemclick: toogleDataSeries
                //},
                data: [{
                    //type: "line",
                    //dataPoints: []
                    type: "line",
                    name: cname + " USD Price",
                    showInLegend: true,
                    axisXIndex: 1,
                    xValueFormatString: "DD MMM YYYY hh:mm:ss TT",
                    yValueFormatString: "$ #,##0.########",
                    dataPoints: dataPoints1
                }
                , {
                    type: "line",                   
                    showInLegend: true,
                    axisYType: "secondary",
                    name: "BitCoin USD Price",
                    axisYIndex: 1,
                    yValueFormatString: "$ #,##0.########",
                    dataPoints: dataPoints2
                }]
            });

            // Ajax request for getting JSON data
            //Replace data.php with your JSON API's url

        var url = $('#APIURL').val() + "ICO/GetChartDataByGuidDate?Guid=" + guid + "&startDate=" + sdate + "&endDate=" + edate;

        $.getJSON(url, null, function (result) {
            //console.log(result);
            var len = result.Data.CoinData.length;
            var len1 = result.Data.BTCData.length;

            if (len == 0 && len1 == 0)
            {
                toastr.error("No Data Found for that Range Date.");
                $("#loader_img").hide();
            }
            else {
               
                for (var i = 0; i < len; i++) {                    
                    dataPoints1.push({ x: new Date(result.Data.CoinData[i]["d"]), y: result.Data.CoinData[i]["Price"] });
                }
                chart.options.data[0].dataPoints = dataPoints1;
                for (var i = 0; i < len1; i++) {
                    //var date=new Date(result.Data.BTCData[i]["UpdatedDate"]);
                    dataPoints2.push({ x: new Date(result.Data.BTCData[i]["d"]), y: result.Data.BTCData[i]["Price"] });
                }
                chart.options.data[1].dataPoints = dataPoints2;
                chart.render();
                $("div.canvasjs-chart-toolbar").children().last().append('<div id="exportPDF" class="exportPDF" onclick="" style="padding: 12px 8px; background-color: white; color: black;">Save as PDF</div>');          
                $("#loader_img").hide();
                $("a.canvasjs-chart-credit").attr("href", "http://crypto-explore.com/");
                $("a.canvasjs-chart-credit").text("CryptoExplore");
                var canvas = $("#renderChart .canvasjs-chart-canvas").get(0);
                var dataURL = canvas.toDataURL();
                //console.log(dataURL);

                var div = document.getElementById('exportPDF');
                div.onmouseover = function () {
                    this.style.backgroundColor = '#2196f3d1'
                };
                div.onmouseout = function () {
                    this.style.backgroundColor = 'white'
                };

                //link about jspdf
                //https://micropyramid.com/blog/export-html-web-page-to-pdf-using-jspdf/ 
                $("div.exportPDF").click(function () {
                    //chart.exportChart({ format: "pdf" });'', 'mm', [canvas.width, canvas.height]
                    var pdf = new jsPDF('p', 'pt', 'a4', true);
                    pdf.addImage(dataURL, 'JPEG', 30, 50, 550, 350,'','FAST');
                    pdf.save("download.pdf");
                });

            }

        });
}
$(document).ready(function () {
    $("div.exportPDF").hover("backgroun-color", "blue");
    $(function () {
        $('#a').hover(function () {
            $('#b').css('background-color', 'yellow');
        }, function () {
            // on mouseout, reset the background colour
            $('#b').css('background-color', '');
        });
    });
    $("#a").mouseover(function () {
        $("#b").css("background-color", "red");
    });
});
function savePDF(chart) {
    //document.getElementById("exportPDF").addEventListener("click",function(){
    //var chart = new CanvasJS.Chart("renderChart",sdate = "2018/02/25", edate = "2018/03/26"
    //        {});
    chart.exportChart({ format: "pdf" });
    //});  
}

//Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds())
//Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds())

//var date=new Date(result.Data.CoinData[i]["UpdatedDate"]);
//function convertUTCDateToLocalDate(date) {
//    return new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(),  date.getHours(), date.getMinutes(), date.getSeconds()));
//};
//new Date(result.Data.BTCData[i]["UpdatedDate"]).toLocaleString();
//moment.utc('2018-02-25 22:37:01.987').toDate()