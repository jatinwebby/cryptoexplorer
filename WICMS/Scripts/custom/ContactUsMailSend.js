﻿$(document).ready(function () {
    $("#form-mail").validate({
        rules: {
            FirstName: {
                required: true,
                minlength: 5
            },
            LastName: {
                required: true, minlength: 5
            },
            EmailId: {
                required: true,
                email: true
            },
            PhoneNo: {
                required: true,
                digits: true,
                maxlength: 10,
                minlength: 10
            },
            Message: {
                required: true
            }
        },
        messages: {
            FirstName: {
                required: "Please enter your FirstName.",
                minlength: "First name must be atleaet of 5 characters"
            },
            LastName: {
                required: "Please enter your LastName.",
                minlength: "Last name must be atleaet of 5 characters"
            },
            EmailId: {
                required: "Plaese enter your Email Id.",
                email: "Please enter valid Email Id."
            },
            PhoneNo: {
                required: "Please enter your Contact No.",
                digits: "Please enter only digits.",
                maxlength: "Contact No Must be 10 digits.",
                minlength: "Contact No Must be 10 digits."
            },
            Message: "Please write your Messages."
        }
    });

    $("#form-mail").submit(function () {
        if (!$('#form-mail').valid()) {
            return false;;
        }
        var data = $("#form-mail").serialize();
        var postUrl = "/CONTACT_US/Post";
        $.post(postUrl, data, function (result) {
            if (result.success) {
                toastr.success(result.message);
                window.location.href = "/CONTACT_US/Index";
            }
            else {
                toastr.error(result.message);
                var obj = "";
                $.each(result.validation, function (i, vObj) {
                    obj += vObj.ValidationMessage + "<br />";
                });
                toastr.error(obj);
            }
        });
        return false;
    });
});