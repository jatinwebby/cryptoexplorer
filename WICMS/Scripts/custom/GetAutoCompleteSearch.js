﻿$(document).ready(function () {
    //$("#topline-search").autocomplete({
    //    minLength: 1,
    //    source: function (request, response) {
    //        $.ajax({
    //            url: $('#APIURL').val() + "ICO/GetICOList",
    //            method: "get",
    //            dataType: "json",
    //            success: function (data) {
    //                response(data);                    
    //            },
    //            error: function (err) {
    //                alert(err);
    //            }
    //        });
    //    },        
    //    focus: updateTextbox,
    //    select: updateTextbox
    //})

    var coin_name = $("#topline-search").val();

    //$("#topline-search,#page-search").autocomplete({
    //    source: function (request, response) {
    //        $.ajax({
    //            url: $('#APIURL').val() + "ICO/GetICOList?CoinName=" + request.term,
    //            type: "GET",
    //            dataType: "json",
    //            data: { Prefix: request.term },
    //            success: function (result) {
    //                response($.map(result.Data, function (item) {
    //                    return { label:item.CoinName, value: item.CoinName, id: item.Guid, imgsrc: item.CoinIcon };
    //                    //return $("<li>")
    //                    //       .append("<img class='imageClass' src=" + item.CoinIcon + " alt=" + item.CoinName + "/>")
    //                    //       .append('<a>' + item.CoinName + '</a></li>').appendTo("div.dataList"); 
    //                  // $("#ui-id-1").css("z-index","1002");                     
    //                }))
    //            }
    //        })
    //    },
    //    messages: {
    //        noResults: "No Any Matches found For" + coin_name + ".",
    //        results: ""
    //    },
    //    minLength: 1,
    //    select: function (event, ui) {
    //        $("#topline-search,#page-search").val(ui.item.value);
    //        var url = "/ICO/Details?guid=" + ui.item.id;
    //        //if (window.location == "http://localhost:43995/ico")
    //        //if (window.pathname == "/ico/details") {    
    //        //    var url = "details?guid=" + ui.item.id;
    //        //}
    //        //else {
    //        //    url = "/ico/details?guid=" + ui.item.id;
    //        //}
    //        location.href = url;
    //    }
    //});

    //var coinname = $("#page-search").val();

    $("#topline-search").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: $('#APIURL').val() + "ICO/GetICOList?CoinName=" + request.term,
                dataType: "json",
                data: {
                    term: request.term
                },
                success: function (data) {
                    response($.map(data.Data, function (result) {
                        return {
                            label: " - " + result.CoinName,
                            value: result.CoinName,
                            imgsrc: result.CoinIcon,
                            id:result.Guid
                        }
                    }));
                }
            });
        },
        messages: {
            noResults: "No Any Matches found For" + coin_name + ".",
            results: ""
        },
        minLength: 1,
        select: function (event, ui) {
            $("#topline-search").val(ui.item.value);
            var url = "/ICO/Details?guid=" + ui.item.id;
            location.href = url;
        }
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        return $("<li></li>")
            .data("item.autocomplete", item)
            .append("<a>" + "<img style='width:20px;height:20px' src='" + item.imgsrc + "' /> " + item.label + "</a>")
            .appendTo(ul);
    };

    $("#page-search").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: $('#APIURL').val() + "ICO/GetICOList?CoinName=" + request.term,
                dataType: "json",
                data: {
                    term: request.term
                },
                success: function (data) {
                    response($.map(data.Data, function (result) {
                        return {
                            label: " - " + result.CoinName,
                            value: result.CoinName,
                            imgsrc: result.CoinIcon,
                            id: result.Guid
                        }
                    }));
                }
            });
        },
        messages: {
            noResults: "No Any Matches found For" + coin_name + ".",
            results: ""
        },
        minLength: 1,
        select: function (event, ui) {
            $("#page-search").val(ui.item.value);
            var url = "/ICO/Details?guid=" + ui.item.id;
            location.href = url;
        }
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        return $("<li></li>")
            .data("item.autocomplete", item)
            .append("<a>" + "<img style='width:20px;height:20px' src='" + item.imgsrc + "' /> " + item.label + "</a>")
            .appendTo(ul);
    };

    $("#search_btn").click(function () {
        var search_data = $("#topline-search").val();
        var url = "/ICO/Details?guid=" + search_data;
        location.href = url;
    });

    $("#search_data").click(function () {
        var search_dt = $("#page-search").val();
        var url = "/ICO/Details?guid=" + search_dt;
        location.href = url;
    });

});