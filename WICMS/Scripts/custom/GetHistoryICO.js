﻿$(document).ready(function () {

    //$("#historical-data").tabs({
    //    activate: function (event, ui) {
    //        if (ui.newPanel.selector == "historical-data") {
    //            alert("tab historical-data selected");
    //        }
    //    }
    //});
    //$('.nav li').click(function (e) {
    //    $('.nav li').removeClass('active');
    //    var $this = $(this);
    //    if (!$this.hasClass('active')) {
    //        $this.addClass('active');
    //    }
    //    //e.preventDefault();
    //});

    $("a.loadHistoryData").click(function () {
        $("#reportrange").hide();
        $("#loader_img").hide();
        $("#reportrange1").show();
        var start = moment().subtract(29, 'days');
        var end = moment();
        function cb(start, end) {
            $('#reportrange1 span').html(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
        }
        $('#reportrange1').daterangepicker({
            startDate: start,
            endDate: end,
            opens: 'left',
            format: 'DD/MM/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'apply',
                cancelLabel: 'cancel',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom Range',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('day')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }

        }, cb);
        cb(start, end);

        $('#reportrange1').on('apply.daterangepicker', function (ev, picker) {
            //do something, like clearing an input
            var startDate = picker.startDate.format('DD MMMM YYYY');
            var endDate = picker.endDate.format('DD MMMM YYYY');

            //--------------------------------------
            //var startDate =moment(picker.startDate.format('DD MMMM YYYY'));
            //var endDate = picker.endDate.format('DD MMMM YYYY HH:mm:ss');
            //var time = moment.duration("05:30:00");           
            //var s_date = startDate.subtract(time);
            //var sdate = s_date.format('DD MMMM YYYY HH:mm:ss');
            //--------------------------------------

            var check = $("#historical-data").hasClass('active');
            if (check == true) {
                callDatatable(startDate, endDate);
            }            
        });
       
        var s_date = $('#reportrange').data('daterangepicker').startDate._d;
        var e_date = $('#reportrange').data('daterangepicker').endDate._d;

        var startDate = start;
        var endDate =  end;

        var sdate = startDate.format('DD MMMM YYYY');
        var edate = endDate.format('DD MMMM YYYY');

        //var time = moment.duration("05:30:00");
        //var s_date = startDate.subtract(time);
        //var sdate = s_date.format('DD MMMM YYYY HH:mm:ss');

        var check = $("#historical-data").hasClass('active');

        if (check == false) {
            callDatatable(sdate, edate);
        }

        //--------------------------------------------

        ////var s_date = $('#reportrange').data('daterangepicker').startDate._d;
        ////var e_date = $('#reportrange').data('daterangepicker').endDate._d;
        //var startDate = moment(start);
        //var endDate = moment(end);
        ////var sdate = moment(startDate.format('DD MMMM YYYY'));
        //var edate = moment(endDate.format('DD MMMM YYYY HH:mm:ss'));
        //var time = moment.duration("05:30:00");
        //var s_date = startDate.subtract(time);
        //var sdate = s_date.format('DD MMMM YYYY HH:mm:ss');
        //var check = $("#historical-data").hasClass('active');
        //if (check == false) {
        //    callDatatable(sdate, edate);
        //}

        //---------------------------------------------

        //$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        //    var target = $(e.target).attr("href");
        //    if (target == "#historical-data") {
        //        callDatatable(sdate, edate);
        //    }
        //});

        //$('#historical-data').tabs({
        //    activate: function (event, ui) {
        //        var $activeTab = $('#historical-data').tabs('option', 'active-in');
        //        if ($activeTab == 1) {
        //            // HERE YOU CAN ADD CODE TO RUN WHEN THE SECOND TAB HAS BEEN CLICKED
        //            alert("yes working");
        //            callDatatable(sdate, edate);
        //        }
        //    }
        //    $("a.loadHistoryData").click(function () {
        //        callDatatable(sdate, edate);
        //    });
        //});
        //$('#historical-data').click('tabsselect', function (event, ui) {
        //    var selectedTab = $("#historical-data").tabs('option', 'active');
        //    alert(selectedTab);
        //});        

        //console.log(startDate.format('D MMMM YYYY') + ' - ' + endDate.format('D MMMM YYYY'));  
    });
   
    $('a.descriptionData').click(function () {
        $("#loader_img").hide();
        $("#reportrange").hide();
        $("#reportrange1").hide();
            //var displayResources = $('#display-resources');
            //displayResources.text('Loading data from JSON source...');
        var guid = $('#txtguid').val();
            $.ajax({
                type: "GET",
                url: $("#APIURL").val() + "ICO/GetICODataByGuid?Guid=" + guid,
                success: function (result) {
                    console.log(result.Data);
                    var data = result.Data.Description;
                    $('#info-data').html(data);
                    //$("#commanData").attr('disable',true);
                    //$("#commanData").remove();
                    //var info_area = $('#info-data');
                    // info_area.html(output);
                },
                error:function(result){
                    console.log(result);
                }
            });

        }); 
});

    function callDatatable(startDate, endDate) {
        var guid = $('#txtguid').val();
        var table = $('#tblHistoryData').DataTable({
            "scrollY": "75vh",
            "destroy": true,
            "responsive": true,
            "pageLength": 100,
            "scrollCollapse": true,
            "filter":false,
            "order": [[0, "desc"]],
            "ajax": {
                "url": $('#APIURL').val() + "ICO/GetHistoryByDateRange?Guid=" + guid + "&startDate=" + startDate + "&endDate=" + endDate,
                "dataSrc": function (json) {
                    return json.Data;
                }
            },
            "columns": [
                {
                    "data": "d", title: 'Date', width: "50px", responsivePriority: 1,
                    render: function (value) {
                        // var date_val = new Date(value); AA
                        var localTime = moment(value).format('YYYY-MM-DD HH:mm:ss');
                        return localTime;
                    }
                },
                {
                    "data": "Price", title: 'Price', width: "30px", responsivePriority: 2,
                    render: function (value) {
                       
                        // ----- Formate Strign to Money ----- //
                        // ----eg----. g_oMoneyFormatter.formate(value);
                        var g_oMoneyFormat = new Intl.NumberFormat('en-US', {
                            style: 'currency',
                            currency: 'USD',
                            maximumFractionDigits: 10,
                            minimumFractionDigits: 0,
                            // the default value for minimumFractionDigits depends on the currency
                            // and is usually already 10
                        });

                        var price_val;
                        price_val = g_oMoneyFormat.format(value);

                        if (price_val == "$0") {
                            price_val = "";
                        }
                        return price_val;
                    }
                },
                {
                    "data": "MarketCap", title: 'Market Cap', width: "30px", responsivePriority: 3,
                    render: function (value) {
                        return value;
                    }
                },
                {
                    "data": "CirculatingSupply", title: 'Circulating Supply', width: "30px", responsivePriority: 4,
                    render: function (value) {
                        return value;
                    }
                },
                {
                    "data": "MaxSupply", title: 'Max Supply', width: "30px", responsivePriority: 5,
                    render: function (value) {
                        return value;
                    }
                }
            ]
        });
    }