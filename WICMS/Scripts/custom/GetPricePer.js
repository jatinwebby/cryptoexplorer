﻿$(document).ready(function () {
    
    setTimeout(function () {
        cal_PricePer();
    }, 2000);

});

function cal_PricePer(){
    var guid = $('#txtguid').val();
    var price = $("#txtPrice").val();
    $.ajax({
        type: "GET",
        url: $("#APIURL").val() + "ICO/GetHistoryByGuid?Guid=" + guid,
        success: function (result) {
            // info_area.html(output);
            var realData = result.Data.length - 1;
            var res = result.Data[result.Data.length - 2].Price;
            var diff = res - price;
            var mult = diff * 100;
            var per;
            if (mult == 0) {
                per = 0;
            } else {
                per = mult / price;
            }
            //var r = Math.round(per, 2);
            //round2Fixed(10.8034); // Returns "10.80"
            //round2Fixed(10.8);    // Returns "10.80"
            var rr = per.toFixed(2);
            $("span.ChkPrice").text(rr);
            if (res >= price) {
                $('span.text-large2').css('color', '#16b918');
            } else {
                $('span.text-large2').css('color', '#ff0000');
            }

        },
        error: function (result) {
            console.log(result);
            toastr.error("Data not Found");
        }
    });
}