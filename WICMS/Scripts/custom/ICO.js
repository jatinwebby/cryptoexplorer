﻿$(document).ready(function () {
    var l_Categories = null;
    // ----- for home page to take top 5 ICO ----- //
    if ($('#datatable_api_Category_Type').length > 0) {
        l_Categories = $('#datatable_api_Category_Type').val();
    }
    var top = 0;
    var table = $('#tblICO').DataTable({
        "scrollY": "90vh",        
        responsive: true,
        "pageLength": 100,
        "scrollCollapse": true,
        //"paging": false,
        //"bInfo": false,
        //"searching": false,
        //"dom": '<"toolbar">frtip',
        "order": [[1, "desc"]],
        "ajax": {
            "url": $('#APIURL').val() + "ICO/GetList?Categories=" + l_Categories + ",Top=" + top,
            "dataSrc": function (json) {
                return json.Data;
            }
        },
        "initComplete": function (settings, json) {
            populateAllClock(json.Data)
        },
        "createdRow": function (row, data, dataIndex) {
            if (data.Stage == 'ICO TODAY') {
                $(row).css('background-color','aquamarine');
            }
        },

        "columns": [
            {
                "data": "CoinName", title: 'Coin Name', width: "260px", responsivePriority: 1,
                render: function (data, type, row, meta) {
                    if (type === 'display') {
                        data = '<img src="' + row.CoinIcon + '" onerror="this.src=\'/images/no-image-found.jpg\'" height="25" width="25" /> <a href="' + (row.Website == null ? '' : row.Website) + '" ' + (row.ReferrerUrl == null ? '' : ' class="linkRefurl" data-refurl="' + row.ReferrerUrl + '"') + ' >' + data + '</a> <a href="/ICO/Details?guid=' + (row.Guid == null ? '' : row.Guid) + '"> <i class="glyphicon glyphicon-plus-sign"></i></a>';
                    }
                    return data;
                }
            },
            {
                "data": "Rate", title: 'Rate/5', width: "30px", responsivePriority: 3, className: "text-right",
                render: function (value) {
                    return setRate(value);
                }
            },
            {
                "data": "Hype", title: 'Hype', width: "30px", responsivePriority: 4,
                render: function (value) {
                    return setLevel(value);
                }
            },
            {
                "data": "Scam", title: 'Scam', width: "30px", responsivePriority: 5, render: function (value) {
                    return setLevelReverse(value);
                }
            },
            {
                "data": "Moon", title: 'Moon', width: "30px", responsivePriority: 6, render: function (value) {
                    return setLevel(value);
                }
            },
            {
                "data": "Start", title: 'Start', width: "90px", responsivePriority: 9, "render": function (value) {
                    return RenderDate(value);
                }, className: "text-right"
            },
            {
                "data": "ICOEnd", title: 'ICO End', width: "90px", responsivePriority: 10, "render": function (value) {
                    return RenderDate(value);
                }, className: "text-right"
            },
            {
                "data": "Stage", title: 'Stage', width: "70px", responsivePriority: 7,
                render: function (value) {
                    return shortStage(value);
                }
            },
            {
                "data": "Price", title: 'Price', className: "text-right", responsivePriority: 8,
                render: function (value) {
                    var price_val;
                    price_val= g_oMoneyFormatter.format(value);
                    if (price_val == "$0") {
                        price_val = "";
                    }
                    return price_val;
                }
            },
            {
                "data": "ID", title: 'NextRound',width: "220px", responsivePriority: 2,
                "render": function (data, type, row, meta) {
                    var timer = "<p><div class='colNextRound' id='clock_" + data + "' title='" + RenderDate(row.NextRound, true) + "'></div></p>";
                    // Create our number formatter. 
                    return timer;
                }
            } 
        ] 
    });

   $.fn.dataTable.ext.search.push(
       function (settings, data, dataIndex) {
           var searchStage = $('#searchStage').val();
           var stage = data["7"]; // use data for the stage column
           if (searchStage == undefined || searchStage == "" || searchStage == stage) {
               return true;
           }
           return false;
       }
    );

    $(".dataTables_length").html('<label>Stage:<select id="searchStage" class="" aria-controls="tblICO"><option value="">All</option><option value="EXC">EXCHANGE</option><option value="IT">ICO TODAY</option><option value="END">ENDED</option><option value="SM">SCAM</option></select></label>');
      
    $('#searchStage').change(function () {
        table.draw();
    });
  
});

function RenderDate(value,isShowTime) {
    if (value === null) return "";
    if (isShowTime)
        return moment(value).format('D-M-YY hh:mm');
    else
        return moment(value).format('D-M-YY');
}

function populateClock(ID, NextRound) {
    
    if (NextRound == null)
        return;
    var now = moment(new Date()); //todays date
    var end = moment(NextRound); // another date
    var duration = moment.duration(end.diff(now));
    if (duration <= 0)
        return;
    $('#clock_' + ID).FlipClock(duration.asSeconds(), {
        clockFace: 'DailyCounter',
        countdown: true,
        autoStart: true
    });
}

function populateAllClock(data) {
    $.each(data, function (index, item) {
        if (item.NextRound == null)
            return;
        var now = moment(new Date()); //todays date
        var end = moment(item.NextRound); // another date
        var duration = moment.duration(end.diff(now));
        if (duration <= 0)
            return;
        $('#clock_' + item.ID).FlipClock(duration.asSeconds(), {
            clockFace: 'DailyCounter',
            countdown: true,
            autoStart: true
        });
    });
}

function setLevel(val) {
    switch (val) {
        case "VERY LOW":
            return "<b style='color:#991425' title='Very Low'>VL</b>";
        case "LOW":
            return "<b style='color:#ef4136' title='Low'>L</b>";
        case "MID":
            return "<b style='color:#e69138' title='Midiam'>MID</b>";
        case "HIGH":
            return "<b style='color:#6aa84f' title='High'>H</b>";
        case "VERY HIGH":
            return "<b style='color:#38761d' title='Very High'>VH</b>";
        case "SCAM":
            return "<b style='color:#991425' title='Scam'>SM</b>";
        default:
            return "<b title='Midiam'>" + val + "</b>";
    }
}

function setLevelReverse(val) {
    switch (val) {
        case "VERY LOW":
            return "<b style='color:#38761d' title='Very Low'>VL</b>";
        case "LOW":
            return "<b style='color:#6aa84f' title='Low'>L</b>";
        case "MID":
            return "<b style='color:#e69138' title='Midiam'>MID</b>";
        case "HIGH":
            return "<b style='color:#ef4136' title='High'>H</b>";
        case "VERY HIGH":
            return "<b style='color:#991425' title='Very High'>VH</b>";
        case "SCAM":
            return "<b style='color:#991425' title='Scam'>SM</b>";
        default:
            return "<b class='text-default' title='Midiam'>" + val + "</b>";
    }
}

function setRate(rate) {
    if (rate >= 4)
        return "<b style='color:#38761d'>" + rate + "</b>";
    else if (rate >= 3)
        return "<b style='color:#6aa84f'>" + rate + "</b>";
    else if (rate >= 2)
        return "<b style='color:#e69138'>" + rate + "</b>";
    else if (rate >= 1)
        return "<b style='color:#ef4136'>" + rate + "</b>";
    else
        return "<b style='color:#991425'>" + (rate == null ? 0 :rate)+ "</b>";
}

function shortStage(stage) {
    switch (stage) {
        case 'EXCHANGE':
            return "<b title='Exchange'>EXC</b>";
        case 'ICO':
            return "<b title='ICO'>ICO</b>";
        case 'ICO TODAY':
            return "<b title='ICO Today'>IT</b>";
        case 'ENDED':
            return "<b title='Ended'>END</b>";
        case 'SCAM':
            return "<b title='Scam'>SCM</b>";
        case 'LENDING':
            return "<b title='Lending'>LND</b>";
        case 'CLOSED':
            return "<b title='Close'>CLS</b>";
        default:
            return "<b title=''>"+stage+"</b>";
    }
}

function convertUTCDateToLocalDate(date) {
    var newDate = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);

    var offset = date.getTimezoneOffset() / 60;
    var hours = date.getHours();

    newDate.setHours(hours - offset);

    return newDate;   
}

//var date = convertUTCDateToLocalDate(new Date(date_string_you_received));
//date.toLocaleString();
//var timer = "<p><div class='colNextRound' id='clock_" + data + "' title='" + RenderDate(convertUTCDateToLocalDate(new Date(row.NextRound)), true) + "'></div></p>";
