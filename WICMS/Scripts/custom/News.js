﻿$(document).ready(function () {

    loadNews();

    $(function () {
        var start = moment().subtract(29, 'days');
        var end = moment();
        function cb(start, end) {
            $('#daterange span').html(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
        }
        $('#daterange').daterangepicker({
            startDate: start,
            endDate: end,
            opens: 'left',
            format: 'DD/MM/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'apply',
                cancelLabel: 'cancel',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom Range',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                //'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('day')]
                //'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }

        }, cb);
        cb(start, end);

        $('#daterange').on('apply.daterangepicker', function (ev, picker) {
            //do something, like clearing an input
            var fromDate = picker.startDate.format('DD MMM YYYY');
            var toDate = picker.endDate.format('DD MMM YYYY');
            var exist_news_list = $("#news_list div").hasClass("isotope-item col-lg-4 col-md-6 col-sm-12");

            if (exist_news_list == true) {
                //$("div.isotope-item").remove();
                $("div").remove(".isotope-item");
                NewsList(fromDate, toDate);
            }
            else {
                NewsList(fromDate, toDate);
            }
        });

        var s_date = $('#daterange').data('daterangepicker').startDate._d;
        var e_date = $('#daterange').data('daterangepicker').endDate._d;
        var startDate = start;
        var endDate = end;
        var fromDate = startDate.format('DD MMM YYYY');
        var toDate = endDate.format('DD MMM YYYY');
      

        setTimeout(function () {
            var check_exist = $("#news_list div").hasClass("isotope-item");
            if (check_exist == false) {
                NewsList(fromDate, toDate);
            } 
        },2000);               
                 
    });
});
function hideLink(a) {
    event.preventDefault(); /* prevent the a from changing the url */
    $(a).hide(); /* hide the read more button */
    $(a).parents('.short_descr').find('.more_text').show(); /* show the .more_text span */
}
function NewsList(fromDate, toDate) {
    //Set FadeIn for Progressive Div
    $("#ProgressiveDiv").fadeIn();
$.ajax({
    type: "GET",
    url: $("#APIURL").val() + "News/GetSelDateRangeNewsList?fromDate=" + fromDate + "&toDate=" + toDate,
    datatype: "json",
    success: function (result) { 
        //toastr.success(result.Data);
        //console.log(result.Data);
        
        if (result.Data == null || result.Data.length == 0) {
            //toastr.error("No News found for that Date Range.");
            //$("#lblMessage").css(disabled,false);
            $("#lblMessage").prop('disabled', false);
            $("#NotFoundMessage").css('display', '');
            $("#lblMessage").text("News not available for that Date Range.");
            //loadNews();
        } else {
            $("#NotFoundMessage").css('display', 'none');
            var l = result.Data.length;
            for (i = 0; i <= l; i++) {
                //var date = new Date(result.Data[i]["StartEventDate"]).getDate();
                //var month = new Date(result.Data[i]["StartEventDate"]).getMonth() + 1;
                //var year = new Date(result.Data[i]["StartEventDate"]).getFullYear();

                var date = new Date(moment.utc(result.Data[i]["StartEventDate"]).toDate()).getDate();
                var month = new Date(moment.utc(result.Data[i]["StartEventDate"]).toDate()).getMonth() + 1;
                var year = new Date(moment.utc(result.Data[i]["StartEventDate"]).toDate()).getFullYear();
                var date_full = date + "/" + month + "/" + year;

                $("<div class='isotope-item col-lg-4 col-md-6 col-sm-12' style='position: relative'><article class='post vertical-item content-padding big-padding with_border text-center'> <div class='item-media with_icon'><a href='/News/Details?guid=" + result.Data[i]["Guid"] + "'><img src='" + result.Data[i]["HeaderImage"] + "' alt='' style='min-height:280px;'/></a><div class='post_icon'> <i class='rt-icon2-pen'></i>' </div></div><div class='item-content'><h4 class='entry-title' style='font-size:15px;font-style: NORMAL;'> <a href='/News/Details?guid=" + result.Data[i]["Guid"] + "' class='content-3lines-ellipsis' data-trigger='hover' data-toggle='popover' data-content='" + result.Data[i]["Title"] + "'>" + result.Data[i]["Title"] + "</a> </h4><a href='/News/Details?guid=" + result.Data[i]["Guid"] + "' data-toggle='popover' class='content-3lines-ellipsis short_descr' style='font-size:17px;font-style: NORMAL;' data-trigger='hover' data-content='" + result.Data[i]["ShortDescription"] + "'>" + result.Data[i]["ShortDescription"] + "</a></div><footer class='item-meta small-text darklinks'><div><span class='bg_text'></span> <a href='/News/Details?guid=" + result.Data[i]["Guid"] + "'><time datetime='" + result.Data[i]["StartEventDate"] + "'>" + date_full + "</time></a></div><div><span class='bg_text'></span> <a href='/News/Details?guid=" + result.Data[i]["Guid"] + "'>" + result.Data[i]["Author"] + "</a></div><div><span class='bg_text'></span> <a href='/News/Details?guid=" + result.Data[i]["Guid"] + "'>MORE</a></div></footer></article></div>").appendTo("div#news_list");          
            }
        }
        //set fadeOut for ProgressiveDiv
        $("#ProgressiveDiv").fadeOut(500);
    },
    error: function (result) {
        toastr.error("Error occured.");
        console.log(result.Data);
    }
});
}


function loadNews() {
    $.ajax({
        type: "GET",
        url: $("#APIURL").val() + "News/GetTodayRangeNewsList",
        datatype: "json",
        success: function (result) {
            //toastr.success(result.Data);   //new Date(result.Data[i]["CreatedDate"])
            
            if (result.Data == null || result.Data.length == 0) {
                //toastr.error("News not available for Today.");
                //$("#lblMessage").css('disabled', false);
                //$("#lblMessage").css("display", "none");
                $("#lblMessage").prop('disabled', false);
                $("#NotFoundMessage").css('display', '');
                $("#lblMessage").text("News not available for Today.");
            }
            else {
                $("#NotFoundMessage").css('display', 'none');
                
                var l = result.Data.length;
                for (i = 0; i <= l; i++) {

                    //$('.short_descr').each(function (event) { /* select all divs with the item class */
                    //if (result.Data[i]["ShortDescription"] != undefined) {
                    //    var max_length = 50; /* set the max content length before a read more link will be added */
                    //    if (result.Data[i]["ShortDescription"].length > max_length) { /* check for content length */
                    //        var short_content = result.Data[i]["ShortDescription"].substr(0, max_length); /* split the content in two parts */
                    //        var long_content = result.Data[i]["ShortDescription"].substr(max_length);
                    //        var short_desrcription=short_content +
                    //                     '<a href="#" class="read_more" onclick="hideLink(this)"><br/>Read More</a>' +
                    //                     '<span class="more_text" style="display:none;">' + long_content + '</span>'; /* Alter the html to allow the read more functionality */
                    //    }                    
                    //}

                    //var date = new Date(result.Data[i]["StartEventDate"]).getDate();
                    var date = new Date(moment.utc(result.Data[i]["StartEventDate"]).toDate()).getDate();
                    var month = new Date(moment.utc(result.Data[i]["StartEventDate"]).toDate()).getMonth() + 1;
                    var year = new Date(moment.utc(result.Data[i]["StartEventDate"]).toDate()).getFullYear();
                    var date_full = date + "/" + month + "/" + year;

                    $("<div class='isotope-item col-lg-4 col-md-6 col-sm-12' style='position: relative'><article class='post vertical-item content-padding big-padding with_border text-center'> <div class='item-media with_icon'><a href='/News/Details?guid=" + result.Data[i]["Guid"] + "'><img src='" + result.Data[i]["HeaderImage"] + "' alt='' style='min-height:280px;'/></a><div class='post_icon'> <i class='rt-icon2-pen'></i>' </div></div><div class='item-content'><h4 class='entry-title' style='font-size:15px;font-style: NORMAL;'> <a href='/News/Details?guid=" + result.Data[i]["Guid"] + "' class='content-3lines-ellipsis' data-trigger='hover' data-toggle='popover' data-content='" + result.Data[i]["Title"] + "'>" + result.Data[i]["Title"] + "</a> </h4><a href='/News/Details?guid=" + result.Data[i]["Guid"] + "' data-toggle='popover' class='content-3lines-ellipsis short_descr' style='font-size:17px;font-style: NORMAL;' data-trigger='hover' data-content='" + result.Data[i]["ShortDescription"] + "'>" + result.Data[i]["ShortDescription"] + "</a></div><footer class='item-meta small-text darklinks'><div><span class='bg_text'></span> <a href='/News/Details?guid=" + result.Data[i]["Guid"] + "'><time datetime=''>" + date_full + "</time></a></div><div><span class='bg_text'></span> <a href='/News/Details?guid=" + result.Data[i]["Guid"] + "'>" + result.Data[i]["Author"] + "</a></div><div><span class='bg_text'></span> <a href='/News/Details?guid=" + result.Data[i]["Guid"] + "'>MORE</a></div></footer></article></div>").appendTo("div#news_list");

                    //$('.short_descr').each(function (event) { /* select all divs with the item class */
                    //if ($('.short_descr')[i] != undefined) {
                    //    var max_length = 20; /* set the max content length before a read more link will be added */
                    //    if ($('.short_descr')[i].outerText.length > max_length) { /* check for content length */
                    //        var short_content = $('.short_descr')[i].outerText.substr(0, max_length); /* split the content in two parts */
                    //        var long_content = $('.short_descr')[i].outerText.substr(max_length);
                    //        $('.short_descr').html(short_content +
                    //                     '<a href="#" class="read_more"><br/>Read More</a>' +
                    //                     '<span class="more_text" style="display:none;">' + long_content + '</span>'); /* Alter the html to allow the read more functionality */
                    //    }
                    //});
                    //}             
                }
                }
        },
        error: function (result) {
            toastr.error(result.Data);
        }
    });

}//style = 'position: absolute; left: 0%; top: 0px;'
//$("<div class='isotope-item col-lg-4 col-md-6 col-sm-12' style='position: relative'><article class='post vertical-item content-padding big-padding with_border text-center'> <div class='item-media with_icon'><img src='" + result.Data[i]["HeaderImage"] + "' alt='' style='min-height:280px;'><div class='post_icon'> <i class='rt-icon2-pen'></i>' </div></div><div class='item-content'><h4 class='entry-title' style='font-size:15px;font-style: NORMAL;'> <a href='' class='content-3lines-ellipsis'>" + result.Data[i]["Title"] + "</a> </h4><a href='#' data-toggle='popover' title='Short Description' data-content='" + result.Data[i]["ShortDescription"] + "'>Toggle popover</a><p class='content-3lines-ellipsis short_descr' style='font-size:17px;font-style: NORMAL;'>" + result.Data[i]["ShortDescription"] + "</p></div><footer class='item-meta small-text darklinks'><div><span class='bg_text'>on</span> <a href=''><time datetime=''>" + date_full + "</time></a></div><div><span class='bg_text'>by</span> <a href=''>" + result.Data[i]["Author"] + "</a></div><div><span class='bg_text'>in</span> <a href='/News/Details?guid=" + result.Data[i]["Guid"] + "'>NEWS</a></div></footer></article></div>").appendTo("div#news_list");
//$("<div class='isotope-item col-lg-4 col-md-6 col-sm-12' style='position: relative'><article class='post vertical-item content-padding big-padding with_border text-center'> <div class='item-media with_icon'><a href='/News/Details?guid=" + result.Data[i]["Guid"] + "'><img src='" + result.Data[i]["HeaderImage"] + "' alt='' style='min-height:280px;'/></a><div class='post_icon'> <i class='rt-icon2-pen'></i>' </div></div><div class='item-content'><h4 class='entry-title' style='font-size:15px;font-style: NORMAL;'> <a href='/News/Details?guid=" + result.Data[i]["Guid"] + "' class='content-3lines-ellipsis' data-trigger='hover' data-toggle='popover' data-content='" + result.Data[i]["Title"] + "'>" + result.Data[i]["Title"] + "</a> </h4><a href='/News/Details?guid=" + result.Data[i]["Guid"] + "' data-toggle='popover' class='content-3lines-ellipsis short_descr' style='font-size:17px;font-style: NORMAL;' data-trigger='hover' data-content='" + result.Data[i]["ShortDescription"] + "'>" + result.Data[i]["ShortDescription"] + "</a></div><footer class='item-meta small-text darklinks'><div><span class='bg_text'>on</span> <a href='/News/Details?guid=" + result.Data[i]["Guid"] + "'><time datetime=''>" + date_full + "</time></a></div><div><span class='bg_text'>by</span> <a href='/News/Details?guid=" + result.Data[i]["Guid"] + "'>" + result.Data[i]["Author"] + "</a></div><div><span class='bg_text'>in</span> <a href='/News/Details?guid=" + result.Data[i]["Guid"] + "'>MORE</a></div></footer></article></div>").appendTo("div#news_list");
//$("<div class='isotope-item col-lg-4 col-md-6 col-sm-12' style='position: relative'><a rticle class='post vertical-item content-padding big-padding with_border text-center'> <div class='item-media with_icon'><img src='" + result.Data[i]["HeaderImage"] + "' alt='' class='rounded'><div class='post_icon'> <i class='rt-icon2-pen'></i>' </div></div><div class='item-content'><h4 class='entry-title'> <a href=''>" + result.Data[i]["Title"] + "</a> </h4><p class='content-2lines-ellipsis'>" + result.Data[i]["ShortDescription"] + "</p></div><footer class='item-meta small-text darklinks'><div><span class='bg_text'>on</span> <a href=''><time datetime=''>" + date_full + "</time></a></div><div><span class='bg_text'>by</span> <a href=''>" + result.Data[i]["Author"] + "</a></div><div><span class='bg_text'>in</span> <a href=''>news</a></div></footer></article></div>").appendTo("div#news_list");