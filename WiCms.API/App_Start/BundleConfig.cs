﻿using System.Web;
using System.Web.Optimization;

namespace WiCms.API
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/Scripts/Header").Include(
                     "~/Scripts/modernizr-2.6.2.min.js",
                     "~/Scripts/jquery-1.10.2.min.js",
                     "~/Scripts/moment.js"
                     ));

            bundles.Add(new ScriptBundle("~/Scripts/Footer").Include(
                      "~/Scripts/compressed.js",
                      "~/Scripts/main.js",
                      //"~/Scripts/switcher.js",
                      "~/Scripts/toastr.js",
                      "~/Scripts/jquery.validate.min.js"
                       ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                    "~/Content/bootstrap.min.css",
                    "~/Content/animations.css",
                    "~/Content/fonts.css",
                    "~/Content/toastr.css",
                    "~/Content/main.css",
                    "~/Content/shop.css"));

            BundleTable.EnableOptimizations = false;
        }
    }
}
