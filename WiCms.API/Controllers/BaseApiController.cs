
// creating webapi controller
using System;
using System.Web.Mvc;
using BLL.ModelViews;
//using AutoMapper;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Configuration;
using System.Web.Http;
using BLL.ViewModel;
using static WiCms.API.Helper.Enum;
using WiCms.API.Helper;
using System.Web;
using BLL.Tools;

namespace WiCms.API.Controllers
{
  
    public class BaseApiController : ApiController
    {
        public BaseApiController()
        {
            var name = 0l;
            if (System.Web.HttpContext.Current.User != null)
            {
                var dart = System.Web.HttpContext.Current.GetOwinContext();//.GetUserManager<ApplicationUserManager>(); 
                //new System.Web.Routing.RequestContext .Identity.GetUserId();
                name = BLL.Queries.User.GetIDByUsername(System.Web.HttpContext.Current.User.Identity.Name);
            }
        }
        public HttpResponseMessage ReturnExceptionResponse(Exception ex)
        {
            //WebApiApplication.logger.Error(ex.Message, ex);
            APIResponse response = new APIResponse(APIStatus.Failure.ToString(), ex == null ? "" : ex.Message);
            return new HttpResponseMessage()
            {
                //StatusCode = HttpStatusCode.OK,
                Content = new ObjectContent<object>(response, HelperUtility.GetFormatter(Request.RequestUri, Configuration), HelperUtility.GetMediaType(Request.RequestUri))
            };
        }

        public HttpResponseMessage ReturnResult(APIStatus apiStatus, string Message, object data = null) //HttpStatusCode StatusCode
        {
            APIResponse response = new APIResponse(apiStatus.ToString(), Message, data);

            return new HttpResponseMessage()
            {
                Content = new ObjectContent<APIResponse>(response, HelperUtility.GetFormatter(Request.RequestUri, Configuration), HelperUtility.GetMediaType(Request.RequestUri))
            };
        }
    }
}
