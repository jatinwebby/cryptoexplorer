using System;
using System.Web.Mvc;
using BLL.ModelViews;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Configuration;
using static WiCms.API.Helper.Enum;
using BLL.Tools;
//using System.Web.Http;

namespace WiCms.API.Controllers
{
    [RoutePrefix("api/CMSManage")]
    public class CMSManageController : BaseApiController
    {
	    [Route("GetList")]
        [HttpPost]
		public HttpResponseMessage GetList(BLL.ModelViews.CMSManage.Extend collection)
        {
		    try
            {
			     var resultdata=BLL.Queries.CMSManage.GetAll();
			     return ReturnResult(APIStatus.Success,  string.Empty, resultdata);
			}
			catch(Exception ex){
                Guid Errorguid = CommonFunction.LogManager("API/CMSManageController/GetList()", ex, "Exception Caught");
                return ReturnExceptionResponse(ex);
			}		
		}

		[Route("AddEditDetail")]
        [HttpPost]
		public HttpResponseMessage AddEditDetail(BLL.ModelViews.CMSManage.Extend collection)
        {
		     BLL.ModelViews.CMSManage.Extend  o_response = new BLL.ModelViews.CMSManage.Extend();
             string ResponseMessage = string.Empty;
		    try
            {
			    if (collection.Id != 0)
                {
				    o_response = BLL.Commands.CMSManage.Update(collection);
                    ResponseMessage = "Details Updated Successfully!";
				}
				else
				{
				    o_response = BLL.Commands.CMSManage.Create(collection);
                    ResponseMessage = "Details are added Successfully!";
				}
			     
			     return ReturnResult(APIStatus.Success,  ResponseMessage, o_response);
			}
			catch(Exception ex){
                Guid Errorguid = CommonFunction.LogManager("API/CMSManageController/AddEditDetail()", ex, "Exception Caught");
                return ReturnExceptionResponse(ex);
			}
		
		}
      
	    [Route("GetSingleData")]
        public HttpResponseMessage GetSingleData(string ID) //PageName
        {
            try
            {
                var o_response = BLL.Queries.CMSManage.GetByPageName(ID);

                if (o_response != null)
                    return ReturnResult(APIStatus.Success, string.Empty, o_response);
                else
                    return ReturnResult(APIStatus.NotFound,  string.Empty, null);
            }
            catch (Exception ex)
            {
                Guid Errorguid = CommonFunction.LogManager("API/CMSManageController/GetSingleData()", ex, "Exception Caught");
                return ReturnExceptionResponse(ex);
            }
        }

        [Route("GetRandomAds")]
        public HttpResponseMessage GetRandomAds(string ID) //PageName
        {
            try
            {
                var o_response = BLL.Queries.CMSManage.GetRandomByPageName(ID);
                if (o_response != null)
                    return ReturnResult(APIStatus.Success, string.Empty, o_response);
                else
                    return ReturnResult(APIStatus.NotFound,  string.Empty, null);
            }
            catch (Exception ex)
            {
                Guid Errorguid = CommonFunction.LogManager("API/CMSManageController/GetRandomAds()", ex, "Exception Caught");
                return ReturnExceptionResponse(ex);
            }
        }

        [Route("DeleteRecord")]
        [HttpGet]
        public HttpResponseMessage DeleteRecord(Guid ID)
        {
            try
            {
                var o_response = BLL.Commands.CMSManage.Delete(ID);

                if (o_response == true)
                    return ReturnResult( APIStatus.Success, string.Empty, null);
                else                     
                    return ReturnResult(APIStatus.NotFound, string.Empty, null);
            }
            catch (Exception ex)
            {
                Guid Errorguid = CommonFunction.LogManager("API/CMSManageController/DeleteRecord()", ex, "Exception Caught");
                return ReturnExceptionResponse(ex);
            }
        }
    }
}
