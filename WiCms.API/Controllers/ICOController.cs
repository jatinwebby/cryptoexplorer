using System;
using System.Web.Mvc;
using BLL.ModelViews;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Configuration;
//using System.Web.Http;
using static WiCms.API.Helper.Enum;
using BLL.Tools;

namespace WiCms.API.Controllers
{
    [RoutePrefix("api/ICO")]
    public class ICOController : BaseApiController
    {
       
        [Route("GetList")]
		public HttpResponseMessage GetList(string Categories = "", int Top = 0)
        {
		    try
            {
               if(Top == 5) { 
			     var resultdata=BLL.Queries.ICO.GetGridData(Categories).Select(s=> new { s.ID,s.Guid, s.CoinIcon, s.CoinCode,s.CoinName,s.Website,s.ReferrerUrl,s.Rate,s.Hype,s.Scam,s.Moon,s.Start,s.ICOEnd,s.Stage,s.Price,s.NextRound}).Take(5);
			     return ReturnResult(APIStatus.Success,  string.Empty, resultdata);
                }
               else
                {
                    var resultdata = BLL.Queries.ICO.GetGridData(Categories).Select(s => new { s.ID, s.Guid, s.CoinIcon, s.CoinCode, s.CoinName, s.Website, s.ReferrerUrl, s.Rate, s.Hype, s.Scam, s.Moon, s.Start, s.ICOEnd, s.Stage, s.Price, s.NextRound });
                    return ReturnResult(APIStatus.Success, string.Empty, resultdata);
                }
            }
			catch(Exception ex){
                Guid Errorguid = CommonFunction.LogManager("API/ICOController/GetList()", ex, "Exception Caught.");
                return ReturnExceptionResponse(ex);
			}
		
		}

        [Route("GetICOList")]
        public HttpResponseMessage GetICOList(string CoinName)
        { 
            try
            {
                var resultdata = BLL.Queries.ICO.GetAllList(CoinName).Select(s => new { s.Guid,s.CoinIcon, s.CoinName});
                return ReturnResult(APIStatus.Success, string.Empty, resultdata);

            }
            catch (Exception ex)
            {
                Guid Errorguid = CommonFunction.LogManager("API/ICOController/GetICOList()", ex, "Exception Caught.");
                return ReturnExceptionResponse(ex);
            }
        }

        [Route("GetTopList")]
        public HttpResponseMessage GetTopList(string Categories = "", int top=0)
        {
            try
            {
                var resultdata = BLL.Queries.ICO.GetGridData(Categories).Select(s => new { s.ID, s.Guid, s.CoinIcon, s.CoinCode, s.CoinName, s.Website, s.ReferrerUrl, s.Rate, s.Hype, s.Scam, s.Moon, s.Start, s.ICOEnd, s.Stage, s.Price, s.NextRound }).Take(5);
                return ReturnResult(APIStatus.Success, string.Empty, resultdata);
            
            }
            catch (Exception ex)
            {
                Guid Errorguid = CommonFunction.LogManager("API/ICOController/GetTopList()", ex, "Exception Caught.");
                return ReturnExceptionResponse(ex);
            }

        }


        [Route("GetRecommendCoinList")]
        public HttpResponseMessage GetRecommendCoinList()
        {
            try
            {
                var resultdata = BLL.Queries.ICO.GetRecommandCoin().Select(s => new { s.Guid, s.CoinIcon, s.CoinCode, s.CoinName , s.Price });
                return ReturnResult(APIStatus.Success, string.Empty, resultdata);

            }
            catch (Exception ex)
            {
                Guid Errorguid = CommonFunction.LogManager("API/ICOController/GetRecommendCoinList()", ex, "Exception Caught.");
                return ReturnExceptionResponse(ex);
            }
        }

        [Route("GetChartsDateWiseData")]
        public HttpResponseMessage GetChartsDateWiseData()
        {
            try
            {
                var resultdata = BLL.Queries.ICO.GetChartsData().Select(s => new { s.Price, s.UpdatedDate });
                return ReturnResult(APIStatus.Success, string.Empty, resultdata);

            }
            catch (Exception ex)
            {
                Guid Errorguid = CommonFunction.LogManager("API/ICOController/GetChartsDateWiseData()", ex, "Exception Caught.");
                return ReturnExceptionResponse(ex);
            }
        }

        [Route("GetChartDataByGuidDate")]
        public HttpResponseMessage GetChartDataByGuidDate(Guid guid,string startDate, string endDate)
        {
            try
            {
                //var resultdata = BLL.Queries.ICO.GetByGuid(guid);
                //var CoinData = BLL.Queries.ICO.GetUpDateById(guid).Select(x=> new { x.UpdatedDate, x.Price});
                var CoinData = BLL.Queries.ICO.GetUpDateById(guid, startDate, endDate).Select(x => new { d=x.UpdatedDate.ToLocalTime(), x.Price });
                var guid_data = BLL.Queries.ICO.GetGuidByCoinCode("BTC");
                //var BTCData = BLL.Queries.ICO.GetUpDateById(guid_data.Guid).Select(x => new { x.UpdatedDate, x.Price });
                var BTCData = BLL.Queries.ICO.GetUpDateById(guid_data.Guid, startDate, endDate).Select(x => new { d=x.UpdatedDate.ToLocalTime(), x.Price });
                //var resultdata = result1.Concat(result2);
                return ReturnResult(APIStatus.Success, string.Empty, new { CoinData, BTCData } );

            }
            catch (Exception ex)
            {
                Guid Errorguid = CommonFunction.LogManager("API/ICOController/GetByGuidData()", ex, "Exception Caught.");
                return ReturnExceptionResponse(ex);
            }
        }

        [Route("GetICODataByGuid")]
        public HttpResponseMessage GetICODataByGuid(Guid guid)
        {
            try
            {
                var resultdata = BLL.Queries.ICO.GetByGuid(guid);
                //var resultdata = BLL.Queries.ICO.GetHistoryDataByGuid(guid).Select(x => new { x.Price, x.MarketCap, x.CirculatingSupply, x.MaxSupply, d=x.UpdatedDate.ToLocalTime() }).Distinct();
                return ReturnResult(APIStatus.Success, string.Empty, resultdata);

            }
            catch (Exception ex)
            {
                Guid Errorguid = CommonFunction.LogManager("API/ICOController/GetChartsDateWiseData()", ex, "Exception Caught.");
                return ReturnExceptionResponse(ex);
            }
        }

        [Route("GetHistoryByGuid")]
        public HttpResponseMessage GetHistoryByGuid(Guid guid)
        {
            try
            {
                //var resultdata = BLL.Queries.ICO.GetByGuid(guid);
                var resultdata = BLL.Queries.ICO.GetHistoryDataByGuid(guid).Select(x => new { x.Price, x.MarketCap, x.CirculatingSupply, x.MaxSupply, d = x.UpdatedDate.ToLocalTime() }).Distinct();
                return ReturnResult(APIStatus.Success, string.Empty, resultdata);
            }
            catch (Exception ex)
            {
                Guid Errorguid = CommonFunction.LogManager("API/ICOController/GetChartsDateWiseData()", ex,"Exception Caught.");
                return ReturnExceptionResponse(ex);
            }
        }
        [Route("GetHistoryByDateRange")]
        public HttpResponseMessage GetHistoryByDateRange(Guid guid,string startDate="",string endDate="")
        {
            try
            {
                //var resultdata = BLL.Queries.ICO.GetByGuid(guid);
                var resultdata = BLL.Queries.ICO.GetHistoryDataByDateRange(guid,startDate,endDate).Select(x => new { x.Price, x.MarketCap, x.CirculatingSupply, x.MaxSupply, d = x.UpdatedDate.ToLocalTime() });
                return ReturnResult(APIStatus.Success, string.Empty, resultdata);

            }
            catch (Exception ex)
            {
                Guid Errorguid = CommonFunction.LogManager("API/ICOController/GetChartsDateWiseData()", ex, "Exception Caught.");
                return ReturnExceptionResponse(ex);
            }
        }

        [Route("GetAllICO")]
        public HttpResponseMessage GetAllICO()
        {
            try
            {
                //var resultdata = BLL.Queries.ICO.GetByGuid(guid);
                var resultdata = BLL.Queries.ICO.GetAll().Select(s=>new {s.ID, s.Guid, s.CoinIcon, s.CoinCode, s.CoinName, s.Website, s.ReferrerUrl, s.Rate, s.Stage, s.Price,s.Description, s.IsRecommend});
                return ReturnResult(APIStatus.Success, string.Empty, resultdata);

            }
            catch (Exception ex)
            {
                Guid Errorguid = CommonFunction.LogManager("API/ICOController/GetChartsDateWiseData()", ex, "Exception Caught.");
                return ReturnExceptionResponse(ex);
            }
        }
        [Route("AddEditDetail")]
        [HttpPost]
		public HttpResponseMessage AddEditDetail(BLL.ModelViews.ICO.Extend collection)
        {
		     BLL.ModelViews.ICO.Extend  o_response = new BLL.ModelViews.ICO.Extend();
            string ResponseMessage = string.Empty;
		    try
            {
			    if (collection.ID != 0)
                {
				    o_response = BLL.Commands.ICO.Update(collection);
                    ResponseMessage = "Details Updated Successfully!";
				}
				else
				{
				    o_response = BLL.Commands.ICO.Create(collection);
                    ResponseMessage = "Details are added Successfully!";
				}
			     return ReturnResult(APIStatus.Success,  ResponseMessage, o_response);
			}
			catch(Exception ex){

                Guid Errorguid = CommonFunction.LogManager("API/ICOController/AddEditDetail()", ex, "Exception Caught.");
                //return RedirectToAction("Index", "LogManager", new { guID = gid });
                //results.success = false;
                ResponseMessage = "Error Creating ICO";
                return ReturnExceptionResponse(ex);
			}
		
		}

      
	    [Route("GetSingleData")]
        public HttpResponseMessage GetSingleData(int ID)
        {
            try
            {
                var o_response = BLL.Queries.ICO.GetById(ID);

                if (o_response != null)
                    return ReturnResult(APIStatus.Success, string.Empty, o_response);
                else
                    return ReturnResult(APIStatus.NotFound, string.Empty, null);
            }
            catch (Exception ex)
            {
                Guid Errorguid = CommonFunction.LogManager("API/ICOController/GetSingleData()", ex, "Exception Caught.");
                //return RedirectToAction("Index", "LogManager", new { guID = gid });
                //results.success = false;
                //ResponseMessage = "Error Creating ICO";
                return ReturnExceptionResponse(ex);
            }
        }

        [Route("DeleteRecord")]
        [HttpGet]
        public HttpResponseMessage DeleteRecord(Guid ID)
        {
            try
            {
                var o_response = BLL.Commands.ICO.Delete(ID);

                if (o_response == true)
                    return ReturnResult(APIStatus.Success, string.Empty, null);
                else
                    return ReturnResult(APIStatus.NotFound, string.Empty, null);
            }
            catch (Exception ex)
            {
                Guid Errorguid = CommonFunction.LogManager("API/ICOController/DeleteRecord()", ex, "Exception Caught.");
                return ReturnExceptionResponse(ex);
            }
        }
}

}
