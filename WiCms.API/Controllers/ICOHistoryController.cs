﻿using System;
using System.Web.Mvc;
using BLL.ModelViews;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Configuration;
using static WiCms.API.Helper.Enum;
using BLL.Tools;
namespace WiCms.API.Controllers
{
    [RoutePrefix("api/ICOHistory")]
    public class ICOHistoryController : BaseApiController
    {
        // GET: ICOHistory
        
        [Route("GetTodayICOHistoryData")]
        public HttpResponseMessage GetTodayICOHistoryData()
        {
            try
            {
                var resultdata = BLL.Queries.ICO.GetOneYearOldDataFromToday();
                return ReturnResult(APIStatus.Success, string.Empty, resultdata);

            }
            catch (Exception ex)
            {
                Guid Errorguid = CommonFunction.LogManager("API/ICOHitoryController/GetTodayICOHistoryData()", ex, "Exception Caught.");
                return ReturnExceptionResponse(ex);
            }
        }   
    }
}