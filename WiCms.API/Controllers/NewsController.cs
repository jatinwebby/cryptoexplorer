﻿using BLL.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using static WiCms.API.Helper.Enum;

namespace WiCms.API.Controllers
{
    [RoutePrefix("api/News")]
    public class NewsController : BaseApiController
    {
        [Route("GetList")]
        public HttpResponseMessage GetList()
        {
            try
            {               
                    var resultdata = BLL.Queries.NewsManage.GetAll().Select(s => new { s.ID, s.Guid, s.Author,s.CreatedDate,s.EndEventDate,s.StartEventDate,s.HeaderImage,s.LongDescription,s.ShortDescription,s.Title });
                    return ReturnResult(APIStatus.Success, string.Empty, resultdata);               
            }
            catch (Exception ex)
            {
                    Guid Errorguid = CommonFunction.LogManager("API/NewsController/GetList()", ex, "Exception Caught.");
                    return ReturnExceptionResponse(ex);
            }
        }
        
        [Route("GetTodayRangeNewsList")]
        public HttpResponseMessage GetTodayRangeNewsList()
        {
            try
            {
                var resultdata = BLL.Queries.NewsManage.GetTodayRangeNews().Select(s => new { s.ID, s.Guid, s.Author, s.Title, s.LongDescription, s.ShortDescription, s.CreatedDate, s.StartEventDate, s.EndEventDate, s.HeaderImage });
                return ReturnResult(APIStatus.Success, string.Empty, resultdata);
            }
            catch (Exception ex)
            {
                Guid Errorguid = CommonFunction.LogManager("API/NewsController/GetTodayRangeNewsList()", ex, "Exception Caught.");
                return ReturnExceptionResponse(ex);
            }
        }

        [Route("GetSelDateRangeNewsList")]
        public HttpResponseMessage GetSelDateRangeNewsList(string fromDate,string toDate)
        {
            try
            {
                var resultdata = BLL.Queries.NewsManage.GetSelDateRangeNews(fromDate, toDate).Select(s => new { s.ID, s.Guid, s.Author, s.Title, s.LongDescription, s.ShortDescription, s.CreatedDate, s.StartEventDate, s.EndEventDate, s.HeaderImage });
                return ReturnResult(APIStatus.Success, string.Empty, resultdata);
            }
            catch (Exception ex)
            {
                Guid Errorguid = CommonFunction.LogManager("API/NewsController/GetSelDateRangeNewsList()", ex, "Exception Caught.");
                return ReturnExceptionResponse(ex);
            }
        }
        [Route("GetByGuid")]
        public HttpResponseMessage GetByGuid(Guid guid)
        {
            try
            {
                var resultdata = BLL.Queries.NewsManage.GetByGuid(guid);
                return ReturnResult(APIStatus.Success, string.Empty, resultdata);
            }
            catch (Exception ex)
            {
                Guid Errorguid = CommonFunction.LogManager("API/NewsController/GetByGuid()", ex, "Exception Caught.");
                return ReturnExceptionResponse(ex);
            }
        }
    }
}