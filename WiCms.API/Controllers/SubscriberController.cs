
using System;
using System.Web.Mvc;
using BLL.ModelViews;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Configuration;
//using System.Web.Http;
using static WiCms.API.Helper.Enum;

namespace WiCms.API.Controllers
{
    [RoutePrefix("api/Subscriber")]
    public class SubscriberController : BaseApiController
    {
	    [Route("GetList")]
        [HttpPost]
		public HttpResponseMessage GetList(BLL.ModelViews.Subscriber.Extend collection)
        {
		    try
            {
			     var resultdata=BLL.Queries.Subscriber.GetAll();
			     return ReturnResult( APIStatus.Success,  string.Empty, resultdata);
			}
			catch(Exception ex){
			  return ReturnExceptionResponse(ex);
			}
		
		}

		[Route("AddEditDetail")]
        [HttpPost]
		public HttpResponseMessage AddEditDetail(BLL.ModelViews.Subscriber.Extend collection)
        {
            BLL.ModelViews.Subscriber.Extend o_response = new BLL.ModelViews.Subscriber.Extend();
            string ResponseMessage = string.Empty;
            try
            {
                var ip = System.Web.HttpContext.Current != null ? System.Web.HttpContext.Current.Request.UserHostAddress : "";
                // ----- local test ----- //
                if (ip == "::1")
                    ip = "116.73.199.88";

                // ----- request for location details by ip ----- //
                string response = BLL.Tools.CommonFunction.GetStringFromURL("http://freegeoip.net/json/" + ip);
                dynamic result = BLL.Tools.Converstions.JsonDeserialize<dynamic>(response);
                collection.IPAddress = result.ip;
                collection.City = result.city;
                collection.State = result.region_name;
                collection.Country = result.country_name;
                collection.PostCode = result.zip_code;
                //String request = stream.Read(stream,0,0);

                var error = BLL.Validations.Subscriber.Validate(collection);
                if (error.Count > 0)
                {
                    return ReturnResult(APIStatus.Failure, string.Join(", ", error.Select(s=>s.ValidationMessage)), error);
                }

                if (collection.Guid == Guid.Empty)
                {
                    // Create record
                    var data = BLL.Commands.Subscriber.Create(collection);
                    if (data.ID != 0)
                    {
                        var cms_data=BLL.Queries.CMSManage.GetByPageName("NEW_SUBSCRIBER");
                        //cms_data.Content = cms_data.Content.Replace("##username##", "pooja");
                        BLL.Tools.CommonFunction.sendMail(cms_data.Title,cms_data.Content,collection.Email,"");
                        return ReturnResult(APIStatus.Success, "Thank you for Subscriber!!", data);
                        
                    }
                    else
                    {
                        return ReturnResult(APIStatus.Failure, "There was an error inserting.", data);
                    }

                }
                else
                {
                    // Update record
                    var data = BLL.Commands.Subscriber.Update(collection);
                    if (data != null)
                    {
                        return ReturnResult(APIStatus.Success, "Details Updated Successfully!", data);
                    }
                    else
                    {
                        return ReturnResult(APIStatus.Failure, "There was an error updating.", data);
                    }
                }

            }
            catch (Exception ex)
            {
                return ReturnExceptionResponse(ex);
            }
		}

      
	    [Route("GetSingleData")]
        public HttpResponseMessage GetSingleData(int ID)
        {
            try
            {
                var o_response = BLL.Queries.Subscriber.GetById(ID);

                if (o_response != null)
                    return ReturnResult(APIStatus.Success, string.Empty, o_response);
                else
                    return ReturnResult( APIStatus.NotFound,  string.Empty, null);
            }
            catch (Exception ex)
            {              
                return ReturnExceptionResponse(ex);
            }
        }

        [Route("DeleteRecord")]
        [HttpGet]
        public HttpResponseMessage DeleteRecord(Guid ID)
        {
            try
            {
                var o_response = BLL.Commands.Subscriber.Delete(ID);

                if (o_response == true)
                    return ReturnResult(APIStatus.Success, string.Empty, null);
                else
                    return ReturnResult(APIStatus.NotFound, string.Empty, null);
            }
            catch (Exception ex)
            {                
                return ReturnExceptionResponse(ex);
            }
        }
}

}
