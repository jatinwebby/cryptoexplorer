﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;

namespace WiCms.API.Helper
{
    public class HelperUtility
    {
        public static MediaTypeFormatter GetFormatter(Uri requestUri, System.Web.Http.HttpConfiguration Configuration)
        {
            var requestUriString = Convert.ToString(requestUri);
            if (requestUriString.Contains("format=xml"))
                return Configuration.Formatters.XmlFormatter;
            else
                return Configuration.Formatters.JsonFormatter;
        }

        public static string GetMediaType(Uri requestUri)
        {
            var requestUriString = Convert.ToString(requestUri);
            if (requestUriString.Contains("format=xml"))
                return "application/xml";
            else
                return "application/json";
        }

        public static T DataMapping<T>(object data)
        {
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(data);
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(json);
        }
    }
}
