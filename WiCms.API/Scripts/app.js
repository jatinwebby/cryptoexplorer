﻿$(document).ready(function () {
    // ----- Hide and Show Menu Header Logo on scroll possion ----- //
    scrollFunction();
    window.onscroll = function () { scrollFunction() };
    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            document.getElementById("CryptoExploreLogo").style.top = "0";
        }
        else {
            document.getElementById("CryptoExploreLogo").style.top = "-200px";
        }
    }
});