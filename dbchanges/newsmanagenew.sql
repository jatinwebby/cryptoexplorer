USE [dbCryptoExplorer]
GO
/****** Object:  Table [dbo].[NewsManage]    Script Date: 3/10/2018 10:18:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsManage](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL,
	[HeaderImage] [varchar](2083) NULL,
	[Author] [varchar](50) NULL,
	[Title] [varchar](250) NULL,
	[ShortDescription] [varchar](500) NULL,
	[LongDescription] [varchar](max) NULL,
	[StartEventDate] [datetime] NULL,
	[EndEventDate] [datetime] NULL,
	[Sort] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NOT NULL,
	[isDeleted] [bit] NOT NULL,
	[Status] [varchar](50) NOT NULL,
 CONSTRAINT [PK_NewsManage] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
